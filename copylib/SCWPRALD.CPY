       01 SCWT-PAIRED-AISLE.
          05 CPY-CD PIC G(5).
          05 FACL PIC G(1).
          05 WHSE PIC G(2).
          05 FROM-HIGH-AISLE PIC G(3).
          05 FROM-HIGH-BAY PIC S9(9) COMP-5.
          05 FROM-LOW-AISLE PIC G(3).
          05 FROM-LOW-BAY PIC S9(9) COMP-5.
          05 TO-HIGH-AISLE PIC G(3).
          05 TO-HIGH-BAY PIC S9(9) COMP-5.
          05 TO-LOW-AISLE PIC G(3).
          05 TO-LOW-BAY PIC S9(9) COMP-5.
          05 RECORD-STATUS PIC G(1).
          05 MAINT-DATE PIC G(10).
          05 MAINT-TIME PIC G(8).
          05 MAINT-TRAN PIC G(10).
          05 MAINT-USER PIC G(10).
          05 UPDATE-SERIAL PIC S9(18) COMP-5.
          05 UNIQUE-ID PIC S9(18) COMP-5.
