      *****************************************************************
      *---------------------------------------------------------------*
      *                                                               *
      *                     Change History                            *
      *---------------------------------------------------------------*
      * CHANGE  |CHANGE|                                              *
      *  DATE   |NUMBER|      Detailed Change Description             *
      *---------------------------------------------------------------*
      *10Dec02  |..-...| New Copybook                                 *
      *         |      |                                              *
      *****************************************************************
       01  CPEX-PARMS.
      *    05 CPEX-TASK-ID              PIC S9(9)        COMP-5
      *                                  GET-PROPERTY SET-PROPERTY.
           05 CPEX-TASK-ID              PIC G(9)
                                         GET-PROPERTY SET-PROPERTY.
           
           05 CPEX-EXCPT-CODE           PIC G(2)
                                        GET-PROPERTY SET-PROPERTY.
      *     05 CPEX-ASSIGNMENT           LIKE AA-ASSIGNMENT
      *                                  GET-PROPERTY SET-PROPERTY.
            05 CPEX-ASSIGNMENT           PIC G(9)
                                         GET-PROPERTY SET-PROPERTY.
           
           05 CPEX-EXCPT-LOCATION       LIKE AA-LOCATION
                                        GET-PROPERTY SET-PROPERTY.
           05 CPEX-EMPLOYEE-CODE        LIKE AA-EMPLOYEE-CODE
                                        GET-PROPERTY SET-PROPERTY.
           05 CPEX-CURR-LOCATION        LIKE AA-LOCATION
                                        GET-PROPERTY SET-PROPERTY.
           05 CPEX-EXCPT-TASK-ID	PIC G(9)        
                                        GET-PROPERTY SET-PROPERTY.

       01 CPEX-TASK-ID-PIC9              PIC S9(9) COMP-5.
       01 CPEX-ASSIGNMENT-PICS9           LIKE AA-ASSIGNMENT.
     
      *01  CPEX-EXCPT-TASK-ID-PICS9        PIC S9(9)         COMP-5.