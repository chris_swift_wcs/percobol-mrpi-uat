      ** This working storage copybook contains the working storage fields 
      ** required to process the objects passed to the customer-order 
      ** interface nodes. 
      *================================================================* 
      *                   COPYBOOK CHANGE HISTORY                      * 
      *----------------------------------------------------------------* 
      *  CHANGE �CHANGE �                                              * 
      *   DATE  �NUMBER �      DETAILED CHANGE DESCRIPTION             * 
      *----------------------------------------------------------------*  
      * 29JAN03 |..-....| New Copybook                                 *
      *         |       |                                              *   
      *================================================================*
      * These are fields used to reference the VOs set up by on-line receiving
       01 customer-order-line-list             object reference 
                "java.util.ArrayList"
                                        get-property set-property.       
       01 customer-order-details          object reference 
       "com.aquitecintl.ewms.common.impex.transformers.voext.CustOrderLi
      -"neVOExt".
      **** cdidbcs ****
       01 customer-order-details-vou          object reference 
       "com.aquitecintl.ewms.common.cobolvoutil.CustOrderLineVOExt".
      *****************          
       
       01 customer-order-header           object reference 
       "com.aquitecintl.ewms.common.impex.transformers.voext.CustOrderHe
      -"aderVOExt"
                                        get-property set-property.
      **** cdidbcs *****
       01 customer-order-header-vou           object reference 
       "com.aquitecintl.ewms.common.cobolvoutil.CustOrderHeaderVOExt".      
      ******************                                          

      * These are fields that are used by the COBOL program when dealing
      * with the VOs that have been set up. 
       01 line-details-size             pic s9(09)     comp-5.
       01 line-details-count            pic s9(09)     comp-5. 
      
       01 WX-CUS-ORDER-HDR-LIST.          
          05 COH-CPY-CD                 PIC G(5).
          05 COH-ORDER-FACL             PIC G.
          05 COH-ORDER-WHSE             PIC G(2).
          05 COH-CUSTOMER-CODE          PIC G(14).
          05 COH-CUSTOMER-TYPE          PIC G(2).
          05 COH-ORDER-NUMBER           PIC S9(9) COMP-5.
          05 COH-ORDER-TYPE             PIC G.
          05 COH-PICK-CYCLE             PIC G(3).
          05 COH-ROUTE                  PIC G(3).
          05 COH-STOP-NUMBER            PIC G(2).
          05 COH-PICK-WAVE              PIC G(2).
          05 COH-PICK-BSEQ              like aa-integer.
          05 COH-ORDER-DATE             PIC G(10).
          05 COH-SHIP-DATE              PIC G(10).
          05 COH-LOAD-POINT             PIC G(15).
          05 COH-ORDER-TEXT             PIC G(25).
          05 COH-INVOICE                PIC S9(9) COMP-5.
          05 COH-INVOICE-DATE           PIC G(10).
          05 COH-XDOC-PR-REF            PIC S9(9) COMP-5.
          05 COH-XDOC-TRADER            PIC G(14).
          05 COH-XDOC-TRADER-TYPE       PIC G(2).
          05 COH-WEB-ORD-CD             PIC G(15).
          05 COH-ORDER-COMMENT          PIC G(80).
          05 COH-ORDER-TIME             PIC G(8).
          05 COH-REFERENCE-CODE         PIC G(15).
          05 COH-TRUCK                  PIC G(5).
          05 COH-SALESPERSON            PIC G(3).
          05 COH-CUSTOMER-CONTACT       PIC G(30).
          05 COH-CONTAINER-TYPE         PIC G.
          
       01 WX-CUS-ORDER-LIN-LIST.
          05 COL-ORDER-LINE             PIC S9(9) COMP-5.
          05 COL-ITEM-OWNER             PIC G(5).
          05 COL-ITEM-FACL              PIC G.
          05 COL-ITEM-WHSE              PIC G(2).
          05 COL-ITEM-CODE              PIC G(14).
          05 COL-ITEM-PACK-CODE         PIC G(2).
          05 COL-ORDER-QTY              PIC S9(9) COMP-5.
          05 COL-ADJUSTED-QTY           PIC S9(9) COMP-5.
          05 COL-APPORTION-QTY          PIC S9(9) COMP-5.
          05 COL-RELEASED-QTY           PIC S9(9) COMP-5.
          05 COL-RETAIL-PRICE           PIC S9(10)V9(5) COMP-3.
          05 COL-RETAIL-FOR             PIC S9(9) COMP-5.
          05 COL-RETAIL-ZONE            PIC S9(9) COMP-5.
          05 COL-GROSS-INV-AMT          PIC S9(6)V9(5) COMP-3.
          05 COL-INV-RTL-AMT            PIC S9(10)V9(5) COMP-3.
          05 COL-INV-GRS-PFT-PCT        PIC S9(6)V9(5) COMP-3.
          05 COL-INV-MKU                PIC S9(10)V9(5) COMP-3.
          05 COL-INV-DLV-CHG            PIC S9(10)V9(5) COMP-3.
          05 COL-INV-WHS-UCH            PIC S9(10)V9(5) COMP-3.
          05 COL-INV-SPORD-CHG          PIC S9(10)V9(5) COMP-3.
          05 COL-INV-DIS-AMT            PIC S9(10)V9(5) COMP-3.
          05 COL-INV-ALW-APP            PIC S9(10)V9(5) COMP-3.
          05 COL-INV-TAX                PIC S9(10)V9(5) COMP-3.
          05 COL-NET-INV-AMT            PIC S9(10)V9(5) COMP-3.
          05 COL-SPC-PRC-FL             PIC G.
          05 COL-ORDERED-ITEM           PIC G(14).
          05 COL-EB-BAL-ON-HAND         PIC S9(9) COMP-5.
          05 COL-SPECIAL-APPORTION      PIC G.
          05 COL-TRANSHIP-FACL          PIC G.
          05 COL-TRANSHIP-WHSE          PIC G(2).
          05 COL-SUB-NUMBER             PIC S9(9) COMP-5.
          05 COL-SUB-SOURCE             PIC G.
