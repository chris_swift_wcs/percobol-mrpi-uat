      *================================================================* 
      *                   COPYBOOK CHANGE HISTORY                      * 
      *----------------------------------------------------------------* 
      *  CHANGE �CHANGE �                                              * 
      *   DATE  �NUMBER �      DETAILED CHANGE DESCRIPTION             * 
      *----------------------------------------------------------------*  
      * 17SEP02 |..-....| New Copybook                                 *
      *         |       |                                              *   
      *================================================================*

       01  SBLF-PARMS.
      * input 
           05  SBLF-LBL-PRINTER-NAME   LIKE AA-PRINTER.
           05  SBLF-LIST-ID            LIKE AA-INTEGER.
           05  SBLF-RELEASE-NO         LIKE AA-INTEGER.
           05  SBLF-PROCESS-NODE       LIKE AA-BIGINT.
