      *================================================================*
      *                   COPYBOOK CHANGE HISTORY                      *
      *----------------------------------------------------------------*
      *  CHANGE �CHANGE �                                              *
      *   DATE  �NUMBER �      DETAILED CHANGE DESCRIPTION             *
      *----------------------------------------------------------------*
      * 20JUN02 |..-....| New Copybook                                 *
      *         |       |                                              *
      *================================================================*
      ******************************************************************
      *                                                                *
      *                                                                *
      ******************************************************************
       01  PKDP-PARMS.
           05  PKDP-CPY-CD                 PIC G(5)
                                             get-property  set-property.
           05  PKDP-FACL                   PIC G
                                             get-property  set-property.
           05  PKDP-WHSE                   PIC GG
                                             get-property  set-property.
           05  PKDP-ITEM-OWNER             PIC G(5)
                                             get-property  set-property.
           05  PKDP-ITEM-CODE              PIC G(14)
                                             get-property  set-property.
           05  PKDP-ITEM-PACK-CODE         PIC G(2)
                                             get-property  set-property.
           05  PKDP-RETURN-PACK            PIC G(15)
                                             get-property  set-property.
      *     05 PKDP-RETURN-PACK-PICX PIC X(15) .

           05  PKDP-UPDATE-SERIAL PIC G(18)
                    get-property  set-property.
           
      *    05  PKDP-UPDATE-SERIAL          PIC S9(18)   COMP-5
      *                                      get-property  set-property.

       01  PKDP-UPDATE-SERIAL-PICS9 PIC S9(18)   COMP-5.