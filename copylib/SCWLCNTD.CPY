       01 SCWT-LOT-CONTROL.
          05 CPY-CD PIC G(5).
          05 ITEM-OWNER PIC G(5).
          05 FACL PIC G(5).
          05 WHSE PIC G(5).
          05 ITEM-CODE PIC G(14).
          05 LOT-CODE PIC G(20).
          05 QTY-ADDED PIC S9(9) COMP-5.
          05 QTY-SHIPPED PIC S9(9) COMP-5.
          05 QTY-ADJ-OFF PIC S9(9) COMP-5.
          05 UPDATE-SERIAL PIC S9(9) COMP-5.
          05 MAINT-DATE PIC G(10).
          05 MAINT-TIME PIC G(8).
          05 MAINT-TRAN PIC G(10).
          05 MAINT-USER PIC G(10).
