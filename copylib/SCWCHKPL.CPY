      *================================================================* 
      *                   COPYBOOK CHANGE HISTORY                      * 
      *----------------------------------------------------------------* 
      *  CHANGE �CHANGE �                                              * 
      *   DATE  �NUMBER �      DETAILED CHANGE DESCRIPTION             * 
      *----------------------------------------------------------------*  
      * 20JUN02 |..-....| New Copybook                                 *
      *         |       |                                              *   
      *================================================================*

       01 CHKP-parms.
          05 Passed-data. 
            10  CHKP-cpy-cde             like aa-cpy-cde   VALUE " ".
            10  CHKP-facl                like aa-facl      VALUE " ".
            10  CHKP-whse                like aa-whse      VALUE " ".
            10  CHKP-owner               like aa-owner     VALUE " ".
            10  CHKP-item                like aa-item-code VALUE " ".
            10  CHKP-pack-code           PIC G(02)         VALUE " ".
            10  CHKP-loc-facl            like aa-facl      VALUE " ".
            10  CHKP-loc-whse            like aa-whse      VALUE " ".
            
          05  Current-Pack-PSLT-Data.
            10  CHKP-curpak-pslt         like aa-location  VALUE " ".
            10  CHKP-curpak-pslt-tp      PIC G(01)         VALUE " ".
            10  CHKP-curpak-pslt-fl      PIC G(01)         VALUE " ".
            10  CHKP-curpak-pslt-sect    like aa-sect   VALUE N"00".
            10  CHKP-curpak-pslt-rboh    like aa-integer   VALUE 0.
            10  CHKP-curpak-pslt-pboh    like aa-integer   VALUE 0.
            10  CHKP-curpak-pslt-rotd    like aa-date      VALUE
                                                       N"1980-01-01". 
            10  CHKP-curpak-pslt-lot     like aa-lotcode   VALUE " ".
            
          05  Storage-Pack-PSLT-Data.
            10  CHKP-stgpak-pslt         like aa-location  VALUE " ".
            10  CHKP-stgpak-pslt-tp      PIC G(01)         VALUE " ".
            10  CHKP-stgpak-pslt-fl      PIC G(01)         VALUE " ".
            10  CHKP-stgpak-pslt-sect    like aa-sect   VALUE N"00".            
            10  CHKP-stgpak-pslt-rboh    like aa-integer   VALUE 0.
            10  CHKP-stgpak-pslt-pboh    like aa-integer   VALUE 0.
            10  CHKP-stgpak-pslt-rotd    like aa-date      VALUE
                                                       N"1980-01-01".
            10  CHKP-stgpak-pslt-lot     like aa-lotcode VALUE " ".
         