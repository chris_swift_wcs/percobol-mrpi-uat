       01 SCTT-RSI-HOUR.
          05 TRANSACTION PIC S9(18) COMP-5.
          05 CPY-CD PIC G(5).
          05 FACL PIC G(1).
          05 WHSE PIC G(2).
          05 SCHD-DATE PIC G(10).
          05 START-TIME PIC G(8).
          05 END-TIME PIC G(8).
          05 CREATE-DATE PIC G(10).
