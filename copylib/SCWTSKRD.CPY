      ******************************************************************
      *
      *================================================================* 
      *                   Change History                               * 
      *----------------------------------------------------------------* 
      *  CHANGE �CHANGE �                                              * 
      *   DATE  �NUMBER �      DETAILED CHANGE DESCRIPTION             * 
      *----------------------------------------------------------------*  
      * 13MAY10 |09-0824a Normalise task types and                     *
      *                 | increase task type length                    *
      ******************************************************************
       01 SCWT-TASK-RATES.
          05 CPY-CD PIC G(5).
          05 FACL PIC G(1).
          05 WHSE PIC G(2).
      *&10 Begin 09-0824a 
      *&1005 TASK-TYPE PIC G(1).
          05 TASK-TYPE PIC G(3).
      *&10 End   09-0824a
          05 TASK-RATE PIC S9(9) COMP-5.
          05 MAINT-DATE PIC G(10).
          05 MAINT-TIME PIC G(8).
          05 MAINT-USER PIC G(10).
          05 MAINT-TRAN PIC G(10).
          05 UPDATE-SERIAL PIC S9(18) COMP-5.
