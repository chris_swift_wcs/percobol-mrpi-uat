       01 SCWT-OWNER.
          05 CPY-CD PIC G(5).
          05 ITEM-OWNER PIC G(5).
          05 DESCRIPTION PIC G(30).
          05 UPDATE-SERIAL PIC S9(18) COMP-5.
          05 MAINT-DATE PIC G(10).
          05 MAINT-TIME PIC G(8).
          05 MAINT-USER PIC G(10).
          05 MAINT-TRAN PIC G(10).
