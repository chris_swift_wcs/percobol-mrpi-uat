      ******************************************************************
      *
      *================================================================* 
      *                   Change History                               * 
      *----------------------------------------------------------------* 
      *  CHANGE �CHANGE �                                              * 
      *   DATE  �NUMBER �      DETAILED CHANGE DESCRIPTION             * 
      *----------------------------------------------------------------*  
      * 13MAY10 |09-0824a Normalise task types and                     *
      *                 | increase task type length                    *
      ******************************************************************
       01 SCWT-TASK-PRI.
          05 CPY-CD PIC G(5).
          05 FACL PIC G(1).
          05 WHSE PIC G(2).
          05 PARAMETER-SET PIC G(3).
      *10 Begin 09-0824a   
      *10 05 PUTAWAY-PRIORITY PIC S9(9) COMP-5.
      *10 05 LETDOWN-PRIORITY PIC S9(9) COMP-5.
      *10 05 REFILL-PRIORITY PIC S9(9) COMP-5.
      *10 05 SLOTMOVE-PRIORITY PIC S9(9) COMP-5.
      *10 05 MAX-TIME-ASSIGN PIC S9(9) COMP-5.
      *10 05 MAX-TASK-ASSIGN PIC S9(9) COMP-5.
      *10 05 LET-MAX-READY-TIME PIC S9(9) COMP-5.
      *10 05 LET-MAX-TRAVL-TIME PIC S9(9) COMP-5.
      *10 05 PUT-MAX-READY-TIME PIC S9(9) COMP-5.
      *10 05 PUT-MAX-TRAVL-TIME PIC S9(9) COMP-5.
      *10 05 SMV-MAX-READY-TIME PIC S9(9) COMP-5.
      *10 05 SMV-MAX-TRAVL-TIME PIC S9(9) COMP-5.
      *10 05 RFL-MAX-READY-TIME PIC S9(9) COMP-5.
      *10 05 RFL-MAX-TRAVL-TIME PIC S9(9) COMP-5.
      *10 05 ACTIVE-STATUS PIC G(1).
          05 TASK-TYPE PIC G(3).
          05 TASK-PRIORITY PIC S9(9) COMP-5.
          05 MAX-READY-TIME PIC S9(9) COMP-5.
          05 MAX-TRAVL-TIME PIC S9(9) COMP-5.
      *10 End   09-0824a        
          05 MAINT-DATE PIC G(10).
          05 MAINT-TIME PIC G(8).
          05 MAINT-USER PIC G(10).
          05 MAINT-TRAN PIC G(10).
          05 UPDATE-SERIAL PIC S9(18) COMP-5.
