      *================================================================* 
      *                   COPYBOOK CHANGE HISTORY                      * 
      *----------------------------------------------------------------* 
      *  CHANGE �CHANGE �                                              * 
      *   DATE  �NUMBER �      DETAILED CHANGE DESCRIPTION             * 
      *----------------------------------------------------------------*  
      * 20JUN02 |..-....| New Copybook                                 *
      *         |       |                                              *   
      *================================================================*
      
       01 SYS-CALLS.
          05 SYS-CALL.
             10 SYS-COM          PIC G(17)
                               VALUE N"C:\COMMAND.COM/C ".
             10 SYS-OPTION       PIC G(63).
             
       01 SYS-BLOCK           PIC G.
       01 SYS-CODE            PIC GG.
       01           REDEFINES SYS-CODE.
          05 SYS-CODE-1       USAGE IS COMP.                                        
          05 SYS-SLEEP.
             10 FILLER           PIC G(6) VALUE N"sleep ".
             10 SYS-SLEEP-CNT    PIC 9 VALUE 1.
             10 FILLER           PIC G VALUE LOW-VALUES.
