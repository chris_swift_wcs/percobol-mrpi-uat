
      *================================================================* 
      *                   COPYBOOK CHANGE HISTORY                      * 
      *----------------------------------------------------------------* 
      *  CHANGE �CHANGE �                                              * 
      *   DATE  �NUMBER �      DETAILED CHANGE DESCRIPTION             * 
      *----------------------------------------------------------------*  
      * 04APR04 |..-....| New Copybook                                 *
      *         |       |                                              *   
      *================================================================*
      * These are fields that are set up before the node programs call 
      * the COBOL programs, and are passed to each called COBOL program.
      
       01 rcsp-company-code      PIC G(5)  get-property set-property.
       01 rcsp-facility          PIC G(1)  get-property set-property.
       01 rcsp-warehouse         PIC G(2)  get-property set-property.       
       01 Rcsp-Shipment          PIC G(9)  get-property set-property.
       01 rcsp-shipment-PICS9    pic S9(9) comp-5.
       01 rcsp-trader-id         PIC G(14) get-property set-property.


      *** Shipped Pallets Array  
       01 rcsp-pallets            object reference 
                                     "java.util.ArrayList"
                                     get-property set-property.

   
      *** Shipped Cases Array  
       01 rcsp-cases            object reference 
                                     "java.util.ArrayList"
                                     get-property set-property.

       01 rf-ship-pallet-VO   object reference 
              "com.aquitecintl.ewms.warehouse.common.RfShipPalletVO".        

       01 rf-ship-case-VO   object reference 
              "com.aquitecintl.ewms.warehouse.common.RfShipCaseVO".        

       01 rf-pallet-id pic g(18). 
       01 rf-startcase-id pic g(09).
       01 rf-endcase-id pic g(09).
              
       01 rf-pallet-id-pic9 pic 9(18).
       01 rf-startcase-id-pic9 pic 9(09).          
       01 rf-endcase-id-pic9 pic 9(09).          
       
       01 rcsp-shipped-pallet-size   pic s9(09) comp-5.
       01 rcsp-shipped-case-size     pic s9(09) comp-5.              