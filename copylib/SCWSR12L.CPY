      *================================================================* 
      *                   COPYBOOK CHANGE HISTORY                      * 
      *----------------------------------------------------------------* 
      *  CHANGE �CHANGE �                                              * 
      *   DATE  �NUMBER �      DETAILED CHANGE DESCRIPTION             * 
      *----------------------------------------------------------------*  
      * 05DEC02 |..-....| New Copybook                                 *
      * 24JUN04 |01-xxxx| Rewrite Copybook to remove DBCS Mess        *   
      *================================================================*
       
       01  SR12-PARMS.
      *Input:
           05 SR12-RELEASE                      PIC G(10).      
           05 SR12-TASK-ID                      PIC G(10).     
           05 SR12-ASSIGNMENT-ID                PIC G(9).                                                      
           05 SR12-LABEL-TYPE                   PIC G.
           05 SR12-PLAN-SHIPMENT                PIC G(10).

       01   SR12-RELEASE-PIC9 PIC 9(9).
       01   SR12-TASK-ID-PIC9 PIC 9(9).
       01   SR12-ASSIGNMENT-ID-PIC9 PIC 9(9).
       01   SR12-PLAN-SHIPMENT-PIC9  PIC 9(9).
           