       01 SCWA-ITEM-PICKSLOT.
          05 CPY-CD PIC G(5).
          05 ITEM-FACL PIC G(1).
          05 ITEM-WHSE PIC G(2).
          05 ITEM-OWNER PIC G(5).
          05 ITEM-CODE PIC G(14).
          05 ITEM-PACK-CODE PIC G(2).
          05 LOC-FACL PIC G(1).
          05 LOC-WHSE PIC G(2).
          05 IPK-SEQ PIC S9(18) COMP-5.
          05 PICK-SLOT PIC G(15).
          05 LETDOWN-PT PIC S9(9) COMP-5.
          05 SLOT-CAPACITY PIC S9(9) COMP-5.
          05 PICK-PENDING PIC S9(9) COMP-5.
          05 MAINT-DATE PIC G(10).
          05 MAINT-TIME PIC G(8).
          05 MAINT-USER PIC G(10).
          05 MAINT-TRAN PIC G(10).
          05 PICK-PENDING-RU PIC S9(9) COMP-5.
          05 UPDATE-SERIAL PIC S9(18) COMP-5.
          05 AUDIT-ACTION PIC G(2).
          05 AUDIT-DATE PIC G(10).
          05 AUDIT-TIME PIC G(8).
          05 AUDIT-USER PIC G(10).
          05 AUDIT-TRAN PIC G(10).
          05 AUDIT-IFLAG PIC G(1).
