       01 SCWT-SUPERVISOR.
          05 CPY-CD PIC G(5).
          05 FACL PIC G(1).
          05 WHSE PIC G(2).
          05 MAINT-USER PIC G(10).
          05 MAINT-TRAN PIC G(10).
          05 MAINT-DATE PIC G(10).
          05 MAINT-TIME PIC G(8).
          05 SUPERVISOR-CODE PIC G(10).
          05 SUPERVISOR-NAME PIC G(30).
          05 UPDATE-SERIAL PIC S9(18) COMP-5.
