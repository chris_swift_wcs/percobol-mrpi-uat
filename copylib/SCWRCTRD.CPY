       01 SCWT-RELP-CONTROL.
          05 CPY-CD PIC G(5).
          05 FACL PIC G(1).
          05 WHSE PIC G(2).
          05 PICK-RELEASE PIC S9(9) COMP-5.
          05 RELEASE-TYPE PIC G(1).
          05 RELEASE-STATUS PIC G(1).
          05 SHIP-DATE PIC G(10).
          05 PICK-CYCLE PIC G(3).
          05 PICK-WAVE PIC G(2).
          05 WHSE-DEPT PIC G(2).
          05 WHSE-SECT PIC G(2).
          05 PLAN-SHIPMENT PIC S9(9) COMP-5.
          05 ITEM-OWNER PIC G(5).
          05 ITEM-CODE PIC G(14).
          05 ITEM-FACL PIC G(1).
          05 ITEM-WHSE PIC G(2).
          05 LETDOWN-POINT PIC S9(9) COMP-5.
          05 EST-TOTAL-CASES PIC S9(9) COMP-5.
          05 EST-TOTAL-UNITS PIC S9(9) COMP-5.
          05 EST-TOTAL-WEIGHT PIC S9(10)V9(5) COMP-3.
          05 EST-TOTAL-VOLUME PIC S9(10)V9(5) COMP-3.
          05 EST-TOTAL-LINES PIC S9(9) COMP-5.
          05 EST-TOTAL-SHIPS PIC S9(9) COMP-5.
          05 EST-TOTAL-VALUE PIC S9(12)V9(3) COMP-3.
          05 EST-TOTAL-COST PIC S9(12)V9(3) COMP-3.
          05 ACT-TOTAL-CASES PIC S9(9) COMP-5.
          05 ACT-TOTAL-UNITS PIC S9(9) COMP-5.
          05 ACT-TOTAL-WEIGHT PIC S9(10)V9(5) COMP-3.
          05 ACT-TOTAL-VOLUME PIC S9(10)V9(5) COMP-3.
          05 ACT-TOTAL-LINES PIC S9(9) COMP-5.
          05 ACT-TOTAL-SHIPS PIC S9(9) COMP-5.
          05 ACT-TOTAL-VALUE PIC S9(12)V9(3) COMP-3.
          05 ACT-TOTAL-COST PIC S9(12)V9(3) COMP-3.
          05 ACT-TOTAL-OUTS PIC S9(9) COMP-5.
          05 ACT-TOTAL-FP-PICKS PIC S9(9) COMP-5.
          05 ACT-TOTAL-LETDOWN PIC S9(9) COMP-5.
          05 ACT-TOTAL-SW-LTD PIC S9(9) COMP-5.
          05 START-DATE PIC G(10).
          05 START-TIME PIC G(8).
          05 END-DATE PIC G(10).
          05 END-TIME PIC G(8).
          05 UPDATE-SERIAL PIC S9(9) COMP-5.
          05 MAINT-DATE PIC G(10).
          05 MAINT-TIME PIC G(8).
          05 MAINT-USER PIC G(10).
          05 MAINT-TRAN PIC G(10).
          05 PICK-RELEASE-LINE PIC S9(9) COMP-5.
          05 PRINT-DLV-DOCS PIC G(1).
          05 RUN-APPORTIONMENT PIC G(1).
          05 PBL-ORIGINAL-QTY PIC S9(9) COMP-5.
          05 PBL-REM-QTY PIC S9(9) COMP-5.
          05 PBL-CLOSED-QTY PIC S9(9) COMP-5.
          05 PBL-RELEASE-PCT PIC S9(3)V9(2) COMP-3.
          05 PBL-RELEASE-QTY PIC S9(9) COMP-5.
          05 PBL-QTY-PCT-FLAG PIC G(1).
          05 PBL-PROCESS-OUTS PIC G(1).
          05 PBL-LAST-ITEM-FLAG PIC G(1).
      *10 Begin 09-0826
          05 DELAY-SHIPMENT PIC G(1).
          05 DELAY-SHIP-DATE LIKE AA-DATE.
          05 DELAY-CNTR-DROP LIKE AA-LOCATION.
          05 PUTAWAY-RULE LIKE AA-PUTAWAY-RULE.
      *10 End   09-0826