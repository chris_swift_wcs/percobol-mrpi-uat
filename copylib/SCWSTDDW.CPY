      *****************************************************************
      *---------------------------------------------------------------*
      *                                                               *
      *                    Program Change History                     *
      *---------------------------------------------------------------*
      * CHANGE  |CHANGE|                                              *
      *  DATE   |NUMBER|      Detailed Change Description             *
      *---------------------------------------------------------------*
      *18JUN02  |..-....| New Program                                 *
      *26MAY03  |03-2831| Change element code / id / sub code to text.*               
      *         |       |                                             *
      *****************************************************************
       01  STDD-SWITCHES.
           05  WF-END-OF-STD-DESCS-SW   PIC G        VALUE N"N".
           
       01  STD-DESCS-CURSOR-STATE       PIC G        VALUE N"C".
           88  STD-DESCS-CURSOR-OPEN                 VALUE N"O".
           88  STD-DESCS-CURSOR-CLOSED               VALUE N"C".
       
       01  STDD-STD-DESCS-TABLE.
           05  STDD-INFO                OCCURS 99
                                        INDEXED BY STDD-IDX.
               10  STDD-TYPE            PIC G(2).
               10  STDD-TABLE           PIC G(2).
      *03 BEGIN 03-2831 Change element code / id / sub code to text.                                        
               10  STDD-ELEMENT         PIC G(3).
      *03         PIC S9(9)      COMP-5.
      *03 END   03-2831 Change element code / id / sub code to text.                               
               10  STDD-DESC            PIC G(30).
