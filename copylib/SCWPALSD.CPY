      ******************************************************************
      *                                                                *
      *  Copyright 2002 Aquitec International, Inc.                    *
      *  All rights reserved                                           *
      * ---------------------------------------------------------------*
      *  Version 1  Release  0                                         *
      *                                                                *
      *================================================================* 
      *                   CHANGE HISTORY                               * 
      *----------------------------------------------------------------* 
      *  CHANGE �CHANGE �                                              * 
      *   DATE  �NUMBER �      DETAILED CHANGE DESCRIPTION             * 
      *----------------------------------------------------------------*  
      * 10MAY10 |09-0824a Increase length of task type to 3 characters *  
      *================================================================*
       01 SCWT-PALLET-STEPS.
          05 CPY-CD PIC G(5).
          05 FACL PIC G(1).
          05 WHSE PIC G(2).
          05 PUTAWAY-RULE-SEQ PIC S9(9) COMP-5.
          05 PUTAWAY-SLOT PIC G(15).
          05 SLOT-PALLETS PIC G(1).
          05 BREAKDOWN-REQUIRED PIC G(1).
          05 MAKE-AVAILABLE PIC G(1).
          05 PALLET-QTY PIC S9(9) COMP-5.
          05 UPDATE-SERIAL PIC S9(18) COMP-5.
          05 PALLET-SEQ PIC S9(18) COMP-5.
      *&10 Begin 09-0824a 
      *&1005 TASK-TYPE PIC G(1).
          05 TASK-TYPE PIC G(3).
      *&10 End   09-0824a
          05 MAINT-DATE PIC G(10).
          05 MAINT-TIME PIC G(8).
          05 MAINT-USER PIC G(10).
          05 MAINT-TRAN PIC G(10).
