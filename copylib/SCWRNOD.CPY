      *================================================================* 
      *                   COPYBOOK CHANGE HISTORY                      * 
      *----------------------------------------------------------------* 
      *  CHANGE �CHANGE �                                              * 
      *   DATE  �NUMBER �      DETAILED CHANGE DESCRIPTION             * 
      *----------------------------------------------------------------*  
      * 20JUN02 |..-....| New Copybook                                 *
      *         |       |                                              *   
      *================================================================*
      *
      *  copy book for the reporting process node specific parameters
      *
           01  RNOD-PARMS. 
               05 rnod-report-program PIC G(30)
                  get-property set-property.
               05 rnod-report-data    PIC G(500)
                  get-property set-property.
           
