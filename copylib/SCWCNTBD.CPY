       01 SCTT-COUNT-BOOK.
          05 CPY-CD PIC G(5).
          05 FACL PIC G(1).
          05 WHSE PIC G(2).
          05 PROCESS-NODE PIC S9(9) COMP-5.
          05 SEQUENCE-ID PIC S9(9) COMP-5.
          05 CREATE-DATE PIC G(10).
          05 CREATE-PROGRAM PIC G(10).
          05 PICK-OR-RESERVE PIC G(1).
          05 ODD-OR-EVEN PIC G(1).
          05 SLOT-OR-NON PIC G(1).
          05 LOC-FACL PIC G(1).
          05 LOC-WHSE PIC G(2).
          05 LOCATION PIC G(15).
          05 AISLE PIC G(3).
          05 BAY PIC S9(9) COMP-5.
          05 INTERVAL-EXPIRED PIC G(1).
          05 RELEASE-EXCEPTION PIC G(1).
          05 SLOT-COUNT PIC G(1).
          05 SECTION-COUNT PIC G(1).
          05 ITEM-COUNT PIC G(1).
          05 COMMODITY-COUNT PIC G(1).
          05 RELATED-SLOT PIC G(1).
          05 RELATED-WHSE PIC G(1).
          05 UPDATE-SERIAL PIC S9(18) COMP-5.
