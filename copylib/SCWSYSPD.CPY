       01 SCWT-SYS-PARMS.
          05 CPY-CD PIC G(5).
          05 FACL PIC G(1).
          05 WHSE PIC G(2).
          05 CNT-PER PIC G(1).
          05 EXC-PER PIC G(1).
          05 EXC-RLP PIC G(1).
          05 EXC-IQR PIC G(1).
          05 W44-UPDT PIC G(1).
          05 W44-PBLS PIC G(1).
          05 W44-PBLN PIC G(1).
          05 W70-REST PIC G(1).
          05 OUTSIDE-WHSE PIC G(1).
          05 LMS-LETD PIC G(1).
          05 LMS-RECV PIC G(1).
          05 LMS-SELT PIC G(1).
          05 LMS-FRRF PIC G(1).
          05 LMS-SUPV PIC G(1).
          05 LMS-PBL PIC G(1).
          05 RF-PUTAWAY PIC G(1).
          05 RF-LETDOWN PIC G(1).
          05 RF-REFILL PIC G(1).
          05 ORD-STAT PIC G(1).
          05 ORD-ASSIGN-ORDER PIC G(1).
          05 RCPT-ALW-OVER PIC G(1).
          05 RCPT-OVER-PERC PIC S9(9) COMP-5.
          05 RCPT-WEIGHT-TOL PIC S9(9) COMP-5.
          05 RCPT-WEIGHT-MSG PIC G(1).
          05 MEASURE PIC G(1).
          05 DATE-FORMAT PIC G(1).
          05 SYS-SLOT-LOCN PIC G(1).
          05 SYS-SLOT-AISL PIC S9(9) COMP-5.
          05 SYS-SLOT-BAY PIC S9(9) COMP-5.
          05 ITEM-FLD-SIZE PIC S9(9) COMP-5.
          05 VENDOR-FLD-SIZE PIC S9(9) COMP-5.
          05 CUSTOMER-FLD-SIZE PIC S9(9) COMP-5.
          05 ITEM-FLD-TYPE PIC G(1).
          05 VENDOR-FLD-TYPE PIC G(1).
          05 CUSTOMER-FLD-TYPE PIC G(1).
          05 DAYS-PER-WEEK PIC S9(9) COMP-5.
          05 DEFAULT-PRINT-USER PIC G(10).
          05 RWI-WEIGHT-TYPE PIC G(1).
          05 AUDIT-RWI PIC G(1).
          05 PALT-LBL-FORMAT PIC G(3).
          05 PRE-PRINTED-LABELS PIC G(1).
          05 SYSTEM-LANGUAGE PIC G(2).
          05 SUM-LBL-COUNT PIC S9(9) COMP-5.
          05 RF-SMOV PIC G(1).
          05 UPDATE-SERIAL PIC S9(18) COMP-5.
          05 HIGH-PACK-LTD PIC G(1).
          05 USE-ALLOC-BOH PIC G(1).
          05 ORD-BOH-CHANGE PIC G(1).
          05 BREAK-SHIP-DEPT PIC G(1).
          05 BREAK-SHIP-VOL PIC G(1).
          05 CHG-DELIV-SCHED PIC G(1).
          05 USE-APPORTIONMENT PIC G(1).
          05 DOOR-STOCK-AVAIL PIC G(1).
          05 ORDER-REQD PIC G(1).
          05 PERN-REQ-CASE PIC G(1).
          05 TIME-FORMAT PIC G(3).
          05 RANGING-FLAG PIC G(1).
          05 APR-ONE-CASE PIC G(1).
          05 APR-ONE-CASE-PROF PIC G(1).
          05 RELP-ITEM-MAX-LOCK PIC S9(9) COMP-5.
          05 BNH-OUT-WHEN-SHORT PIC G(1).
          05 RF-SEL-WGHT-ENTRY PIC G(1).
          05 DFT-SCAN-METHOD PIC G(1).
          05 DFT-ITEM-VERIF PIC G(1).
          05 RF-SLT-COUNT-UPD PIC G(1).
          05 EWMS-INSTALLED PIC G(1).
          05 ELMS-INSTALLED PIC G(1).
          05 EB-INSTALLED PIC G(1).
          05 EP-INSTALLED PIC G(1).
          05 RCV-UPD-INV-VALUE PIC G(1).
          05 NON-PBL-PROC-SEQ PIC G(1).
          05 RF-XFER PIC G(1).
          05 MAX-PBL-RLSE-PCT PIC S9(5)V9(2) COMP-3.
          05 MAINT-DATE PIC G(10).
          05 MAINT-TIME PIC G(8).
          05 MAINT-USER PIC G(10).
          05 MAINT-TRAN PIC G(10).
          05 TEMP-FILE-DIR PIC G(50).
