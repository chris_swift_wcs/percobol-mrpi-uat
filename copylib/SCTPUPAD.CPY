      ******************************************************************
      *                                                                *
      *  Copyright 2002 Aquitec International, Inc.                    *
      *  All rights reserved                                           *
      * ---------------------------------------------------------------*
      *  Version 1  Release  0                                         *
      *                                                                *
      *================================================================* 
      *                   CHANGE HISTORY                               * 
      *----------------------------------------------------------------* 
      *  CHANGE �CHANGE �                                              * 
      *   DATE  �NUMBER �      DETAILED CHANGE DESCRIPTION             * 
      *----------------------------------------------------------------*  
      * 10MAY10 |09-0824a Increase length of task type to 3 characters *  
      *================================================================*
       01 SCTT-PUTAWAY-PALTS.
          05 CPY-CD PIC G(5).
          05 TRANSACTION-ID PIC S9(18) COMP-5.
          05 PR-LINE PIC S9(9) COMP-5.
          05 PUTAWAY-FLAG PIC G(1).
          05 PALLET-SEQ PIC S9(18) COMP-5.
          05 PALT-UPDATE-SERIAL PIC S9(18) COMP-5.
          05 PALLET-QTY PIC S9(9) COMP-5.
          05 PALLET-LOT-CODE PIC G(20).
          05 PALLET-ROTN-DATE PIC G(10).
          05 PUTAWAY-LOCATION PIC G(15).
          05 PARTIAL-PALT-FLAG PIC G(1).
          05 PUTAWAY-RULE PIC G(3).
          05 PUTAWAY-LOC-TYPE PIC G(2).
          05 DEFAULT-REASON PIC G(1).
      *&10 Begin 09-0824a  
      *&1005 TASK-TYPE PIC G(1).
          05 TASK-TYPE PIC G(3).
      *&10 End   09-0824a
          05 CREATE-DATE PIC G(10).
          05 PR PIC S9(9) COMP-5.
          05 INTF-COMPLETE PIC G.
          05 FACL PIC G.
          05 WHSE PIC GG.
          05 DAMAGED-QTY PIC S9(9) COMP-5.
          05 PALLET-WEIGHT PIC S9(07)v9(05) COMP-3.
