      *****************************************************************
      *---------------------------------------------------------------*
      *                                                               *
      *                    Program Change History                     *
      *---------------------------------------------------------------*
      * CHANGE  |CHANGE|                                              *
      *  DATE   |NUMBER|      Detailed Change Description             *
      *---------------------------------------------------------------*
      *03SEP02  |..-....| New Copybook                                *
      *         |       |                                             *
      *****************************************************************
       01  IB07-PARMS.
           05  IB07-LOC-FACL           LIKE AA-FACL
               get-property set-property.
           05  IB07-LOC-WHSE           LIKE AA-WHSE
               get-property set-property.
           05  IB07-ITEM-FACL          LIKE AA-FACL
               get-property set-property.
           05  IB07-ITEM-WHSE          LIKE AA-WHSE
               get-property set-property.
           05  IB07-ITEM-OWNER         LIKE AA-OWNER
               get-property set-property.
           05  IB07-ITEM-CODE          LIKE AA-ITEM-CODE
               get-property set-property.
           05  IB07-LOCATION           LIKE AA-LOCATION
               get-property set-property.
