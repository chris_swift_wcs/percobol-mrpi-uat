       01 SCWT-COMMODITY.
          05 MAINT-DATE PIC G(10).
          05 MAINT-TIME PIC G(8).
          05 MAINT-TRAN PIC G(10).
          05 MAINT-USER PIC G(10).
          05 CPY-CD PIC G(5).
          05 FACL PIC G(1).
          05 WHSE PIC G(2).
          05 COMMODITY-CODE PIC S9(9) COMP-5.
          05 COMM-DESCRIPTION PIC G(40).
          05 UPDATE-SERIAL PIC S9(18) COMP-5.
