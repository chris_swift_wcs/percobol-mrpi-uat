       01 SCWT-DELAY.
          05 CPY-CD PIC G(5).
          05 FACL PIC G(1).
          05 WHSE PIC G(2).
          05 DELAY-ID PIC S9(9) COMP-5.
          05 DELAY-CODE PIC G(2).
          05 EMPLOYEE-CODE PIC G(10).
          05 DAY-IN-WEEK PIC G(1).
          05 SHIFT PIC G(1).
          05 EMPLOYEE-DEPT PIC G(2).
          05 ASSIGNMENT PIC S9(9) COMP-5.
          05 AUTH-SUPER PIC G(10).
          05 DELAY-MINUTES PIC S9(6)V9(1) COMP-3.
          05 UPDATE-SERIAL PIC S9(18) COMP-5.
          05 MAINT-DATE PIC G(10).
          05 MAINT-TIME PIC G(8).
          05 MAINT-TRAN PIC G(10).
          05 MAINT-USER PIC G(10).
          05 WEEK-NUMBER PIC S9(9) COMP-5.
