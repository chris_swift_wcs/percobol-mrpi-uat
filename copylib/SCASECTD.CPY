       01 SCWA-SECTION.
          05 CPY-CD PIC G(5).
          05 FACL PIC G(1).
          05 WHSE PIC G(2).
          05 WHSE-SECT PIC G(2).
          05 MAINT-USER PIC G(10).
          05 MAINT-TRAN PIC G(10).
          05 MAINT-DATE PIC G(10).
          05 MAINT-TIME PIC G(8).
          05 SECT-DESC PIC G(20).
          05 PICK-SEQN-TYPE PIC G(1).
          05 PBL-TYPE PIC G(2).
          05 CASES-PER-HOUR PIC S9(9) COMP-5.
          05 CONTAINER-TYPE PIC G(1).
          05 SINGLE-LABEL PIC S9(9) COMP-5.
          05 WHSE-DEPT PIC G(2).
          05 SECT-LOCKED PIC G(1).
          05 PICK-RELEASE PIC S9(9) COMP-5.
          05 VERIF-TYPE PIC G(1).
          05 CONTAINER-MAX PIC S9(9) COMP-5.
          05 PICK-INS-TYPE PIC G(1).
          05 PICK-PRINTER PIC G(20).
          05 TARGET-SLOT PIC G(15).
          05 LETDOWN-SYNC PIC G(1).
          05 SYNC-PRIORITY PIC S9(9) COMP-5.
          05 GENERATE-REPICK PIC G(1).
          05 WAIT-LOCATION PIC G(15).
          05 BULK-PICK-DROP PIC G(15).
          05 UPDATE-SERIAL PIC S9(18) COMP-5.
          05 AUDIT-ACTION PIC G(2).
          05 AUDIT-DATE PIC G(10).
          05 AUDIT-TIME PIC G(8).
          05 AUDIT-USER PIC G(10).
          05 AUDIT-TRAN PIC G(10).
          05 AUDIT-IFLAG PIC G(1).
