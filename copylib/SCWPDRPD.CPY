       01 SCWT-PICKUP-DROP.
         05 CPY-CD PIC G(5).
         05 FACL PIC G(1).
         05 WHSE PIC G(2).
         05 LOCATION PIC G(15).
         05 FROM-AISL PIC G(3).
         05 FROM-BAY PIC S9(3)V     COMP-3.
         05 TO-AISL PIC G(3).
         05 TO-BAY PIC S9(3)V     COMP-3.

