      ******************************************************************
      * Program Change Description                                     *
      *	                                                               *
      * 20JUN2004 | 04-MMMM  | Represent Time Fields as PIC X          *
      *	                                                               *
      ******************************************************************
       01 SCWT-SHIFT.
          05 CPY-CD PIC G(5).
          05 FACL PIC G(1).
          05 WHSE PIC G(2).
          05 EMP-DEPT PIC G(2).
          05 SHIFT PIC G(1).
          05 MAINT-USER PIC G(10).
          05 MAINT-TRAN PIC G(10).
          05 MAINT-DATE PIC G(10).
          05 MAINT-TIME PIC G(8).
      *04 Begin Change **** 04-MMMMM **********    
      *04    05 SHIFT-START-TIME PIC G(8).
      *04    05 SHIFT-END-TIME PIC G(8).
          05 SHIFT-START-TIME PIC X(8).
          05 SHIFT-END-TIME PIC X(8).
      *04 End Change **** 04-MMMMM **********
          05 LUNCH-TIME PIC S9(9) COMP-5.
          05 BREAK-TIME PIC S9(9) COMP-5.
          05 UPDATE-SERIAL PIC S9(18) COMP-5.
