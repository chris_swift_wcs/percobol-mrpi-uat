      *================================================================* 
      *                   COPYBOOK CHANGE HISTORY                      * 
      *----------------------------------------------------------------* 
      *  CHANGE �CHANGE �                                              * 
      *   DATE  �NUMBER �      DETAILED CHANGE DESCRIPTION             * 
      *----------------------------------------------------------------*  
      * 16SEP02 |..-....| New Copybook                                 *
      *         |       |                                              *   
      *================================================================*

       01  PDCT-TABLE.
      * input 
           05  RELP-RELEASE-NO PIC G(9)
                              get-property set-property.
           
                              
           05  RELP-ASSIGN-LIST-NUM  PIC G(9)
               get-property set-property.
       01  RELP-ASSIGN-LIST-NUM-PICS9  LIKE AA-INTEGER .
       01  RELP-RELEASE-NO-PICS9  LIKE AA-INTEGER .
               