      *================================================================* 
      *                   COPYBOOK CHANGE HISTORY                      * 
      *----------------------------------------------------------------* 
      *  CHANGE �CHANGE �                                              * 
      *   DATE  �NUMBER �      DETAILED CHANGE DESCRIPTION             * 
      *----------------------------------------------------------------*  
      * 17SEP02 |..-....| New Copybook                                 *
      *         |       |                                              *   
      *================================================================*

       01  SR10-PARMS. 
      * input 
           05  SR10-LBL-PRINTER-NAME   LIKE AA-REPORT-NAME
                                       get-property set-property.           
           05  SR10-REPORT-PARAMETERS.
               10  SR10-LB1            LIKE AA-REPORT-NAME
                                       get-property set-property.               
               10  SR10-LB2            LIKE AA-REPORT-NAME
                                       get-property set-property.               
               10  SR10-LB3            LIKE AA-REPORT-NAME
                                       get-property set-property.               
               10  SR10-LB4            LIKE AA-REPORT-NAME
                                       get-property set-property.                                                            