      *================================================================* 
      *                   COPYBOOK CHANGE HISTORY                      * 
      *----------------------------------------------------------------* 
      *  CHANGE �CHANGE �                                              * 
      *   DATE  �NUMBER �      DETAILED CHANGE DESCRIPTION             * 
      *----------------------------------------------------------------*  
      * 20JUN02 |..-....| New Copybook                                 *
      *         |       |                                              *   
      *================================================================*
      ****************************************************************
      *  R E C E I V I N G  L A B E L  D E T A I L S                 *
      ****************************************************************

       01 RECL-LABEL-LINES.
          05 RECL-LABEL OCCURS 2 TIMES.
             10 RECL-PO.
                15 RECL-PO-FWHS             PIC G(3).
                15 RECL-PO-NUMB-MAX         PIC G(8).
                15 RECL-PO-LINE             PIC G(3).
             10 RECL-RFWH                   PIC G(3).
             10 RECL-DOOR                   PIC G(3).
             10 RECL-DATE                   PIC G(8).
             10 RECL-TYPE                   PIC G(10).
             10 RECL-VEND-NAME.
                15 RECL-FWHL                PIC G(15).
                15 RECL-SFWH                PIC G(3).
             10 RECL-PAL-LBL                PIC G(7).
             10 RECL-PAL-ID-MAX             PIC G(18).
      **     THE BIG LETTERS FIELD FOR THE LABEL           **
             10 RECL-HOLD-SLOT-DATA.
                15 RECL-HOLD-SLOT           PIC G(6).
             10 RECL-ITM-NM.
                15 RECL-FWH                 PIC G(3).
                15 RECL-ITEM-MAX            PIC G(14).
             10 RECL-QTY-NM                 PIC 9(4).
             10 RECL-PALLET-ID              PIC G(9).
             10 RECL-PACK-CODE              PIC G(2).
             10 RECL-LOT-CODE               PIC G(20).
             10 RECL-ROTATION-DATE          PIC G(8).
             10 RECL-PAL-NUMB               PIC 9(3).
             10 RECL-PAL-SIZE               PIC G(5).
             10 RECL-TI                     PIC 9(2).
             10 RECL-HI                     PIC 9(2).
             10 RECL-PACK                   PIC G(7).
             10 RECL-ITEM-SIZE              PIC G(9).
             10 RECL-ITEM-DESC              PIC G(30).
             10 RECL-PICK.
                15 RECL-FWW                 PIC G(3).
                15 RECL-SLOT                PIC G(6).
             10 RECL-ASGN                   PIC 9(5).
          EJECT
