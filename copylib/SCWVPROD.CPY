      * 
       01 SCWT-VOICE-PROFILE. 
         05 CPY-CD                  PIC G(5).
         05 FACL                    PIC G(1).
         05 WHSE                    PIC G(2).
         05 VOICE-PROFILE           PIC G(15).
         05 ALLOW-SKIP-AISLE        PIC G(1).
         05 ALLOW-SKIP-SLOT         PIC G(1).
         05 CHECK-DIGIT-LENGTH      PIC S9(09) COMP-5.
         05 SLOT-EMPTY-SLOT         PIC G(1).
         05 BARCODE-LENGTH          PIC S9(09) COMP-5.
         05 GTIN-EMPTY-SLOT         PIC G(1).
         05 ITEM-LENGTH             PIC S9(09) COMP-5.
         05 ITEM-EMPTY-SLOT         PIC G(1).  
         05 PRINT-CNTR-LABEL        PIC G(1). 
         05 RECALC-ASSIGNMENT       PIC G(1).
         05 PICKING-ROUTINE         PIC G(1).
         05 BASE-QTY-LIMIT          PIC S9(09) COMP-5.
         05 BASE-WEIGHT-LIMIT       PIC S9(09) COMP-5.
         05 BASE-CUBE-LIMIT         PIC S9(09) COMP-5.
         05 SPEAK-ASSIGNMENT        PIC G(1).
         05 SPEAK-TOTAL-CASES       PIC G(1). 
         05 SPEAK-TOTAL-CUBE        PIC G(1).
         05 SPEAK-TOTAL-WEIGHT      PIC G(1).  
         05 SPEAK-CUSTOMER          PIC G(1).
         05 SPEAK-DOOR              PIC G(1).
         05 SPEAK-COUNT-CONT        PIC G(1).
         05 SPEAK-CONT-TYPE         PIC G(1).
         05 SPEAK-STD-TIME          PIC G(1).
         05 SPEAK-OUT-ASSIGN        PIC G(1). 
         05 MAINT-DATE              PIC G(10).
         05 MAINT-TIME              PIC G(8).
         05 MAINT-USER              PIC G(10).
         05 MAINT-TRAN              PIC G(10).
         05 UPDATE-SERIAL           PIC S9(18) COMP-5.
       