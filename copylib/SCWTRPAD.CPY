       01 SCWT-TRAVEL-PATH.
          05 CPY-CD PIC G(5).
          05 FACL PIC G(1).
          05 WHSE PIC G(2).
          05 FROM-AREA PIC G(15).
          05 TO-AREA PIC G(15).
          05 VIA-AREA1 PIC G(15).
          05 VIA-AREA2 PIC G(15).
          05 PATH-NAME PIC G(6).
          05 TRAVEL-TIME PIC S9(6)V9(5) COMP-3.
          05 RECORD-STATUS PIC G(1).
          05 MAINT-DATE PIC G(10).
          05 MAINT-TIME PIC G(8).
          05 MAINT-TRAN PIC G(10).
          05 MAINT-USER PIC G(10).
          05 UPDATE-SERIAL PIC S9(18) COMP-5.
          05 UNIQUE-ID PIC S9(18) COMP-5.
