       01 SCWT-LOG-SLOT1.
          05 LOG-TIMESTAMP PIC G(26).
          05 LOG-SEQUENCE PIC S9(18) COMP-5.
          05 CPY-CD PIC G(5).
          05 LOC-FACL PIC G(1).
          05 LOC-WHSE PIC G(2).
          05 LOCATION PIC G(15).
          05 AISLE PIC G(3).
          05 BAY PIC S9(9) COMP-5.
          05 SLOT-CODE PIC G(2).
          05 SLOT-STAT PIC G(1).
          05 SLOT-SIZE PIC G(1).
          05 SLOT-TYPE PIC G(1).
          05 FLOAT-CODE PIC G(1).
          05 SLOT-HEIGHT PIC S9(9) COMP-5.
          05 AVAIL-HEIGHT PIC S9(9) COMP-5.
          05 PICK-POSITIONS PIC S9(9) COMP-5.
          05 RES-POSITIONS PIC S9(9) COMP-5.
          05 ORIG-HEIGHT PIC S9(9) COMP-5.
          05 WEIGHT-LIMIT PIC S9(9) COMP-5.
          05 SLOT-CONFIG PIC G(1).
          05 SLOT-MODULE PIC G(2).
          05 WHSE-SECT PIC G(2).
          05 PICK-SEQ-A PIC S9(9) COMP-5.
          05 PICK-SEQ-B PIC S9(9) COMP-5.
          05 COUNT-SLOT PIC G(1).
          05 COUNT-DATE PIC G(10).
          05 COUNT-PREV PIC G(10).
          05 HOLD-REASON PIC G(2).
          05 HOLD-DESC PIC G(30).
          05 HOLD-USER PIC G(30).
          05 COMMINGLE-FLAG PIC G(1).
          05 X-CORD-PICK PIC S9(6)V9(3) COMP-3.
          05 Y-CORD-PICK PIC S9(6)V9(3) COMP-3.
          05 Z-CORD-PICK PIC S9(6)V9(3) COMP-3.
          05 X-CORD-REFL PIC S9(6)V9(3) COMP-3.
          05 Y-CORD-REFL PIC S9(6)V9(3) COMP-3.
          05 Z-CORD-REFL PIC S9(6)V9(3) COMP-3.
          05 VERIFY-CODE PIC G(15).
          05 UPDATE-SERIAL PIC S9(18) COMP-5.
          05 MAINT-USER PIC G(10).
          05 MAINT-TRAN PIC G(10).
          05 MAINT-DATE PIC G(10).
          05 MAINT-TIME PIC G(8).
