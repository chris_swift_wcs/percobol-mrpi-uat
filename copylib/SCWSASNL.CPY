      *****************************************************************
      *---------------------------------------------------------------*
      *                                                               *
      *                     Change History                            *
      *---------------------------------------------------------------*
      * CHANGE  |CHANGE|                                              *
      *  DATE   |NUMBER|      Detailed Change Description             *
      *---------------------------------------------------------------*
      * 05SEP02 |..-....| New Copybook                                *
      *         |       |                                             *
      *****************************************************************
          01 SASN-PARMS.
             05 SASN-ASSIGNM-1 PIC G(9)
                    get-property set-property.
             
      *      05 SASN-ASSIGNM-1          LIKE AA-INTEGER
      *         get-property set-property.

             05 SASN-ASSIGNM-2 PIC G(9)
                    get-property set-property.
             
      *      05 SASN-ASSIGNM-2          LIKE AA-INTEGER
      *         get-property set-property.
      
             05 SASN-ASSIGNM-3 PIC G(9)
                    get-property set-property.
             
      *      05 SASN-ASSIGNM-3          LIKE AA-INTEGER
      *         get-property set-property.
      
             05 SASN-ASSIGNM-4 PIC G(9)
                    get-property set-property.
             
      *      05 SASN-ASSIGNM-4          LIKE AA-INTEGER
      *         get-property set-property.
             05 SASN-EMPL-CODE          PIC G(10)
                get-property set-property.
             05 SASN-START-DATE         LIKE AA-DATE
                get-property set-property.
             05 SASN-START-TIME         LIKE AA-TIME
                get-property set-property.
             05 SASN-CURR-DAY           PIC G
                get-property set-property.
             05 SASN-CURR-SHIFT         PIC G
                get-property set-property.
             05 SASN-SUPERVISOR         PIC G(10)
                get-property set-property.
                
            
           
             05  SASN-EMPL-UPDATE-SERIAL PIC G(18)
               get-property set-property.
      
             05  SASN-ASGN-HDR-UPD-SERIAL-1 PIC G(18)
               get-property set-property.
           
      *    05  SASN-ASGN-HDR-UPD-SERIAL-1 LIKE AA-UPDATE-SERIAL
      *        get-property set-property.

             05  SASN-ASGN-HDR-UPD-SERIAL-2 PIC G(18)
               get-property set-property.
           
      *    05  SASN-ASGN-HDR-UPD-SERIAL-2 LIKE AA-UPDATE-SERIAL
      *        get-property set-property.

             05  SASN-ASGN-HDR-UPD-SERIAL-3 PIC G(18)
               get-property set-property.
           
      *    05  SASN-ASGN-HDR-UPD-SERIAL-3 LIKE AA-UPDATE-SERIAL
      *        get-property set-property.

             05  SASN-ASGN-HDR-UPD-SERIAL-4 PIC G(18)
               get-property set-property.
               
             05  SASN-START-LOCATION  PIC G(15)  
               get-property set-property.
                          
      *    05  SASN-ASGN-HDR-UPD-SERIAL-4 LIKE AA-UPDATE-SERIAL
      *        get-property set-property.         
      
       01 SASN-ASSIGNM-1-PICS9 LIKE AA-INTEGER.
       01  SASN-ASGN-HDR-UPD-SERIAL-4-PICS9 LIKE AA-UPDATE-SERIAL.
       01  SASN-ASGN-HDR-UPD-SERIAL-3-PICS9 LIKE AA-UPDATE-SERIAL.
       01  SASN-ASGN-HDR-UPD-SERIAL-2-PICS9 LIKE AA-UPDATE-SERIAL.
       01  SASN-ASGN-HDR-UPD-SERIAL-1-PICS9 LIKE AA-UPDATE-SERIAL.
       01  SASN-EMPL-UPDATE-SERIAL-PICS9     LIKE AA-UPDATE-SERIAL.
       01 SASN-ASSIGNM-4-PICS9 LIKE AA-INTEGER.
       01 SASN-ASSIGNM-3-PICS9 LIKE AA-INTEGER.
       01 SASN-ASSIGNM-2-PICS9 LIKE AA-INTEGER.