       01 SCWT-ACCESS-POINTS.
          05 CPY-CD PIC G(5).
          05 FACL PIC G(1).
          05 WHSE PIC G(2).
          05 HIGH-LOCATION PIC G(15).
          05 LOW-LOCATION PIC G(15).
          05 HIGH-X-COORD PIC S9(6)V9(3) COMP-3.
          05 LOW-X-COORD PIC S9(6)V9(3) COMP-3.
          05 HIGH-Y-COORD PIC S9(6)V9(3) COMP-3.
          05 LOW-Y-COORD PIC S9(6)V9(3) COMP-3.
          05 HIGH-Z-COORD PIC S9(6)V9(3) COMP-3.
          05 LOW-Z-COORD PIC S9(6)V9(3) COMP-3.
          05 BAY-LENGTH PIC S9(6)V9(1) COMP-3.
          05 DIRECTION PIC G(1).
          05 ACCESS-AREA PIC G(15).
          05 RECORD-STATUS PIC G(1).
          05 MAINT-DATE PIC G(10).
          05 MAINT-TIME PIC G(8).
          05 MAINT-TRAN PIC G(10).
          05 MAINT-USER PIC G(10).
          05 UPDATE-SERIAL PIC S9(18) COMP-5.
          05 UNIQUE-ID PIC S9(18) COMP-5.
