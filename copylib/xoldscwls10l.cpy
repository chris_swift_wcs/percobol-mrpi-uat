      *================================================================* 
      *                   COPYBOOK CHANGE HISTORY                      * 
      *----------------------------------------------------------------* 
      *  CHANGE �CHANGE �                                              * 
      *   DATE  �NUMBER �      DETAILED CHANGE DESCRIPTION             * 
      *----------------------------------------------------------------*  
      * 20JUN02 |..-....| New Copybook                                 *
      *         |       |                                              *   
      *================================================================*
      ****************************************************************** 
      *                                                                * 
      *   Copybook for SCWLS10 - Travel Time Calculation               *
      *                                                                * 
      ****************************************************************** 
             
        01 LS10-PARMS.
      **** INPUT PARAMETERS 
      
          05 LS10-CPY-CD                PIC G(05).
          05 LS10-FACL                  PIC G.
          05 LS10-WHSE                  PIC GG.
          05 LS10-EQUIP-CODE            PIC GGGGG.
          05 LS10-NUMBER-PALLETS        PIC S999. 
          05 LS10-AUDIT-FLAG            PIC G.      
      
          05 LS10-FROM. 
             10 LS10-FROM-LOCATION      PIC G(15). 
             10 LS10-FROM-AISLE         PIC G(03).
             10 LS10-FROM-BAY           PIC S9(03). 
             10 LS10-FROM-X-CORD        LIKE AA-XYZ-CORDS. 
             10 LS10-FROM-Y-CORD        LIKE AA-XYZ-CORDS.
             10 LS10-FROM-TYPE          PIC GG. 
      
          05 LS10-TO. 
             10 LS10-TO-LOCATION        PIC G(15). 
             10 LS10-TO-AISLE           PIC G(03).
             10 LS10-TO-BAY             PIC S9(03).
             10 LS10-TO-X-CORD          LIKE AA-XYZ-CORDS. 
             10 LS10-TO-Y-CORD          LIKE AA-XYZ-CORDS.
             10 LS10-TO-Z-CORD          LIKE AA-XYZ-CORDS.
             10 LS10-TO-TYPE            PIC GG.  
             10 LS10-TO-SLOT-TYPE       PIC G. 
             10 LS10-TO-SLOT-CONFIG     PIC G.
           05 LS10-UPDATE-SERIAL        PIC S9(18) COMP-5.
      
      
      **** OUTPUT PARAMETERS 
      ****** Total travel time for the move 
           05 LS10-TRAVEL-TIME           PIC S9(07)v9(03) COMP-3.
           05 LS10-TRAVEL-DISTANCE       PIC S9(05)V99 COMP-3.
           05 LS10-TRAVEL-RATE           PIC S9(06)V9(5) COMP-5.
       
      ****** Audit table listing task elements that make up the move
           05 LS10-AT-TABLE              OCCURS 20 TIMES. 
             10 LS10-AT-TO-SLOT         PIC G(15).
             10 LS10-AT-FROM-SLOT       PIC G(15). 
             10 LS10-AT-DIRECTION       PIC G(01). 
             10 LS10-AT-DESCRIPTION     PIC G(30). 
             10 LS10-AT-DISTANCE        LIKE AA-XYZ-CORDS. 
             10 LS10-AT-TIME            LIKE AA-TRAVEL-TIME.
             10 LS10-AT-FROM-X          PIC S9(05)V9.
             10 LS10-AT-FROM-Y          PIC S9(05)V9.  
             10 LS10-AT-TO-X            PIC S9(05)V9.
             10 LS10-AT-TO-Y            PIC S9(05)V9.
             10 LS10-AT-TURN-TIME       PIC S9(05)V9(02) COMP-5.
           05 LS10-AT-SUB                PIC S99. 


