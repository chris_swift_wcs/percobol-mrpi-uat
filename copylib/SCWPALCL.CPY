      *================================================================* 
      *                   COPYBOOK CHANGE HISTORY                      * 
      *----------------------------------------------------------------* 
      *  CHANGE �CHANGE �                                              * 
      *   DATE  �NUMBER �      DETAILED CHANGE DESCRIPTION             * 
      *----------------------------------------------------------------*  
      * 20JUN02 |..-....| New Copybook                                 *
      *         |       |                                              *   
      *================================================================*
      * LINKAGE COPYBOOK FOR SCWT_PALLET TO BE USED BY:
      *     SCWPALC.sqb
      *     SCWPALCM.sqb
      *
       01 PALC-PARMS.
         05 PALC-CPY-CD          like aa-cpy-cde
                                      get-property  set-property.         
         05 PALC-PAL-FACL        like aa-facl
                                      get-property  set-property.         
         05 PALC-PAL-WHSE        like aa-whse
                                      get-property  set-property.         
         05 PALC-OWNER           like aa-owner
                                      get-property  set-property.         

         05 PALC-PAL-PID PIC G(18)
                                      get-property  set-property.
         
      *  05 PALC-PAL-PID         like aa-pallet-id
      *                                get-property  set-property.
         05 PALC-PAL-TYPE        PIC G(1)
                                      get-property  set-property.         
         05 PALC-PAL-INV-TYPE    PIC G(1)
                                      get-property  set-property.         
         05 PALC-PAL-INV-STAT    PIC G(1)
                                      get-property  set-property.         
         05 PALC-PAL-CUR-SLOT    PIC G(15)
                                      get-property  set-property.         
         05 PALC-PAL-CUST        PIC G(14)
                                      get-property  set-property.         
         05 PALC-PAL-TO-TRADER-TYPE 
                                 PIC G(02) 
                                      get-property  set-property.                                          
         05 PALC-PAL-SHIP-DATE   PIC G(10)
                                      get-property  set-property.         
         05 PALC-PAL-ROUTE       PIC G(3)
                                      get-property  set-property.         
         05 PALC-PAL-STOP        PIC G(2)
                                      get-property  set-property.         
         05 PALC-PAL-LOAD-PT     PIC G(15)
                                      get-property  set-property.         
         05 PALC-SECTION         PIC G(2)
                                      get-property  set-property.         
         05 PALC-PAL-VEND-PID    PIC G(18)
                                      get-property  set-property.         
         05 PALC-BEING-RECEIVED  PIC G(1)
                                      get-property  set-property.         
         05 PALC-PALLET-POSITION PIC G(1)
                                      get-property  set-property.         
         05 PALC-OPTION          PIC G(1)
                                      get-property  set-property.    
                                           
         05 PALC-UPDATE-SERIAL PIC G(18)
                                      get-property  set-property.
         
      *  05 PALC-UPDATE-SERIAL   LIKE AA-UPDATE-SERIAL
      *                               get-property  set-property.
      
         05 PALC-PALLET-SEQ PIC G(18)
                                      get-property  set-property.
         
      *  05 PALC-PALLET-SEQ      LIKE AA-PALLET-SEQ
      *                                get-property  set-property.
         05 PALC-TRANSACTION     LIKE AA-TRAN 
                                      get-property  set-property.         
         05 PALC-CYCLE          PIC GGG                             
                                      get-property  set-property.                  
                                      
       01 PALC-PAL-PID-PICS9 like aa-pallet-id  .
       01 PALC-PALLET-SEQ-PICS9      LIKE AA-PALLET-SEQ.
       01 PALC-UPDATE-SERIAL-PICS9   LIKE AA-UPDATE-SERIAL.
       01 PALC-PAL-VEND-PID-PICS9 PIC S9(18) COMP-5.
         
                                      