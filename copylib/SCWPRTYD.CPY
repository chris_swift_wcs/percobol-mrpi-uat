       01 SCWT-PR-TYPE.
          05 CPY-CD PIC G(5).
          05 PR-TYPE PIC G(1).
          05 PR-TYPE-DESC PIC G(15).
          05 PR-FUNC-TYPE PIC G(1).
          05 ALLOW-ADD PIC G(1).
          05 ALLOW-CHG PIC G(1).
          05 ALLOW-DEL PIC G(1).
          05 DEFAULT-PACK-CODE PIC G(2).
          05 PALLET-RCV-TIME PIC S9(9) COMP-5.
          05 MAINT-DATE PIC G(10).
          05 MAINT-TIME PIC G(8).
          05 MAINT-USER PIC G(10).
          05 MAINT-TRAN PIC G(10).
          05 PUTAWAY-RULE PIC G(3).
          05 UPDATE-SERIAL PIC S9(18) COMP-5.
