       01 SCWT-ROUTE.
          05 CPY-CD PIC G(5).
          05 FACL PIC G(1).
          05 WHSE PIC G(2).
          05 ROUTE PIC G(3).
          05 TRUCK PIC G(5).
          05 UPDATE-SERIAL PIC S9(18) COMP-5.
          05 MAINT-DATE PIC G(10).
          05 MAINT-TIME PIC G(8).
          05 MAINT-USER PIC G(10).
          05 MAINT-TRAN PIC G(10).
