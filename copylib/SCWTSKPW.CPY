      *================================================================* 
      ** This working storage copybook contains the working storage fields 
      ** required to process the objects passed to the receiving process 
      ** nodes. 
      *================================================================* 
      *                   COPYBOOK CHANGE HISTORY                      * 
      *----------------------------------------------------------------* 
      *  CHANGE �CHANGE �                                              * 
      *   DATE  �NUMBER �      DETAILED CHANGE DESCRIPTION             * 
      *----------------------------------------------------------------*  
      * 10OCT06 |..-....| New Copybook                                 *
      * 10MAY10 |09-0824a Increase length of task type to 3 characters *  
      *================================================================*
      
       01 TASK-CONTENTS                 OBJECT REFERENCE 
          "com.aquitecintl.ewms.warehouse.interfaces.TaskPalletVO"
               
          GET-PROPERTY SET-PROPERTY.
      **** cdidbcs *****
       01 TASK-CONTENTS-VOU                OBJECT REFERENCE 
          "com.aquitecintl.ewms.warehouse.cobolvoutil.TaskPalletVO" .     
      ******************
       01 WS-TASK-CONTENT               OBJECT REFERENCE 
          "com.aquitecintl.ewms.warehouse.interfaces.TaskPalletVO".
       01 TASK-LIST                     OBJECT REFERENCE 
          "java.util.ArrayList"
          GET-PROPERTY SET-PROPERTY.
                       
       01 TASK-SIZE            PIC S9(09) COMP-5.
       01 TASK-COUNT           PIC S9(09) COMP-5. 

               
      ** The following fileds are built to be used with TaskVO
       01 WS-TASK-CONTENTS.
          05 TC-CPY-CD                  PIC G(5).
          05 TC-TASK-ID                 PIC S9(9) COMP-5.
      *&10 Begin 09-0824a 
      *&1005 TC-TASK-TYPE               PIC G(1).
          05 TC-TASK-TYPE               PIC G(3).
      *&10 End   09-0824a 
          05 TC-TASK-STATUS             PIC G(1).
          05 TC-EMPLOYEE-CODE           PIC G(10).
          05 TC-ASSIGNMENT              PIC S9(9) COMP-5.
          05 TC-FROM-AREA               PIC G(15).
          05 TC-TO-AREA                 PIC G(15).
          05 TC-AREA-BASIS              PIC G(1).
          05 TC-FROM-LOCATION           PIC G(15).
          05 TC-TO-LOCATION             PIC G(15).
          05 TC-FROM-XCOR               LIKE AA-XYZ-CORDS.
          05 TC-FROM-YCOR               LIKE AA-XYZ-CORDS.
          05 TC-FROM-ZCOR               LIKE AA-XYZ-CORDS.
          05 TC-TO-XCOR                 LIKE AA-XYZ-CORDS.
          05 TC-TO-YCOR                 LIKE AA-XYZ-CORDS.
          05 TC-TO-ZCOR                 LIKE AA-XYZ-CORDS.
          05 TC-ITEM-OWNER              PIC G(5).
          05 TC-ITEM-CODE               PIC G(14).
          05 TC-ITEM-PACK-CODE          PIC G(2).
          05 TC-FROM-FLAG               PIC G(1).
          05 TC-TO-FLAG                 PIC G(1).
          05 TC-DEFAULT-REASON          PIC G(1).
          05 TC-ORIG-PALT-QTY           PIC S9(9) COMP-5.
          05 TC-PALLET-QTY              PIC S9(9) COMP-5.
          05 TC-NO-PALLETS              PIC S9(9) COMP-5.
          05 TC-PICK-RELEASE            PIC S9(9) COMP-5.
          05 TC-SHIP-DATE               PIC G(10).
          05 TC-RELEASE-CYCLE           PIC G(2).
          05 TC-RELEASE-WAVE            PIC G(2).
          05 TC-RELEASE-BSEQ            PIC S9(9) COMP-5.
          05 TC-LETDOWN-POINT           PIC S9(9) COMP-5.
          05 TC-CUSTOMER-CODE           PIC G(14).
          05 TC-TASK-GROUP              PIC S9(9) COMP-5.          
          05 TC-FORKLIFT-TYPE           PIC G(2).
          05 TC-TO-VERIF                PIC G(15).
          05 TC-FROM-VERIF              PIC G(15).
          05 TC-PR                      PIC S9(9) COMP-5.
          05 TC-TASK-FACL               PIC G(1).
          05 TC-TASK-WHSE               PIC G(2).         
          05 TC-ITEM-FACL               PIC G(1).
          05 TC-ITEM-WHSE               PIC G(2).
          05 TC-FROM-COMMINGLE          PIC G(1).
          05 TC-TO-COMMINGLE            PIC G(1).
          05 TC-FROM-CONFIG             PIC G(1).
          05 TC-TO-CONFIG               PIC G(1).
          05 TC-TASK-PRIORITY           PIC S9(9) COMP-5.
          05 TC-READY-TIME              PIC G(8).
          05 TC-READY-DATE              PIC G(10).
          05 TC-START-TIME              PIC G(8).
          05 TC-START-DATE              PIC G(10).
          05 TC-COMPLETE-TIME           PIC G(8).
          05 TC-COMPLETE-DATE           PIC G(10).
          05 TC-PR-STACK-FLAG           PIC G(1).
          05 TC-FROM-SLOT-TYPE          PIC G(1).
          05 TC-TO-SLOT-TYPE            PIC G(1).
          05 TC-SLOT-MOVE-TYPE          PIC G(1).
          05 TC-EXCEPTION-CODE          PIC G(2).
          05 TC-WHSE-DEPT               PIC G(2).
          05 TC-TOTAL-TVL-TIME          LIKE AA-TRAVEL-TIME.
          05 TC-TOTAL-STD-TIME          LIKE AA-STANDARD-TIME.
          05 TC-TOTAL-ACT-TIME          LIKE AA-STANDARD-TIME.
          05 TC-FROM-POS                PIC G(1).
          05 TC-TO-POS                  PIC G(1).
          05 TC-TO-SECT                 PIC G(2).
          05 TC-LOT-CODE                PIC G(20).
          05 TC-CREATE-TRAN-ID          PIC S9(9) COMP-5.
          05 TC-PRE-TASK-ID             PIC S9(9) COMP-5.
          05 TC-PR-REFERENCE            PIC S9(9) COMP-5.
          05 TC-NEXT-TASK               PIC S9(9) COMP-5.
          05 TC-LMS-SECT                PIC G(2).
          05 TC-LMS-STDS                PIC G(1).
          05 TC-LMS-METHOD              PIC G(1).
          05 TC-PR-LINE                 PIC S9(9) COMP-5.
          05 TC-LINES-ON-PALLET         PIC S9(9) COMP-5.
          05 TC-ITEM-DESC               PIC G(30).
          05 TC-ITEM-SIZE               PIC G(15).
          05 TC-LMS-SEQ                 PIC S9(9) COMP-5.
          05 TC-LMS-EXCEPTION           PIC G(1).
          05 TC-LMS-GROUP               PIC S9(9) COMP-5.
          05 TC-PALLET-STACK            PIC S9(9) COMP-5.
          05 TC-FROM-LOCATION-TYPE      PIC G(2).
          05 TC-TO-LOCATION-TYPE        PIC G(2).         
          05 TC-FROM-AISLE              PIC G(3).
          05 TC-FROM-BAY                PIC S9(9) COMP-5.
          05 TC-TO-AISLE                PIC G(3).
          05 TC-TO-BAY                  PIC S9(9) COMP-5.
          05 TC-UPDATE-SERIAL           PIC S9(9) COMP-5.
          05 TC-PALLET-SEQ              PIC S9(18) COMP-5.
          05 TC-CUSTOMER-COST           LIKE AA-COST.
          05 TC-PLAN-SHIPMENT           PIC S9(9) COMP-5.
          05 TC-RESTOCK-ASSIGN          PIC S9(9) COMP-5.
          05 TC-FROM-SECTION            PIC G(2).
          05 TC-MAINT-DATE              LIKE AA-DATE.
          05 TC-MAINT-TIME              LIKE AA-TIME.
          05 TC-MAINT-USER              LIKE AA-USER.
          05 TC-MAINT-TRAN              LIKE AA-TRAN.
          05 TC-FROM-PICK-SEQN          PIC S9(9) COMP-5.
          05 TC-TO-PICK-SEQN            PIC S9(9) COMP-5.
          05 TC-FULFILMENT-TYPE         PIC G.   
          05 TC-EXCEP-TASK-ID           PIC S9(9) COMP-5.
          05 TC-DEFERED-TASK            PIC G.   
          05 TC-FROM-EXCEPTION-CODE     PIC G(2).
          05 TC-TO-EXCEPTION-CODE       PIC G(2).
         
                  