      * This copybook includes the parameters to call the program SCWPKQC
      *================================================================* 
      *                   COPYBOOK CHANGE HISTORY                      * 
      *----------------------------------------------------------------* 
      *  CHANGE �CHANGE �                                              * 
      *   DATE  �NUMBER �      DETAILED CHANGE DESCRIPTION             * 
      *----------------------------------------------------------------*  
      * 20JUN02 |..-....| New Copybook                                 *
      *         |       |                                              *   
      *================================================================*
          
        01 pkqc-parms.
           05 pkqc-cpy-cd         like aa-cpy-cde.
           05 pkqc-facl           like aa-facl.
           05 pkqc-whse           like aa-whse.
           05 pkqc-owner          like aa-owner.
           05 pkqc-item           like aa-item-code.
           05 pkqc-pack1          like aa-pack-code.
           05 pkqc-pack2          like aa-pack-code.
           05 pkqc-qty1           pic s9(9) comp-5 VALUE 0.
           05 pkqc-qty2           pic s9(9) comp-5 VALUE 0.
           05 pkqc-weight1        pic s9(6)v9(3) comp-5 VALUE 0.
           05 pkqc-weight2        pic s9(6)v9(3) comp-5 VALUE 0. 
           05 pkqc-return         PIC G(10).


