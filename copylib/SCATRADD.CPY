       01 SCWA-TRADER.
          05 CPY-CD PIC G(5).
          05 FACL PIC G(1).
          05 WHSE PIC G(2).
          05 TRADER-CODE PIC G(14).
          05 TRADER-TYPE PIC G(2).
          05 ADDRESS-1 PIC G(30).
          05 ADDRESS-2 PIC G(30).
          05 ADDRESS-3 PIC G(30).
          05 CITY PIC G(30).
          05 STATE-REGION PIC G(2).
          05 POSTAL-CODE PIC G(15).
          05 PHONE PIC G(20).
          05 FAX PIC G(20).
          05 EMAIL PIC G(50).
          05 URL PIC G(50).
          05 ANA-TRADER-ID PIC S9(18) COMP-5.
          05 TRADER-NAME PIC G(30).
          05 COUNTRY-CODE PIC G(3).
          05 UPDATE-SERIAL PIC S9(18) COMP-5.
          05 MAINT-DATE PIC G(10).
          05 MAINT-TIME PIC G(8).
          05 MAINT-USER PIC G(10).
          05 AUDIT-ACTION PIC G(2).
          05 AUDIT-DATE PIC G(10).
          05 AUDIT-TIME PIC G(8).
          05 AUDIT-USER PIC G(10).
          05 AUDIT-TRAN PIC G(10).
          05 AUDIT-IFLAG PIC G(1).
