       IDENTIFICATION DIVISION.
       PROGRAM-ID. SCWIMHT.
      ******************************************************************
      *                                                                *
      *  Copyright 2002 Aquitec International, Inc.                    *
      *  All rights reserved                                           *
      * ---------------------------------------------------------------*
      *  Version 1  Release  0                                         *
      *                                                                *
      ******************************************************************
      
      *================================================================* 
      * PROGRAM/MODULE  NAME........SCWIMHT.                           *
      *             Interface for Material Handling Equipment          *
      * DESIGNED BY.........Aquitec Inc.                               *
      * PROGRAMMED BY.......Aquitec Inc.                               *
      * Date coded..........12 February 2003                           *
      * System..............eWarehousing                               *
      * Frequency used......ON REQUEST                                 *
      *                                                                *
      * Input Tables.                                                  *
      * -------------                                                  *      
      * SCWT_TASK                                                      *
      * SCWT_MESSAGE_TEXT                                              *      
      *                                                                *      
      * Input Output Tables.                                           *      
      * --------------------                                           *      
      *                                                                *
      * Output Tables                                                  *        
                
      * --------------                                                 *
      *                                                                *
      * Processing/Description                                         *
      * ----------------------                                         *
      *  Input interface that receives information that a task         *
      *  has been completed.                                           *
      ******************************************************************

      *================================================================* 
      *                   PROGRAM CHANGE HISTORY                       * 
      *----------------------------------------------------------------* 
      *  CHANGE �CHANGE �                                              * 
      *   DATE  �NUMBER �      DETAILED CHANGE DESCRIPTION             * 
      *----------------------------------------------------------------* 
      * 12FEB03 |..-....| New Program                                  *
      *         |       |                                              *   
      *================================================================*
       ENVIRONMENT DIVISION.
      *--------------------- 
      *
       DATA DIVISION. 
      *--------------
      **************************************************************************
      *    W o r k i n g   S t o r a g e   C o p y b o o k s                   *
       
      **************************************************************************
       WORKING-STORAGE SECTION.
      *------------------------
      *** General copybook that holds field sizes.                
       COPY "SCWAAAW.cpy". 
      *** Testing Flag. 
      ** CDIDBCS ** 
       01  INT-VAL    object reference 
          "com.aquitecintl.ewms.common.facades.DateService". 
       
      ****************************************************************          
         
       01  WF-DEBUG                     PIC G          VALUE N"Y"
           get-property  set-property.            
      *
       01  WX-CURRENT-PARAGRAPH         PIC G(30)      VALUE " ".
       01  user-lang-code               PIC G(2) VALUE N"EN".
       01  WX-CURRENT-SQL-STMT          PIC GG.
       01  W9-CURRENT-SQL-STMT REDEFINES WX-CURRENT-SQL-STMT
                                        PIC 99         VALUE 0.           
       01  WX-ERROR-METHOD              LIKE AA-INVOKE-ERROR.
       01  W9-ORDER-LINE-COUNT          PIC S9(9) COMP-5 VALUE ZERO.  
       01  WF-COMP-COUNT                PIC S9(9) COMP-5 VALUE ZERO.  
       01  WF-OWNER-COUNT               PIC S9(9) COMP-5 VALUE ZERO.
       01  WX-DATE                      PIC G(10).  
      *              
       01  TASK-COMPLETION-DETAILS          object reference 
       "com.aquitecintl.ewms.common.impex.transformers.voext.TaskComplet
      -"ionIntVOExt"
                                        get-property set-property.  
                                        
      **** cdidbcs ******
       01  TASK-COMPLETION-DETAILS-VOU          object reference 
       "com.aquitecintl.ewms.common.cobolvoutil.TaskCompletionIntVOExt".
      *******************                                                       
         
      
       01  WX-TASK-COMPLETE-LIST.          
           05 TCL-CPY-CD                LIKE AA-CPY-CDE.
           05 TCL-TASK-FACL             LIKE AA-FACL.
           05 TCL-TASK-WHSE             LIKE AA-WHSE.
           05 TCL-TASK-ID               LIKE AA-INTEGER.
           05 TCL-EMPLOYEE              PIC G(10).
      * 
       01  WX-TASK-VALIDITY             PIC G.
           88 WX-TASK-INVALID         VALUE N"N".
           88 WX-TASK-VALID           VALUE N"Y".                               
                                                 
      * 
       01  LOG-ERROR-FIELDS             PIC G(1024)     VALUE SPACE.
      *
       01  LG-ERRORS.
           05  LG-LOG-DATE              LIKE AA-DATE.
           05  LG-LOG-TIME              LIKE AA-TIME.
           05  LG-CPY-CD                LIKE AA-CPY-CDE.
           05  LG-FACL                  LIKE AA-FACL.
           05  LG-WHSE                  LIKE AA-WHSE.
           05  LG-USERNAME              PIC G(32).
           05  LG-ERROR-TYPE            PIC G(32).
           05  LG-ERROR-CODE            PIC G(10).
           05  LG-ERROR-DATA.
               10  LG-ERR-MSG-TEXT      PIC G(48).      
               10  LG-DASH              PIC G(3) VALUE N" - ".
               10  LG-CODE-ID           PIC G(20).
               10  LG-EXTRA             PIC G(1977).
           05  LG-UPDATE-SERIAL         LIKE AA-UPDATE-SERIAL.
           05  LG-LOG-SEQUENCE          LIKE AA-UPDATE-SERIAL.       
      *        
      **************************************************************************
      *         D a t a b a s e     T a b l e s                                *
      **************************************************************************
      *                
       COPY "SCWTASKD.cpy".
      * 
      **************************************************************************
      *    O t h e r   W o r k i n g   S t o r a g e   C o p y b o o k s       *
       
      *********************************** **************************************
      *** Standard error return code                                
       COPY "SCWSQLEL.cpy".   
      *** Sqlca                                                  
       EXEC SQL        INCLUDE SQLCA.cpy    END-EXEC.
      *
      *** Java Node Copybook                                      
       COPY "SCWNODE.cpy".    
      *
       COPY "SCWCDATL.cpy".
      *  
       COPY "SCWDATEL.cpy".  
      * 
      *** Receiving Inventory Update linkage
       COPY "SCWRFC2L.cpy".       
      * 
      *** SCWT_MESSAGE_TEXT copybook
       COPY "SCWMESSD.cpy".
      *       
      **************************************************************************
      **************************************************************************
      ***                                                                    ***
      ***    P R O C E D U R E   D I V I S I O N                             ***
      ***                                                                    ***
      **************************************************************************
      **************************************************************************
       PROCEDURE DIVISION.
      **************************************************************************
                   
       0000-IMHT-MAIN-PROC SECTION.
       0000-IMHT-MAIN-PROC-START.
           MOVE N"0000-IMHT-MAIN-PROC"  TO WX-CURRENT-PARAGRAPH.       
                      
           IF  WF-DEBUG = N"Y"
               DISPLAY "******************************************"         
               DISPLAY "*  SCWIMHT  STARTED                      *" 
               DISPLAY "******************************************"             
        
           END-IF.    

      ********************************************     
      *** CDIDBCS***
      ********************************************     
            INVOKE "com.aquitecintl.ewms.common.facades.DateService"
            "getInstance"               RETURNING INT-VAL
           ON EXCEPTION
               MOVE "INVK0002"         TO ERR-RTN-CODE           
               MOVE "getInstance"      TO WX-ERROR-METHOD
               PERFORM  9900-IMHT-STOP
           END-INVOKE.   
      ********************************************
      ******************      
      
        INVOKE INT-VAL "getParsedString"
                                        USING RFC2-TASK-ID
                               RETURNING RFC2-TASK-ID-PICS9  
        INVOKE INT-VAL "getParsedString"
                                        USING RFC2-CURR-ASSIGN-NUM
                               RETURNING RFC2-CURR-ASSIGN-NUM-PICS9  
        INVOKE INT-VAL "getParsedString"
                                        USING RFC2-UPDATE-SERIAL
                               RETURNING RFC2-UPDATE-SERIAL-PICS9  
      
           PERFORM 1000-INITIALIZE.      
          
           PERFORM 2000-MAIN-PROCESS.
          
           PERFORM 9900-IMHT-STOP.
           
       0000-IMHT-EXIT. 
           EXIT.
      **************************************************************************
                   
       1000-INITIALIZE SECTION.
       1000-INITIALIZE-START.
           MOVE N"1000-INITIALIZE"      TO WX-CURRENT-PARAGRAPH.       
           IF  WF-DEBUG = N"Y" OR N"P"
               DISPLAY WX-CURRENT-PARAGRAPH
           END-IF. 
                         
      *     INITIALIZE ERR-RETURN-CODE.
      *     MOVE ZEROES                 TO ERR-RTN-CODE.     
            MOVE N"0000000000"          TO ERR-RTN-CODE.
      *** Create a connetion to DB2          
           COPY "SCWCNCTP.cpy".
           COPY "SCWCNTDB.cpy".       
           
      *** Retrieve the current date for the system             
            CALL "SCWCDAT" USING CDAT-PARMS
            ON OVERFLOW
                MOVE  N"IMHT1004"       TO ERR-RTN-CODE
                MOVE N"Y"               TO ERR-RTN-POISON
                PERFORM 6000-LOG-ERROR
            END-CALL.
            CANCEL "SCWCDAT".           
                  
       1000-EXIT.
           EXIT.                  
      **************************************************************************
                   
       2000-MAIN-PROCESS SECTION.       
       2000-MAIN-PROCESS-START.
           MOVE N"2000-MAIN-PROCESS"  TO WX-CURRENT-PARAGRAPH.       
           IF  WF-DEBUG = N"Y" OR N"P"
               DISPLAY WX-CURRENT-PARAGRAPH
           END-IF. 
           
           PERFORM 3000-GET-VO-DETAILS. 
           PERFORM 4000-VALIDATE-TASK.
           IF  WX-TASK-VALID
               PERFORM 5000-COMPLETE-TASK
           ELSE 
               PERFORM 6000-LOG-ERROR
           END-IF.
      
       2000-EXIT.
           EXIT.                  
      **************************************************************************
             
       3000-GET-VO-DETAILS SECTION.
       3000-GET-VO-DETAILS-START.
           MOVE N"3000-GET-VO-DETAILS"  TO WX-CURRENT-PARAGRAPH.       
           IF  WF-DEBUG = N"Y" OR N"P"
               DISPLAY WX-CURRENT-PARAGRAPH
           END-IF. 
           
      *** cdidbcs:
      **  Add the object to the wrapper class and for string objects get the
      *   expanded string from the wrapper class

	   INVOKE 
	  "com.aquitecintl.ewms.common.cobolvoutil.TaskCompletionIntVOEx
      -"t"
            "getInstance"               RETURNING
                                        TASK-COMPLETION-DETAILS-VOU      
           Invoke TASK-COMPLETION-DETAILS-VOU "setInstance" USING
                                 TASK-COMPLETION-DETAILS           
                      
      *         INVOKE TASK-COMPLETION-DETAILS "getCompanyCode"
               INVOKE TASK-COMPLETION-DETAILS-VOU "getCompanyCode"
                                         RETURNING TCL-CPY-CD 
                ON EXCEPTION
                   MOVE N"INVK0003"             TO ERR-RTN-CODE           
                   MOVE N"getCompanyCode"       TO WX-ERROR-METHOD
                   PERFORM 6000-LOG-ERROR
               END-INVOKE.

      *
      *         INVOKE TASK-COMPLETION-DETAILS "getFacility"
               INVOKE TASK-COMPLETION-DETAILS-VOU "getFacility"
                                         RETURNING TCL-TASK-FACL
                ON EXCEPTION
                   MOVE N"INVK0003"             TO ERR-RTN-CODE           
                   MOVE N"getFacility"          TO WX-ERROR-METHOD
                   PERFORM 6000-LOG-ERROR
               END-INVOKE.
      *       
      *         INVOKE TASK-COMPLETION-DETAILS "getWarehouse"
               INVOKE TASK-COMPLETION-DETAILS-VOU "getWarehouse"
                                         RETURNING TCL-TASK-WHSE
                ON EXCEPTION
                   MOVE N"INVK0003"             TO ERR-RTN-CODE           
                   MOVE N"getWarehouse"         TO WX-ERROR-METHOD
                   PERFORM 6000-LOG-ERROR
               END-INVOKE.

      *
      *         INVOKE TASK-COMPLETION-DETAILS "getTaskID"
               INVOKE TASK-COMPLETION-DETAILS-VOU "getTaskID"
                                         RETURNING TCL-TASK-ID
                ON EXCEPTION
                   MOVE N"INVK0003"             TO ERR-RTN-CODE           
                   MOVE N"getTaskID"            TO WX-ERROR-METHOD
                   PERFORM 6000-LOG-ERROR
               END-INVOKE.
      *
      *         INVOKE TASK-COMPLETION-DETAILS "getEmployee"
               INVOKE TASK-COMPLETION-DETAILS-VOU "getEmployee"
                                         RETURNING TCL-EMPLOYEE
                ON EXCEPTION
                   MOVE N"INVK0003"             TO ERR-RTN-CODE           
                   MOVE N"getEmployee"          TO WX-ERROR-METHOD
                   PERFORM 6000-LOG-ERROR
               END-INVOKE.      
        
      *                    
       3000-EXIT.
           EXIT.    
      **************************************************************************
       4000-VALIDATE-TASK SECTION.
       4000-VALIDATE-TASK-START.
           MOVE N"4000-VALIDATE-TASK" TO WX-CURRENT-PARAGRAPH.       
           IF  WF-DEBUG = N"Y" OR N"P"
               DISPLAY WX-CURRENT-PARAGRAPH
           END-IF. 
                         
      *** Confirm that task-id from VO exists on SCWT_TASK table
           EXEC sql
               SELECT to_location
                     ,update_serial
                 INTO :scwt-task.to-location
                     ,:scwt-task.update-serial
                 FROM scwt_task 
                WHERE cpy_cd            = :tcl-cpy-cd
                  AND task_facl         = :tcl-task-facl
                  AND task_whse         = :tcl-task-whse
                  AND task_id           = :tcl-task-id
          END-EXEC.      
         
          EVALUATE SQLCODE
            WHEN 0
              SET WX-TASK-VALID          TO TRUE
            WHEN +100
              MOVE N"IMHT1001"            TO ERR-RTN-CODE            
              SET WX-TASK-INVALID        TO TRUE
            WHEN OTHER
              MOVE N"IMHT1003"            TO ERR-RTN-CODE
              PERFORM 6000-LOG-ERROR          
          END-EVALUATE.                     
 
       4000-EXIT.
           EXIT.
      **************************************************************************
       5000-COMPLETE-TASK SECTION.    
       5000-COMPLETE-TASK-START.
           MOVE N"5000-COMPLETE-TASK" TO WX-CURRENT-PARAGRAPH.       
           IF  WF-DEBUG = N"Y" OR N"P"
               DISPLAY WX-CURRENT-PARAGRAPH
           END-IF. 
                    
      *** Set up SCWRFC2 parameters  
          MOVE TCL-CPY-CD                TO RFC2-CPY-CD.
          MOVE TCL-TASK-FACL             TO RFC2-FACL.
          MOVE TCL-TASK-WHSE             TO RFC2-WHSE.
          MOVE TCL-TASK-ID               TO RFC2-TASK-ID-PICS9.
          MOVE TO-LOCATION OF SCWT-TASK  TO RFC2-TO-LOCATION.
          INITIALIZE RFC2-EXCEPTION-CODE
                     RFC2-CURR-ASSIGN-NUM-PICS9.
          MOVE N"N"                       TO RFC2-RF-FLAG.
          MOVE UPDATE-SERIAL OF SCWT-TASK TO RFC2-UPDATE-SERIAL-PICS9.
        INVOKE INT-VAL "expandString"
                                        USING RFC2-TASK-ID-PICS9
                               RETURNING RFC2-TASK-ID  
        INVOKE INT-VAL "expandString"
                                        USING RFC2-CURR-ASSIGN-NUM-PICS9
                               RETURNING RFC2-CURR-ASSIGN-NUM  
        INVOKE INT-VAL "expandString"
                                        USING RFC2-UPDATE-SERIAL-PICS9
                               RETURNING RFC2-UPDATE-SERIAL  
      *** Call SCWRFC2 to complete the task
          CALL "SCWRFC2" USING RFC2-PARMS ERR-RETURN-CODE
            ON OVERFLOW
               MOVE  N"IMHT1002"    TO ERR-RTN-CODE
               MOVE N"Y"            TO ERR-RTN-POISON
               PERFORM 6000-LOG-ERROR
          END-CALL
      *****CDIDBCS-START-ECOBOL1************
        INVOKE INT-VAL "getParsedString"
                                        USING RFC2-TASK-ID
                               RETURNING RFC2-TASK-ID-PICS9
      *****************     
        INVOKE INT-VAL "getParsedString"
                                        USING RFC2-CURR-ASSIGN-NUM
                               RETURNING RFC2-CURR-ASSIGN-NUM-PICS9
      *****************     
        INVOKE INT-VAL "getParsedString"
                                        USING RFC2-UPDATE-SERIAL
                               RETURNING RFC2-UPDATE-SERIAL-PICS9
      *****CDIDBCS-END-ECOBOL1************
      
          IF ERR-RTN-CODE NOT =  N"0000000000"
              MOVE ERR-RTN-CODE        TO NODE-RESULT-FIELD
              MOVE ERR-RTN-DATA        TO NODE-ERROR-MESSAGE-DATA       
              MOVE ERR-RTN-POISON      TO NODE-POISON-MESSAGE-FLAG
              PERFORM 6000-LOG-ERROR
          END-IF              
        
       5000-EXIT.
           EXIT.                  
      **************************************************************************
                        
       6000-LOG-ERROR SECTION.
       6000-LOG-ERROR-START.    
           MOVE N"6000-LOG-ERROR"       TO WX-CURRENT-PARAGRAPH.                
        
           IF  WF-DEBUG = N"Y" OR N"P"
               DISPLAY WX-CURRENT-PARAGRAPH
           END-IF. 
           
      *** This section will pass any errors the the logging service 
      *** which will insert rows into the LOG_FK_ERROR table.
           MOVE SPACE                  TO LOG-ERROR-FIELDS. 
        
     *** Get USER DEFAULT LANGUAGE CODE.
           EXEC SQL
               SELECT DEFAULT_LANG_CODE
                 INTO :user-lang-code
                 FROM USERS
                WHERE USERNAME      = :NODE-USER
                AND CPY_CD = :NODE-COMPANY-CODE
           END-EXEC.

      *** Get message text details.
           EXEC SQL
               SELECT message_text
                 INTO :lg-err-msg-text
                 FROM scwt_message_text
                WHERE message_id         = :err-rtn-code
                AND SYSTEM_LANGUAGE = :user-lang-code
           END-EXEC.

           MOVE 04                     TO W9-CURRENT-SQL-STMT.
           
           EVALUATE SQLCODE
             WHEN 0
               CONTINUE
             WHEN 100
               MOVE N"ERROR MESSAGE NOT FOUND."
                                       TO LG-ERROR-DATA
             WHEN OTHER 
               MOVE N"IMHT1003"         TO ERR-RTN-CODE
               PERFORM 9500-POISON-ERROR-AND-EXIT
           END-EVALUATE.
           
           MOVE CDAT-DATE-10           TO LG-LOG-DATE.
           MOVE CDAT-TIME-11           TO LG-LOG-TIME.
           MOVE TCL-CPY-CD             TO LG-CPY-CD.
           MOVE TCL-TASK-FACL          TO LG-FACL.
           MOVE TCL-TASK-WHSE          TO LG-WHSE.
           MOVE NODE-USER              TO LG-USERNAME.
           MOVE N"INTF"                 TO LG-ERROR-TYPE.
           MOVE ERR-RTN-CODE           TO LG-ERROR-CODE.
           MOVE TCL-TASK-ID            TO LG-CODE-ID.
           
           STRING N"log_date:"           LG-LOG-DATE         N"|"
                  N"log_time:"           LG-LOG-TIME         N"|"
                  N"cpy_cd:"             LG-CPY-CD           N"|"
                  N"facl:"               LG-FACL             N"|"
                  N"whse:"               LG-WHSE             N"|"
                  N"username:"           LG-USERNAME         N"|"
                  N"error_type:"         LG-ERROR-TYPE       N"|"
                  N"error_code:"         LG-ERROR-CODE       N"|"
                  N"error_data:"         LG-ERROR-DATA       N"|"               
         
                                        DELIMITED BY SIZE
                                        INTO LOG-ERROR-FIELDS.

           IF  WF-DEBUG = N"Y"                                    
               DISPLAY " ERROR-LOG " LOG-ERROR-FIELDS
           END-IF.                                       

      *** Get the LogService to pass SCWT_LOG_ERROR.
           INVOKE NODE-LOG-SERVICE "logUnconditional" 
                                    USING NODE-SESSION,
                                          "SCWT_LOG_ERROR",
                                          ERR-RTN-CODE,
                                          LOG-ERROR-FIELDS
             ON EXCEPTION
               MOVE N"INVK0002"         TO ERR-RTN-CODE           
               MOVE N"logUnconditional" 
                                       TO WX-ERROR-METHOD
               PERFORM 9500-POISON-ERROR-AND-EXIT
           END-INVOKE.
           
           MOVE SPACE                  TO ERR-RTN-CODE.

           GO TO 9900-IMHT-STOP.
      
       6000-EXIT.
           EXIT.    
      **************************************************************************
             
       9500-POISON-ERROR-AND-EXIT SECTION.
       9500-POISON-ERROR-AND-EXIT-START.
       
           MOVE N"Y"                    TO ERR-RTN-POISON.
           MOVE SQLCODE                TO ERR-SQLCODE.
           MOVE SQLSTATE               TO ERR-SQLSTATE.      
           PERFORM 9700-SETUP-ERROR-RTN-DATA.
           GO TO 9900-IMHT-STOP.

       9500-EXIT.
           EXIT.
      **************************************************************************
             
       9600-ERROR-AND-EXIT SECTION.
       9600-ERROR-AND-EXIT-START.

           MOVE SQLCODE                TO ERR-SQLCODE.
           MOVE SQLSTATE               TO ERR-SQLSTATE.     
           PERFORM 9700-SETUP-ERROR-RTN-DATA.
           GO TO 9900-IMHT-STOP.

       9600-EXIT.
           EXIT.
      **************************************************************************
             
       9700-SETUP-ERROR-RTN-DATA SECTION.
       9700-SETUP-ERROR-RTN-DATA-START.

     *****************  
      ** CDIDBCS **
      *****************   
        INVOKE INT-VAL "expandString"
                                        USING RFC2-TASK-ID-PICS9
                               RETURNING RFC2-TASK-ID  
        INVOKE INT-VAL "expandString"
                                        USING RFC2-CURR-ASSIGN-NUM-PICS9
                               RETURNING RFC2-CURR-ASSIGN-NUM  
        INVOKE INT-VAL "expandString"
                                        USING RFC2-UPDATE-SERIAL-PICS9
                               RETURNING RFC2-UPDATE-SERIAL  
      *** Set up error data depending on error code
           MOVE SPACES                 TO ERR-RTN-DATA.
           EVALUATE ERR-RTN-CODE
             WHEN N"INVK0002"
             WHEN N"INVK0003"
             WHEN N"INVK0004"
               STRING N"Error in method " DELIMITED BY SIZE
                   WX-ERROR-METHOD       DELIMITED BY SIZE
                 INTO ERR-RTN-DATA
               END-STRING
             WHEN OTHER            
               STRING N"ST: "                 
                  ,SQLState               
                  ,N"-"                   
                  ,tcl-cpy-cd
                  ,N"-"                   
                  ,tcl-task-facl        
                  ,tcl-task-whse
                  ,N"-"                   
                  ,N"SQL Stmt-"           
                  ,WX-CURRENT-SQL-STMT  DELIMITED BY SIZE 
                 INTO ERR-RTN-DATA
                END-STRING
           END-EVALUATE.   

           MOVE ERR-RTN-CODE       TO NODE-RESULT-FIELD. 
           MOVE ERR-RTN-DATA       TO NODE-ERROR-MESSAGE-DATA.
           MOVE ERR-RTN-POISON     TO NODE-POISON-MESSAGE-FLAG.           

           IF WF-DEBUG = N"Y"
             DISPLAY "*************************************************"
             DISPLAY "*** DEBUG INFO    "
             DISPLAY "*** Paragraph Name = "   WX-CURRENT-PARAGRAPH
             DISPLAY "*** SQLCODE        = "   SQLCODE
             DISPLAY "*** SQLSTATE       = "   SQLSTATE
             DISPLAY "*** Current SQL Stmt = " WX-CURRENT-SQL-STMT
             DISPLAY "*** Err-Rtn-Code   = "   ERR-RTN-CODE
             DISPLAY "*** Err-Rtn-Data   = "   ERR-RTN-DATA
             DISPLAY "*************************************************"        
            
           END-IF.

       9700-EXIT.
           EXIT.
      **************************************************************************
             
       9900-IMHT-STOP SECTION.
       9900-IMHT-STOP-START.

      *****************  
      ** CDIDBCS **
      *****************   
        INVOKE INT-VAL "expandString"
                                        USING RFC2-TASK-ID-PICS9
                               RETURNING RFC2-TASK-ID  
                                       
        INVOKE INT-VAL "expandString"
                                        USING RFC2-CURR-ASSIGN-NUM-PICS9
                               RETURNING RFC2-CURR-ASSIGN-NUM  
        INVOKE INT-VAL "expandString"
                                        USING RFC2-UPDATE-SERIAL-PICS9
                               RETURNING RFC2-UPDATE-SERIAL  
           MOVE N"9900-IMHT-STOP" TO WX-CURRENT-PARAGRAPH.     
           IF  WF-DEBUG = N"Y" OR N"P"
               DISPLAY WX-CURRENT-PARAGRAPH
           END-IF. 
           
           IF  WF-DEBUG = N"Y" OR N"P"
               DISPLAY "******************************************"         
               DISPLAY "*  SCWIMHT  FINISHED                     *" 
               DISPLAY "******************************************"             
        
           END-IF.    
           
           GOBACK.
           
       COPY "SCWDBSTP.cpy".      
      **************************************************************************
                    
