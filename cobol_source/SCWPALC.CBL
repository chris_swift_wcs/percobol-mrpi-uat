       IDENTIFICATION DIVISION.
       PROGRAM-ID. SCWPALC.
      /
      ******************************************************************
      *                                                                *
      *  Copyright 2002 Aquitec International, Inc.                    *
      *  All rights reserved                                           *
      * ---------------------------------------------------------------*
      *  Version 1  Release  0                                         *
      *                                                                *
      ******************************************************************
      
      *================================================================* 
      * PROGRAM/MODULE  NAME........SCWPALC                            *
      *                     PALLET CREATE MODULE                       *
      * DESIGNED BY.........Aquitec Inc.                               *
      * PROGRAMMED BY.......Aquitec Inc.                               *
      * Date coded..........01 May 2002                                *
      * System..............eWarehousing                               *
      * Frequency used......ON REQUEST                                 *
      *                                                                *
      * Input Tables.                                                  *
      * ------------                                                   *
      *  SCWT_WHSE_PARMS                           (WHSP)              *
      *                                                                *
      * I-O Tables.                                                    *
      * ----------                                                     *
      *  PALLET TABLE                              (PALT)              *
      *                                                                *
      *  Processing/Description                                        *
      *  ----------------------                                        *
      *  This module will add a pallet onto the pallet table with no   *
      *  items assigned to it.                                         *
      *                                                                *
      ******************************************************************
      
      *================================================================* 
      *                   PROGRAM CHANGE HISTORY                       * 
      *----------------------------------------------------------------* 
      *  CHANGE �CHANGE �                                              * 
      *   DATE  �NUMBER �      DETAILED CHANGE DESCRIPTION             * 
      *----------------------------------------------------------------*  
      *28MAY2002|..-....| New Program                                  *
      * 21AUG07 |07-1168|Get debug flag from db rather than front end  *
      * 01MAY08 |08-0355|Getting wrong field for debug flag            *
      *================================================================*
           EJECT
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
      
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
      
      
       DATA DIVISION.
       FILE SECTION.
      
       WORKING-STORAGE SECTION.
       01  WF-DEBUG                     PIC G VALUE N"Y"
           GET-PROPERTY SET-PROPERTY.
       01  INT-VAL    object reference 
          "com.aquitecintl.ewms.common.facades.DateService".   
                   
           
      ****************************************************************
      *                                                              *
      *    2.  TEMPORARY FIELDS AND LSO STORAGE FIELDS               *
      *                                                              *
      ****************************************************************
      *   General copybook that holds field sizes.      
            COPY "SCWAAAW.cpy". 
       01  WX-ERROR-METHOD              LIKE AA-INVOKE-ERROR .
       01  W9-COUNT             LIKE AA-INTEGER.
      
      ****************************************************************
      *         Next number linkage                                  *
      ****************************************************************
       COPY "SCWNXTNL.cpy".
                
      ****************************************************************
      *                                                              *
      *        DECLARE SECTION                                       *
      *                                                              *
      ****************************************************************
      
           EXEC SQL        INCLUDE SQLCA.cpy    END-EXEC.
      
           EXEC SQL  BEGIN DECLARE SECTION  END-EXEC.
      * scwt_pallet copybook     
           EXEC SQL        INCLUDE SCWPALTD.cpy END-EXEC.
      * scwt_whse_parms copybook      
           EXEC SQL        INCLUDE SCWWHSPD.cpy END-EXEC. 
                 
           EXEC SQL  END   DECLARE SECTION  END-EXEC.
      
       COPY "SCWNODE.cpy". 
       LINKAGE SECTION.
      *  SCWT_PALLET TABLE.
           COPY "SCWPALCL.cpy".
                
      * Copybook to return to java any database errors received.
            COPY "SCWSQLEL.cpy".          
      
       PROCEDURE DIVISION USING PALC-PARMS ERR-RETURN-CODE.
       
       0000-PALC-MAIN SECTION.
       0000-PALC-START.
      
      *****************

         INVOKE "com.aquitecintl.ewms.common.facades.DateService"
            "getInstance"               RETURNING INT-VAL
           ON EXCEPTION
               MOVE "INVK0002"         TO ERR-RTN-CODE           
               MOVE "getInstance"      TO WX-ERROR-METHOD   
               PERFORM 9999-PALC-CANCEL-PROGRAM
           END-INVOKE.

      ******************      
      
        INVOKE INT-VAL "getParsedString"
                                        USING PALC-PAL-PID
                               RETURNING PALC-PAL-PID-PICS9  
                                       
        INVOKE INT-VAL "getParsedString"
                                        USING PALC-UPDATE-SERIAL
                               RETURNING PALC-UPDATE-SERIAL-PICS9  
        INVOKE INT-VAL "getParsedString"
                                        USING PALC-PALLET-SEQ
                               RETURNING PALC-PALLET-SEQ-PICS9  

      *     INITIALIZE ERR-RETURN-CODE.
      *     MOVE ZEROES                 TO ERR-RTN-CODE.
            MOVE N"0000000000"          TO ERR-RTN-CODE.

           MOVE N"N"                    TO ERR-RTN-POISON.

      * Create a connetion to DB2          
           Copy "SCWCNCTP.cpy".
           Copy "SCWCNTDB.cpy".
           EXEC SQL
               SELECT coalesce(debug_field,'N')
                INTO :wf-debug
                FROM scst_module_hdr
               WHERE module_name = 'SCWPALC'
           END-EXEC.
           IF WF-DEBUG                  = N"Y" OR N"P"
              DISPLAY "******************************************"
              DISPLAY "* SCWPALC STARTED                        *"
              DISPLAY "******************************************"
           END-IF   

      ******************      
      
        INVOKE INT-VAL "getParsedString"
                                        USING PALC-PAL-VEND-PID
                               RETURNING PALC-PAL-VEND-PID-PICS9  
           IF WF-DEBUG                  = N"Y"   
              DISPLAY "======    INPUT DATA    ======"
              DISPLAY "PALC-CPY-CD          :" PALC-CPY-CD
              DISPLAY "PALC-PAL-FACL        :" PALC-PAL-FACL
              DISPLAY "PALC-PAL-WHSE        :" PALC-PAL-WHSE
              DISPLAY "PALC-OWNER           :" PALC-OWNER
              DISPLAY "PALC-PAL-PID         :" PALC-PAL-PID-PICS9
              DISPLAY "PALC-PALLET-SEQ      :" PALC-PALLET-SEQ-PICS9
              DISPLAY "PALC-PAL-TYPE        :" PALC-PAL-TYPE
              DISPLAY "PALC-PAL-INV-TYPE    :" PALC-PAL-INV-TYPE
              DISPLAY "PALC-PAL-INV-STAT    :" PALC-PAL-INV-STAT
              DISPLAY "PALC-PAL-CUR-SLOT    :" PALC-PAL-CUR-SLOT
              DISPLAY "PALC-PAL-CUST        :" PALC-PAL-CUST
              DISPLAY "PALC-PAL-SHIP-DATE   :" PALC-PAL-SHIP-DATE
              DISPLAY "PALC-PAL-ROUTE       :" PALC-PAL-ROUTE
              DISPLAY "PALC-PAL-STOP        :" PALC-PAL-STOP
              DISPLAY "PALC-PAL-LOAD-PT     :" PALC-PAL-LOAD-PT
              DISPLAY "PALC-SECTION         :" PALC-SECTION
              DISPLAY "PALC-PAL-VEND-PID    :" PALC-PAL-VEND-PID
              DISPLAY "PALC-BEING-RECEIVED  :" PALC-BEING-RECEIVED
              DISPLAY "PALC-PALLET-POSITION :" PALC-PALLET-POSITION
              DISPLAY "PALC-OPTION          :" PALC-OPTION
              DISPLAY "PALC-UPDATE-SERIAL   :" PALC-UPDATE-SERIAL-PICS9
              DISPLAY " "
           END-IF.
                                              
           IF PALC-OPTION               = N"A"
              MOVE ZEROES              TO PALC-PALLET-SEQ-PICS9
           END-IF.
           
      * If palc-option = N"A" the pallet will be created
      * If palc-option = N"D" the pallet will be removed.
        
           IF PALC-OPTION               NOT = N"A" AND "D"
              GO TO 0000-EXIT
           END-IF.
            
      * Check if the pallet exits.    
           PERFORM 2110-SELECT-PAL.
           
           IF ERR-RTN-POISON            = N"Y"
              GOTO 0000-EXIT
           END-IF.    
                  
           EVALUATE PALC-OPTION
           WHEN N"A"
              PERFORM 2130-CHECK-PAL
              IF err-rtn-poison NOT     = N"Y"
      * If the values are ok for adding              
                PERFORM 2140-PROCESS-PAL
              END-IF
           WHEN N"D"
      * If the pallet exists and palc-option = N"D" then delete the pallet.                       
              PERFORM 2160-DELETE-PAL
           END-EVALUATE.
      
      * If an error occured, then move the sqlcode and state to the return 
      * linkage.
                            
           IF SQLCODE                   = 0 OR +100
              CONTINUE
           ELSE   
              MOVE SQLCODE             TO ERR-SQLCODE
              MOVE SQLSTATE            TO ERR-SQLSTATE
              MOVE SQLERRM             TO ERR-RTN-DATA
           END-IF.
           
           PERFORM 9999-PALC-CANCEL-PROGRAM.
      
       0000-EXIT.
           EXIT.
           
      * ---------------------------------------------------------------
       2110-SELECT-PAL SECTION.
       2110-SELECT-PAL-START.
      
           MOVE PALC-PAL-TYPE          TO CONTAINER-TYPE.
           MOVE PALC-PAL-INV-TYPE      TO INVENTORY-TYPE.
           MOVE PALC-PAL-INV-STAT      TO INVENTORY-STAT.
      * 
           IF  PALC-OPTION              = N"A" 
           AND PALC-PAL-PID-PICS9             NOT = 0
      *    There can be more than 1 Pallet Id, not unique anymore.
      *        PERFORM 2111-SELECT-PALLET-ID
              CONTINUE 
           ELSE
              PERFORM 2112-SELECT-PALLET-SEQUENCE
           END-IF.
      
       2110-SELECT-PAL-EXIT.
            EXIT.   
      
      * ---------------------------------------------------------------
      * 2111-SELECT-PALLET-ID SECTION.
      * 2111-SELECT-PALLET-ID-START.
      *
      *     MOVE PALC-OWNER             TO ITEM-OWNER.
      *     MOVE PALC-PAL-PID           TO PALLET-ID.
      *
      *     EXEC SQL
      *          SELECT  count(*)
      *          INTO   :w9-count
      *          FROM    scwt_pallet
      *          WHERE   cpy_cd          = :PALC-cpy-cd
      *          AND     facl            = :PALC-pal-facl
      *          AND     whse            = :PALC-pal-whse
      *          AND     pallet_id       = :pallet-id
      *          AND     item_owner      = :item-owner
      *     END-EXEC.
      *
      *     EVALUATE w9-count 
      *     WHEN 0
      *           CONTINUE              
      *     WHEN OTHER
      *           MOVE N"PALC0001"       TO ERR-RTN-CODE
      *           IF WF-DEBUG            = N"Y"
      *               PERFORM 5010-DISPLAY-INPUT
      *           END-IF
      *           PERFORM 9999-ERROR-HANDLE              
      *     END-EVALUATE.
      *
       2111-SELECT-PALLET-ID-EXIT.
           EXIT.
       
      * ---------------------------------------------------------------
       2112-SELECT-PALLET-SEQUENCE SECTION.
       2112-SELECT-PALLET-SEQUENCE-START.
      
           MOVE PALC-OWNER             TO ITEM-OWNER.
           MOVE PALC-PALLET-SEQ-PICS9        TO PALLET-SEQ.
       
           EXEC SQL
                SELECT  pallet_id
                       ,item_owner
                       ,container_type
                       ,inventory_stat
                       ,inventory_type
                INTO  
                       :pallet-id
                      ,:item-owner
                      ,:container-type
                      ,:inventory-stat
                      ,:inventory-type
                FROM  
                        scwt_pallet
                WHERE   cpy_cd          = :PALC-cpy-cd
                AND     facl            = :PALC-pal-facl
                AND     whse            = :PALC-pal-whse
                AND     pallet_seq      = :pallet-seq
                AND     item_owner      = :item-owner
           END-EXEC.
           
      
           EVALUATE SQLCODE
           WHEN 0
              CONTINUE
           WHEN +100
              IF PALC-OPTION            = N"D"              
                 MOVE N"PALC0004"       TO ERR-RTN-CODE
                 IF WF-DEBUG            = N"Y"
                     PERFORM 5010-DISPLAY-INPUT
                 END-IF
                 PERFORM 9999-ERROR-HANDLE                
              END-IF
           WHEN OTHER             
              MOVE N"PALC9000"         TO  ERR-RTN-CODE
              IF WF-DEBUG = N"Y"
                  PERFORM 5010-DISPLAY-INPUT
              END-IF
              PERFORM 9999-ERROR-HANDLE           
           END-EVALUATE.
              
       2112-SELECT-PALLET-SEQUENCE-EXIT.
            EXIT.
      
      * ---------------------------------------------------------------      
       2130-CHECK-PAL SECTION.
       2130-CHECK-PAL-START.
           
      * First ensure that inventory type is valid
           IF PALC-PAL-INV-TYPE         = N"P" OR N"R" OR N"T" 
                                              OR N"S" OR N"1"
                                              OR N"Y"
              CONTINUE
           ELSE   
              MOVE N"PALC0002"          TO ERR-RTN-CODE
              PERFORM 9999-ERROR-HANDLE
           END-IF.
           
      * Now ensure that inventory type /inventory status combination is correct     
           EVALUATE PALC-PAL-INV-TYPE   ALSO PALC-PAL-INV-STAT
           WHEN N"P" ALSO N"A"
           WHEN N"R" ALSO N"A" 
           WHEN N"R" ALSO N"K"
           WHEN N"R" ALSO N"H"
           WHEN N"T" ALSO N"O" 
           WHEN N"T" ALSO N"C"
           WHEN N"S" ALSO N"O" 
           WHEN N"S" ALSO N"C"
           WHEN N"1" ALSO N"U"
           WHEN N"Y" ALSO N"A"
               CONTINUE
           WHEN OTHER
               MOVE N"PALC0003"         TO ERR-RTN-CODE
               PERFORM 9999-ERROR-HANDLE           
           END-EVALUATE.
           
       
       2130-CHECK-PAL-EXIT.
            EXIT.       
      
      * ---------------------------------------------------------------      
       2140-PROCESS-PAL SECTION.
       2140-PROCESS-PAL-START.
           
           PERFORM 2141-GET-PALLET-HEIGHT.
      
           MOVE PALC-CPY-CD            TO NXTN-CPY-CD
           MOVE PALC-PAL-FACL          TO NXTN-FACL
           MOVE PALC-PAL-WHSE          TO NXTN-WHSE
           MOVE N"PLS"                  TO NXTN-TYPE
           CALL "SCWNXTN"               USING NXTN-PARMS 
                                              ERR-RETURN-CODE
           ON OVERFLOW
               MOVE  N"PALC0005"    TO ERR-RTN-CODE
               MOVE N"Y"            TO ERR-RTN-POISON
               PERFORM 9999-PALC-CANCEL-PROGRAM
           END-CALL.
           IF ERR-RTN-CODE              NOT = N"0000000000"
              PERFORM 9999-PALC-CANCEL-PROGRAM
           END-IF
           MOVE NXTN-SEQUENCE-9        TO PALC-PALLET-SEQ-PICS9.
           
           IF WF-DEBUG = N"Y"
              DISPLAY "PALC NXTNMB 1 :" NXTN-SEQUENCE-9
              DISPLAY "PALC NXTNMB 2 :" PALC-PALLET-SEQ-PICS9
           END-IF.
              
           MOVE PALC-PAL-PID-PICS9           TO PALLET-ID.
           MOVE PALC-PALLET-SEQ-PICS9        TO PALLET-SEQ.
           MOVE PALC-OWNER             TO ITEM-OWNER.
           MOVE PALC-PAL-TYPE          TO CONTAINER-TYPE.
           MOVE PALC-PAL-INV-STAT      TO INVENTORY-STAT.
           MOVE PALC-PAL-INV-TYPE      TO INVENTORY-TYPE.
           MOVE PALC-PAL-CUR-SLOT      TO CURR-LOCATION.
      *****************     
        INVOKE INT-VAL "getParsedString"
                  USING CURR-LOCATION
                   RETURNING CURR-LOCATION  
        INVOKE INT-VAL "expandString"
                     USING CURR-LOCATION
                   RETURNING CURR-LOCATION
      *****************     
           MOVE PALC-PAL-CUST          TO CUSTOMER-CODE.
           MOVE PALC-PAL-SHIP-DATE     TO SHIP-DATE.
           MOVE PALC-PAL-ROUTE         TO SHIP-ROUTE.
           MOVE PALC-PAL-STOP          TO SHIP-STOP.
           MOVE PALC-CYCLE          TO SHIP-CYCLE.         
           MOVE PALC-PAL-VEND-PID-PICS9      TO VENDOR-PID.           
           MOVE PALC-SECTION           TO WHSE-SECT.
           MOVE PALC-PAL-LOAD-PT       TO LOAD-POINT.
           MOVE PALC-PALLET-POSITION   TO PALLET-POSITION.
           MOVE PALLET-WOOD            TO PALLET-HEIGHT. 
           
           PERFORM 2150-INSERT-PAL.
           
       2140-PROCESS-PAL-EXIT.
           EXIT.
      
      * ---------------------------------------------------------------
       2141-GET-PALLET-HEIGHT SECTION.
       2141-GET-PALLET-HEIGHT-START.
      
           EXEC SQL
                SELECT  pallet_wood
                INTO   :pallet-wood
                FROM    scwt_whse_parms
                WHERE   cpy_cd          = :PALC-cpy-cd
                AND     facl            = :PALC-pal-facl
                AND     whse            = :PALC-pal-whse
           END-EXEC.
           
           IF WF-DEBUG                  = N"Y"
              DISPLAY " SQLCODE        : " SQLCODE
              DISPLAY " PALLET-WOOD    : " PALLET-WOOD
           END-IF.
      
           EVALUATE SQLCODE
           WHEN 0
              CONTINUE
           WHEN 100
              MOVE ZEROES            TO PALLET-WOOD
           WHEN OTHER              
              MOVE N"PALC9001"         TO  ERR-RTN-CODE
              PERFORM 9999-ERROR-HANDLE           
           END-EVALUATE.
              
       2141-GET-PALLET-HEIGHT-EXIT.
            EXIT.
      
      * -------------------------------------------------------------
       2150-INSERT-PAL SECTION.
       2150-INSERT-PAL-START.
      
           IF SHIP-DATE                 = SPACES OR N" " OR " "
                MOVE AA-NULL-DATE      TO SHIP-DATE
           END-IF.
           
           IF WF-DEBUG = N"Y" OR N"T"
              DISPLAY "**********************************"
              DISPLAY "2150-INSERT-PAL"
              DISPLAY "**********************************"
              DISPLAY "PALC-cpy-cd    " PALC-cpy-cd
              DISPLAY "PALC-pal-facl  " PALC-pal-facl
              DISPLAY "PALC-pal-whse  " PALC-pal-whse
              DISPLAY "pallet-id      " pallet-id
              DISPLAY "pallet-seq     " pallet-seq
              DISPLAY "item-owner     " item-owner
              DISPLAY "container-type " container-type
              DISPLAY "inventory-stat " inventory-stat
              DISPLAY "inventory-type " inventory-type
              DISPLAY "curr-location  " curr-location " !!!"
              DISPLAY "** customer-code " customer-code
              DISPLAY "** ship-date   " ship-date
              DISPLAY "** ship-route  " ship-route
              DISPLAY "** ship-stop   " ship-stop
              DISPLAY "vendor-pid     " vendor-pid
              DISPLAY "whse-sect      " whse-sect
              DISPLAY "load-point     " load-point
              DISPLAY "pallet-position " pallet-position
              DISPLAY "palc-being-received " palc-being-received
              DISPLAY "pallet-height  " pallet-height
              DISPLAY "Palc-transaction " Palc-transaction
              DISPLAY "Palc-cycle " Palc-cycle
              DISPLAY "** palc-pal-to-trader-type " 
                                        palc-pal-to-trader-type
              DISPLAY "**********************************"
           END-IF.           
           
           
           EXEC SQL
               INSERT INTO scwt_pallet
                      (  cpy_cd
                        ,facl
                        ,whse
                        ,pallet_id
                        ,pallet_seq
                        ,item_owner
                        ,container_type
                        ,inventory_stat
                        ,inventory_type
                        ,curr_location
                        ,customer_code
                        ,ship_date
                        ,ship_route
                        ,ship_stop
                        ,vendor_pid
                        ,whse_sect
                        ,load_point
                        ,pallet_position
                        ,being_received
                        ,pallet_height 
                        ,maint_date
                        ,maint_time
                        ,maint_tran
                        ,update_serial
                        ,maint_program
                        ,to_trader_type
                        ,ship_cycle
                        ,maint_user
                        )
            VALUES 
                     (  :PALC-cpy-cd
                       ,:PALC-pal-facl
                       ,:PALC-pal-whse
                       ,:pallet-id
                       ,:pallet-seq
                       ,:item-owner
                       ,:container-type
                       ,:inventory-stat
                       ,:inventory-type
                       ,:curr-location
                       ,:customer-code
                       ,:ship-date
                       ,:ship-route
                       ,:ship-stop
                       ,:vendor-pid
                       ,:whse-sect
                       ,:load-point
                       ,:pallet-position
                       ,:palc-being-received
                       ,:pallet-height
                       ,current date
                       ,current time
                       ,:palc-transaction
                       ,1
                       ,'SCWPALC'
                       ,:palc-pal-to-trader-type
                       ,:ship-cycle
                       ,:node-user)
           END-EXEC.
      *
           MOVE PALLET-SEQ             TO PALC-PALLET-SEQ-PICS9.
           MOVE 1                      TO PALC-UPDATE-SERIAL-PICS9.
      * 
           IF WF-DEBUG                  = N"T" OR N"Y"
              PERFORM 5000-DISPLAY-REC
           END-IF.
      
           IF SQLCODE NOT = 0  
              EVALUATE SQLSTATE 
                WHEN 23505
                  MOVE N"PALC0006"         TO  ERR-RTN-CODE
                  PERFORM 9999-ERROR-HANDLE                
                WHEN 23503
                  MOVE N"PALC0007"         TO  ERR-RTN-CODE
                  PERFORM 9999-ERROR-HANDLE                
                WHEN OTHER 
                  MOVE N"PALC9002"         TO  ERR-RTN-CODE
                  PERFORM 9999-ERROR-HANDLE
              END-EVALUATE
           END-IF. 
      
       2150-INSERT-PAL-EXIT.
            EXIT.
      
      * ---------------------------------------------------------------
       2160-DELETE-PAL SECTION.
       2160-DELETE-PAL-START.
           
           EXEC SQL
                DELETE
                FROM  scwt_pallet
                WHERE cpy_cd            = :PALC-cpy-cd
                AND   facl              = :PALC-pal-facl
                AND   whse              = :PALC-pal-whse
      *         AND   pallet_seq        = :PALC-PALLET-SEQ-PICS9 
                AND   pallet_id         = :PALC-PAL-PID-PICS9 
                AND   item_owner        = :item-owner
           END-EXEC.
      
           IF SQLCODE = 0 OR +100
              CONTINUE
              display "RICHA: DELETE QUERY OF PALC:CONTINUE"
           ELSE
             display "RICHA: DELETE QUERY OF PALC:ELSE"
              PERFORM CB-DATABASE-LOCK-CHECK              
              MOVE  N"PALC9003"          TO  ERR-RTN-CODE
              PERFORM 9999-ERROR-HANDLE
            END-IF.
      
       2160-DELETE-PAL-EXIT.
           EXIT.
      
      * ---------------------------------------------------------------     
       5000-DISPLAY-REC SECTION.
       5000-DISPLAY-INFO.      
       
           DISPLAY " SQLCODE        : " SQLCODE.
           DISPLAY " SQLSTATE       : " SQLSTATE.
           DISPLAY " PAL-PID        : " PALLET-ID.
           DISPLAY " PALLET-SEQ     : " PALLET-SEQ.
           DISPLAY " ITEM-OWNER     : " ITEM-OWNER.
           DISPLAY " PAL-TYPE       : " CONTAINER-TYPE.
           DISPLAY " PAL-INV-STAT   : " INVENTORY-STAT.
           DISPLAY " PAL-INV-TYPE   : " INVENTORY-TYPE.
           DISPLAY " PAL-CUR-SLOT   : " CURR-LOCATION.
           DISPLAY " PAL-CUST       : " CUSTOMER-CODE.
           DISPLAY " PAL-SHIP-DATE  : " SHIP-DATE.
           DISPLAY " PAL-ROUTE      : " SHIP-ROUTE.
           DISPLAY " PAL-STOP       : " SHIP-STOP.
           DISPLAY " PAL-VEND-PID   : " VENDOR-PID.
           DISPLAY " ITEM-SECTION   : " WHSE-SECT.
           DISPLAY " LOAD-POINT     : " LOAD-POINT.
           DISPLAY " PAL-POSITION   : " PALLET-POSITION.
           DISPLAY " PALLET-HEIGHT  : " PALLET-HEIGHT.
       
       5000-EXIT.
           EXIT.
           
       5010-DISPLAY-INPUT SECTION.
       5010-START.
       
           DISPLAY " PALC-CPY-CD          :" PALC-CPY-CD.
           DISPLAY " PALC-PAL-FACL        :" PALC-PAL-FACL.
           DISPLAY " PALC-PAL-WHSE        :" PALC-PAL-WHSE.
           DISPLAY " PALC-OWNER           :" PALC-OWNER.
           DISPLAY " PALC-PAL-PID         :" PALC-PAL-PID-PICS9.
           DISPLAY " PALC-PALLET-SEQ      :" PALC-PALLET-SEQ-PICS9
           DISPLAY " PALC-PAL-TYPE        :" PALC-PAL-TYPE.
           DISPLAY " PALC-PAL-INV-TYPE    :" PALC-PAL-INV-TYPE.
           DISPLAY " PALC-PAL-CUR-SLOT    :" PALC-PAL-CUR-SLOT.
           DISPLAY " PALC-PAL-CUST        :" PALC-PAL-CUST.
           DISPLAY " PALC-PAL-SHIP-DATE   :" PALC-PAL-SHIP-DATE.
           DISPLAY " PALC-PAL-ROUTE       :" PALC-PAL-ROUTE.
           DISPLAY " PALC-PAL-STOP        :" PALC-PAL-STOP.
           DISPLAY " PALC-PAL-LOAD-PT     :" PALC-PAL-LOAD-PT.
           DISPLAY " PALC-SECTION         :" PALC-SECTION.
           DISPLAY " PALC-PAL-VEND-PID    :" PALC-PAL-VEND-PID.
           DISPLAY " PALC-PAL-VEND-PID-PICS9:" PALC-PAL-VEND-PID-PICS9.
           DISPLAY " PALC-BEING-RECEIVED  :" PALC-BEING-RECEIVED.
           DISPLAY " PALC-PALLET-POSITION :" PALC-PALLET-POSITION.
           DISPLAY " PALC-OPTION          :" PALC-OPTION.
           DISPLAY " PALC-UPDATE-SERIAL   :" PALC-UPDATE-SERIAL-PICS9.
           
        5019-EXIT.
            EXIT. 
            
      * ---------------------------------------------------------------
       9999-ERROR-HANDLE SECTION.
       9999-DISPLAY-ERROR.
           
           invoke int-val "expandString"
                   using sqlstate 
                   returning wg-sqlstate
           MOVE SQLCODE                TO  ERR-SQLCODE.        
           MOVE SQLSTATE               TO  ERR-SQLSTATE.
           STRING 
                  N"ST:"
                  wg-SQLSTATE
                  N" /"  
                  PALC-cpy-cd,
                  N"/",
                  PALC-pal-facl,
                  N"/",
                  PALC-pal-whse,
                  N"/"
                  PALLET-SEQ
                  N"/",
                  PALLET-ID
                  N"/"
                  ITEM-OWNER
                  N"/"
                                        DELIMITED BY SIZE 
                                        INTO   ERR-RTN-DATA
           END-STRING.
           
           MOVE N"Y"                    TO ERR-RTN-POISON.
                
           IF ERR-RTN-CODE              NOT = N"0000000000"
              MOVE ERR-RTN-CODE        TO NODE-RESULT-FIELD 
              MOVE ERR-RTN-DATA        TO NODE-ERROR-MESSAGE-DATA
              MOVE ERR-RTN-POISON      TO NODE-POISON-MESSAGE-FLAG
           END-IF.   

           PERFORM 9999-PALC-CANCEL-PROGRAM.
                  
       9999-PALC-DISPLAY-EXIT-GOBACK.
           EXIT. 
                                                      
       9999-PALC-CANCEL-PROGRAM SECTION.
       9999-PALC-GOBACK.
      *****************     
        INVOKE INT-VAL "expandString"
                                        USING PALC-PAL-PID-PICS9
                               RETURNING PALC-PAL-PID  
                                       
        INVOKE INT-VAL "expandString"
                                        USING PALC-UPDATE-SERIAL-PICS9
                               RETURNING PALC-UPDATE-SERIAL  
        INVOKE INT-VAL "expandString"
                                        USING PALC-PALLET-SEQ-PICS9
                               RETURNING PALC-PALLET-SEQ  
           IF WF-DEBUG                  = N"Y" OR N"P"
              DISPLAY "******************************************"
              DISPLAY "* SCWPALC  GOBACK                        *"
              DISPLAY "******************************************"
           END-IF.
           GOBACK.
      
      ******************************************************************
       COPY "SCWDBSTP.cpy". 
      ******************************************************************
      ******************************************************************
