       IDENTIFICATION DIVISION.
       PROGRAM-ID. SCWSBFP.
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
      *---------------------- 
       SOURCE-COMPUTER. IBM-PC.
       OBJECT-COMPUTER. IBM-PC.
       SPECIAL-NAMES.   CONSOLE IS SYSLOG.
       INPUT-OUTPUT SECTION.
      *--------------------- 
       FILE-CONTROL.
      *
       DATA DIVISION.
       FILE SECTION.
      *-------------       
      * 
       WORKING-STORAGE SECTION.
      *------------------------
       COPY "SCWAAAW.cpy". 
      * 
       01  WF-DEBUG                     PIC G         VALUE N"Y"
                                        get-property set-property.
       01  INT-VAL    object reference 
          "com.aquitecintl.ewms.common.facades.DateService".
                                       
       01  WX-ERROR-METHOD              PIC G(10)     VALUE " ".        
      *      
      **************************************************************************
      * D a t a b a s e     T a b l e s                                        * 
      **************************************************************************
      * 
      **************************************************************************   
      * SQLCA                                                                  * 
      **************************************************************************
       EXEC SQL INCLUDE SQLCA.cpy END-EXEC.
      * 
      **************************************************************************
      * Sys Parms Table.                                                       * 
      **************************************************************************
       COPY "SCWSYSPD.cpy". 
      *               
      **************************************************************************
      * Report Request Table.                                                  * 
      **************************************************************************
       COPY "SCWRPTRD.cpy". 
      *                    
      **************************************************************************
      * Current date                                                           * 
      **************************************************************************
       COPY "SCWCDATL.cpy". 
      *  
       COPY "SCWPKDPL.cpy". 
      **************************************************************************
      * Node copybook.                                                         * 
      **************************************************************************
       COPY "SCWNODE.cpy".

      *  Linkage to produce the report.
             COPY "SCWRPTCL.cpy".
      ****************************************************************** 
      * Report request control.                                        * 
      ******************************************************************
           COPY  "SCWRPCTL.cpy".

       LINKAGE SECTION. 
      *---------------- 
      *** SBFP passed parameters
      * 01  RELP-RELEASE-NO         LIKE AA-INTEGER
      *                                  get-property set-property.   
       01  RELP-RELEASE-NO PIC G(9)
                              get-property set-property.
       01  RELP-RELEASE-NO-PICS9  LIKE AA-INTEGER . 
                                                    
      *       
       COPY "SCWSQLEL.cpy".
      *
      **************************************************************************
      **************************************************************************
      ***                                                                    ***
      ***               P R O C E D U R E   D I V I S I O N                  ***
      ***                                                                    ***
      **************************************************************************
      **************************************************************************      
      *
       PROCEDURE DIVISION USING ERR-RETURN-CODE.
      *------------------------------------------------------------------------*
       0000-SBFP-MAIN SECTION.
       0000-SBFP-START.
           IF WF-DEBUG                  = N"Y" OR N"P"
              DISPLAY "******************************************"
              DISPLAY "* SCWSBFP  STARTED                       *"
              DISPLAY "******************************************"
           END-IF.
           
           
           INVOKE "com.aquitecintl.ewms.common.facades.DateService"
            "getInstance"               RETURNING INT-VAL
           ON EXCEPTION
               MOVE "INVK0002"         TO ERR-RTN-CODE           
               MOVE "getInstance"      TO WX-ERROR-METHOD
               PERFORM 9999-SBFP-ENDIT
           END-INVOKE.   
           INVOKE INT-VAL "getParsedString"
                                        USING RELP-RELEASE-NO
                               RETURNING RELP-RELEASE-NO-PICS9  
           
           IF  WF-DEBUG = N"Y"
               DISPLAY "RELP-RELEASE-NO-PICS9:" RELP-RELEASE-NO-PICS9 
           END-IF. 
                               
           INVOKE INT-VAL "expandString"
                                        USING RELP-RELEASE-NO-PICS9
                               RETURNING RELP-RELEASE-NO 
                               

           IF  WF-DEBUG = N"Y"
               DISPLAY "RELP-RELEASE-NO:" RELP-RELEASE-NO 
           END-IF.      
                          
           MOVE N"0000000000"  TO ERR-RTN-CODE.     

           COPY "SCWCNCTP.cpy".
           COPY "SCWCNTDB.cpy".
      *
           Move N"MRPIFPP"          to rpct-rpt-pgm
           MOVE  NODE-COMPANY-CODE       TO   RPCT-CPY-CD.
           MOVE  NODE-FACILITY           TO   RPCT-FACL.
           MOVE  NODE-WAREHOUSE          TO   RPCT-WHSE.
           Move node-user           to rpct-user
           Move node-transaction-id to rpct-trans-id
           Initialize  rpct-parm
           String RELP-RELEASE-NO DELIMITED BY N" "
                  N"|" delimited by size
             into rpct-parm
           End-string
           
           Call "SCWRPCT" using RPCT-PARMS, ERR-RETURN-CODE
                 On overflow
           Move "SBFPnnnn"          to err-rtn-code
           Perform 9999-SBFP-ENDIT
           End-call
           Cancel "SCWRPCT".  
      *    
       0000-SBFP-EXIT.
           PERFORM 9999-SBFP-ENDIT.
      *------------------------------------------------------------------------*
       9999-SBFP-ENDIT SECTION.
       9999-SBFP-ENDIT-START.
           IF WF-DEBUG                  = N"Y" OR N"P"
              DISPLAY "******************************************"
              DISPLAY "* SCWSBFP  GOBACK                        *"
              DISPLAY "******************************************"
           END-IF.

           GOBACK.
         