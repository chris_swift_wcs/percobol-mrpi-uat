       IDENTIFICATION DIVISION.
       PROGRAM-ID. xoldSCWLS10.
       DATA DIVISION. 
       WORKING-STORAGE SECTION.
       01  WF-DEBUG                     PIC G VALUE N"Y"
           GET-PROPERTY SET-PROPERTY.
       01  INT-VAL    object reference 
          "com.aquitecintl.ewms.common.facades.DateService".   
      ** COMMON COPYBOOOK
       COPY "SCWAAAW.cpy".
       01  WX-ERROR-METHOD              LIKE AA-INVOKE-ERROR .                   

       01  WX-PARA                      PIC X(50).
                 
      ****************************************************************************
      * SQL Declare Statements                                                   *
      ****************************************************************************
            EXEC SQL        INCLUDE SQLCA.cpy    END-EXEC.
            
      ****************************************************************************
      * TABLE DEFINITIONS                                                        *
      ****************************************************************************
      ** MESSAGE TEXT
       COPY "SCWMESSD.cpy".
      
      ** SYSTEM PARAMETERS
       COPY "SCWSYSPD.cpy".
      
      ** EQUIPMENT CODE 
       COPY "SCWEQPCD.cpy". 
       
      ** TRAVEL PATHS 
       COPY "SCWTRPAD.cpy".
       
      ** ACCESS POINTS 
       COPY "SCWACCPD.cpy".
      
      ** ACCESS AREAS 
       COPY "SCWACCAD.cpy".
      
      ** FORKLIFT BAY TRAVEL  
       COPY "SCWFKBTD.cpy". 
      
      ** AISLE SUBSTITUTIONS 
       COPY "SCWALSBD.cpy".
        
      
       
      * PROGRAM FLAGS       
       01 WS-SQL-ERROR   PIC G.  
       01 WS-END-QUEST   PIC G.
       01 WS-ROUTE-VALID PIC G. 
       01 WS-SUB-FOUND   PIC G.
      
      *** PROGRAM SUBSCRIPTS 
       01 WS-SUB         PIC S99.
       01 WS-SUB2        PIC S99.
       01 WS-FROM-SUB    PIC S99. 
       01 WS-TO-SUB      PIC S99.
       01 WS-AP-COUNT    PIC S99.
       01 WS-STR-SUB     PIC S99.
       01 WS-IMD-SUB     PIC S99.
       01 WS-END-SUB     PIC S99.
       01 WS-IMD-COUNT   PIC S99.
       01 WS-END-COUNT   PIC S99.
       01 WS-STR-COUNT   PIC S99.
       
       
       01 WS-COMMON. 
          05 WS-COM-FROM-AREA   PIC G(15). 
          05 WS-COM-TO-AREA     PIC G(15).
      

       01 WG-WORK-FIELDS.
          05 WS-aaabbb.
             05  WX-AA       PIC X(3)             VALUE " ".
             05  W9-BB       like aa-bay          VALUE " ".
             05  WX-BB  redefines w9-BB           PIC X(03).
      
       01 WS-PTP. 
      *    05 WS-PTP-DISTANCE    PIC S9(05)V9. 
          05 WS-PTP-DISTANCE PIC S9(06)V9(03) COMP-5.
      *    05 WS-PTP-TIME        PIC S9(05)V99.
          05 WS-PTP-TIME PIC S9(6)V9(05) COMP-5.
          05 WS-PTP-FROM-X      PIC S9(05)V9.
          05 WS-PTP-FROM-Y      PIC S9(05)V9.
          05 WS-PTP-TO-X        PIC S9(05)V9. 
          05 WS-PTP-TO-Y        PIC S9(05)V9. 
          05 WS-X-DIFFERENCE    PIC S9(05)V9.
          05 WS-Y-DIFFERENCE    PIC S9(05)V9.
      
       01 WS-BAY. 
          05 WS-BAY-FROM-X      PIC S9(05)V9.
          05 WS-BAY-FROM-Y      PIC S9(05)V9.
          05 WS-BAY-TO-X        PIC S9(05)V9. 
          05 WS-BAY-TO-Y        PIC S9(05)V9. 
          05 WS-BAY-AISLE       PIC G(03). 
          05 WS-BAY-DISTANCE    PIC S9(05)V9.
          05 WS-BAY-TIME        PIC S99999V99 COMP-3.
          
          
          
       01 WS-PATHS. 
          05 WS-PATH-FROM-AREA  PIC G(15). 
          05 WS-PATH-TO-AREA    PIC G(15). 
          05 WS-PATH-COUNT      PIC S99. 
          05 WS-PATH-TABLE  OCCURS 10 TIMES. 
             10 WS-PATH-START-X   PIC S9(05)V9 COMP-3.
             10 WS-PATH-START-Y   PIC S9(05)V9 COMP-3.
             10 WS-PATH-END-X     PIC S9(05)V9 COMP-3.
             10 WS-PATH-END-Y     PIC S9(05)V9 COMP-3.
             10 WS-PATH-VIA-AREA1 PIC G(15). 
             10 WS-PATH-VIA-AREA2 PIC G(15). 
            10 WS-PATH-DISTANCE  PIC S9(05)V9 COMP-3.
      
      
      *** ACCESS POINTS THAT OVERLAP TWO AREAS  
       01 WS-AP.
          05 WS-AP-TABLE OCCURS 9 TIMES.
             10 WS-AP-NAME      PIC G(15). 
             10 WS-AP-XCORD     PIC S9(05)V9.
             10 WS-AP-YCORD     PIC S9(05)V9.
             10 WS-AP-MINUTES   PIC S9(02)V99.
             
      *** ROUTE INFORMATION FOR A SPECIFIC PATH 
       01 WS-ROUTE. 
          05 WS-RTE-START OCCURS 9 TIMES. 
             10 WS-RTE-IMD OCCURS 9 TIMES.
                15 WS-RTE-END OCCURS 9 TIMES.  
                   20 WS-RTE-DIST  PIC S9(06)V9 COMP-3.
                   20 WS-RTE-TIME  PIC S99999V999 COMP-3.
                   
          05 WS-RTE-STR-COUNT PIC S9.
          05 WS-RTE-IMD-COUNT PIC S9. 
          05 WS-RTE-END-COUNT PIC S9. 
       
       01 WS-BST-ROUTE. 
          05 WS-BEST-RTE-STR  PIC S9.
          05 WS-BEST-RTE-IMD  PIC S9. 
          05 WS-BEST-RTE-END  PIC S9. 
          05 WS-BEST-RTE-DIST PIC S9(06)V9 COMP-3.
      
                   
       01 WS-RTE-POINTS. 
          05 WS-RTE OCCURS 9 TIMES. 
             10 WS-RTE-STR-X PIC S9(05)V9 COMP-3. 
             10 WS-RTE-STR-Y PIC S9(05)V9 COMP-3.
             10 WS-RTE-IMD-X PIC S9(05)V9 COMP-3. 
             10 WS-RTE-IMD-Y PIC S9(05)V9 COMP-3.
             10 WS-RTE-END-X PIC S9(05)V9 COMP-3. 
             10 WS-RTE-END-Y PIC S9(05)V9 COMP-3. 
             10 WS-IMD-TIME  PIC S99V99.
             10 WS-END-TIME  PIC S99V99.            
                   
      
       01 WS-BEST. 
          05 WS-BEST-DISTANCE PIC S9(05)V9 COMP-3.
          05 WS-BEST-TIME     PIC S9(05)V99 COMP-3.
          05 WS-BEST-FROM-AAP PIC S9.
          05 WS-BEST-TO-AAP   PIC S9.
          05 WS-BEST-TYPE     PIC G. 
             88 BEST-IS-COMMON-DOOR VALUE N"D".
             88 BEST-IS-PATH      VALUE N"P". 
          05 WS-BEST-PATH     PIC G(06). 
          05 WS-BEST-AP       PIC S99.
       01 WS-TOTAL-DISTANCE   PIC S9(05)V9 COMP-3. 
       01 WS-TOTAL-TIME       PIC S9999V999 COMP-3.
       
             
       01 FROM-AREA.
          05 WS-FROM-AREA PIC G(15).
          05 FROM-AREA-DATA.
             10  FROM-ACCESS1-X-CORD  LIKE AA-XYZ-CORDS  VALUE 0.
             10  FROM-ACCESS1-Y-CORD  LIKE AA-XYZ-CORDS  VALUE 0.
             10  FROM-ENTRY-OR-EXIT1  PIC G.
             10  FROM-ACCESS-NAME1    PIC G(15).
             10  FROM-ACCESS-MINUTES1 LIKE AA-ACCESS-MINUTES VALUE 0.
             10  FROM-ACCESS2-X-CORD  LIKE AA-XYZ-CORDS  VALUE 0.
             10  FROM-ACCESS2-Y-CORD  LIKE AA-XYZ-CORDS  VALUE 0.
             10  FROM-ENTRY-OR-EXIT2  PIC G.   
             10  FROM-ACCESS-NAME2    PIC G(15).
             10  FROM-ACCESS-MINUTES2 LIKE AA-ACCESS-MINUTES VALUE 0.
             10  FROM-ACCESS3-X-CORD  LIKE AA-XYZ-CORDS  VALUE 0.
             10  FROM-ACCESS3-Y-CORD  LIKE AA-XYZ-CORDS  VALUE 0.
             10  FROM-ENTRY-OR-EXIT3  PIC G.
             10  FROM-ACCESS-NAME3    PIC G(15).
             10  FROM-ACCESS-MINUTES3 LIKE AA-ACCESS-MINUTES VALUE 0.
             10  FROM-ACCESS4-X-CORD  LIKE AA-XYZ-CORDS  VALUE 0.
             10  FROM-ACCESS4-Y-CORD  LIKE AA-XYZ-CORDS  VALUE 0.
             10  FROM-ENTRY-OR-EXIT4  PIC G.
             10  FROM-ACCESS-NAME4    PIC G(15).
             10  FROM-ACCESS-MINUTES4 LIKE AA-ACCESS-MINUTES VALUE 0.
             10  FROM-ACCESS5-X-CORD  LIKE AA-XYZ-CORDS  VALUE 0.
             10  FROM-ACCESS5-Y-CORD  LIKE AA-XYZ-CORDS  VALUE 0.
             10  FROM-ENTRY-OR-EXIT5  PIC G.
             10  FROM-ACCESS-NAME5    PIC G(15).
             10  FROM-ACCESS-MINUTES5 LIKE AA-ACCESS-MINUTES VALUE 0.
             10  FROM-ACCESS6-X-CORD  LIKE AA-XYZ-CORDS  VALUE 0.
             10  FROM-ACCESS6-Y-CORD  LIKE AA-XYZ-CORDS  VALUE 0.
             10  FROM-ENTRY-OR-EXIT6  PIC G.
             10  FROM-ACCESS-NAME6    PIC G(15).
             10  FROM-ACCESS-MINUTES6 LIKE AA-ACCESS-MINUTES VALUE 0.
             10  FROM-ACCESS7-X-CORD  LIKE AA-XYZ-CORDS  VALUE 0.
             10  FROM-ACCESS7-Y-CORD  LIKE AA-XYZ-CORDS  VALUE 0.
             10  FROM-ENTRY-OR-EXIT7  PIC G.
             10  FROM-ACCESS-NAME7    PIC G(15).
             10  FROM-ACCESS-MINUTES7 LIKE AA-ACCESS-MINUTES VALUE 0.
             10  FROM-ACCESS8-X-CORD  LIKE AA-XYZ-CORDS  VALUE 0.
             10  FROM-ACCESS8-Y-CORD  LIKE AA-XYZ-CORDS  VALUE 0.
             10  FROM-ENTRY-OR-EXIT8  PIC G.
             10  FROM-ACCESS-NAME8    PIC G(15).
             10  FROM-ACCESS-MINUTES8 LIKE AA-ACCESS-MINUTES VALUE 0.
             10  FROM-ACCESS9-X-CORD  LIKE AA-XYZ-CORDS  VALUE 0.
             10  FROM-ACCESS9-Y-CORD  LIKE AA-XYZ-CORDS  VALUE 0.
             10  FROM-ENTRY-OR-EXIT9  PIC G.
             10  FROM-ACCESS-NAME9    PIC G(15).
             10  FROM-ACCESS-MINUTES9 LIKE AA-ACCESS-MINUTES VALUE 0.
             
          05 FROM-AREA-TABLE REDEFINES FROM-AREA-DATA. 
             10 FROM-TABLE OCCURS 9 TIMES. 
                15  FROM-ACCESS-X-CORD  LIKE AA-XYZ-CORDS  VALUE 0.
                15  FROM-ACCESS-Y-CORD  LIKE AA-XYZ-CORDS  VALUE 0.
                15  FROM-ENTRY-OR-EXIT  PIC G.
                15  FROM-ACCESS-NAME    PIC G(15).
                15  FROM-ACCESS-MINUTES LIKE AA-ACCESS-MINUTES VALUE 0.
                
                
           01 TO-AREA.
             05 WS-TO-AREA          PIC G(15). 
             05 TO-AREA-DATA.
             10 TO-ACCESS1-X-CORD  LIKE AA-XYZ-CORDS  VALUE 0.
             10 TO-ACCESS1-Y-CORD  LIKE AA-XYZ-CORDS  VALUE 0.
             10 TO-ENTRY-OR-EXIT1  PIC G.
             10 TO-ACCESS-NAME1    PIC G(15).
             10 TO-ACCESS-MINUTES1 LIKE AA-ACCESS-MINUTES VALUE 0.
             10 TO-ACCESS2-X-CORD  LIKE AA-XYZ-CORDS  VALUE 0.
             10 TO-ACCESS2-Y-CORD  LIKE AA-XYZ-CORDS  VALUE 0.
             10 TO-ENTRY-OR-EXIT2  PIC G.   
             10 TO-ACCESS-NAME2    PIC G(15).
             10 TO-ACCESS-MINUTES2 LIKE AA-ACCESS-MINUTES VALUE 0.
             10 TO-ACCESS3-X-CORD  LIKE AA-XYZ-CORDS  VALUE 0.
             10 TO-ACCESS3-Y-CORD  LIKE AA-XYZ-CORDS  VALUE 0.
             10 TO-ENTRY-OR-EXIT3  PIC G.
             10 TO-ACCESS-NAME3    PIC G(15).
             10 TO-ACCESS-MINUTES3 LIKE AA-ACCESS-MINUTES VALUE 0.
             10 TO-ACCESS4-X-CORD  LIKE AA-XYZ-CORDS  VALUE 0.
             10 TO-ACCESS4-Y-CORD  LIKE AA-XYZ-CORDS  VALUE 0.
             10 TO-ENTRY-OR-EXIT4  PIC G.
             10 TO-ACCESS-NAME4    PIC G(15).
             10 TO-ACCESS-MINUTES4 LIKE AA-ACCESS-MINUTES VALUE 0.
             10 TO-ACCESS5-X-CORD  LIKE AA-XYZ-CORDS  VALUE 0.
             10 TO-ACCESS5-Y-CORD  LIKE AA-XYZ-CORDS  VALUE 0.
             10 TO-ENTRY-OR-EXIT5  PIC G.
             10 TO-ACCESS-NAME5    PIC G(15).
             10 TO-ACCESS-MINUTES5 LIKE AA-ACCESS-MINUTES VALUE 0.
             10 TO-ACCESS6-X-CORD  LIKE AA-XYZ-CORDS  VALUE 0.
             10 TO-ACCESS6-Y-CORD  LIKE AA-XYZ-CORDS  VALUE 0.
             10 TO-ENTRY-OR-EXIT6  PIC G.
             10 TO-ACCESS-NAME6    PIC G(15).
             10 TO-ACCESS-MINUTES6 LIKE AA-ACCESS-MINUTES VALUE 0.
             10 TO-ACCESS7-X-CORD  LIKE AA-XYZ-CORDS  VALUE 0.
             10 TO-ACCESS7-Y-CORD  LIKE AA-XYZ-CORDS  VALUE 0.
             10 TO-ENTRY-OR-EXIT7  PIC G.
             10 TO-ACCESS-NAME7    PIC G(15).
             10 TO-ACCESS-MINUTES7 LIKE AA-ACCESS-MINUTES VALUE 0.
             10 TO-ACCESS8-X-CORD  LIKE AA-XYZ-CORDS  VALUE 0.
             10 TO-ACCESS8-Y-CORD  LIKE AA-XYZ-CORDS  VALUE 0.
             10 TO-ENTRY-OR-EXIT8  PIC G.
             10 TO-ACCESS-NAME8    PIC G(15).
             10 TO-ACCESS-MINUTES8 LIKE AA-ACCESS-MINUTES VALUE 0.
             10 TO-ACCESS9-X-CORD  LIKE AA-XYZ-CORDS  VALUE 0.
             10 TO-ACCESS9-Y-CORD  LIKE AA-XYZ-CORDS  VALUE 0.
             10 TO-ENTRY-OR-EXIT9  PIC G.
             10 TO-ACCESS-NAME9    PIC G(15).
             10 TO-ACCESS-MINUTES9 LIKE AA-ACCESS-MINUTES VALUE 0.
             
          05 TO-AREA-TABLE REDEFINES TO-AREA-DATA. 
             10 TO-TABLE OCCURS 9 TIMES. 
                15 TO-ACCESS-X-CORD  LIKE AA-XYZ-CORDS  VALUE 0.
                15 TO-ACCESS-Y-CORD  LIKE AA-XYZ-CORDS  VALUE 0.
                15 TO-ENTRY-OR-EXIT  PIC G.
                15 TO-ACCESS-NAME    PIC G(15).
                15 TO-ACCESS-MINUTES LIKE AA-ACCESS-MINUTES VALUE 0.
      
      **** ACCESS POINT INFORMATION FOR THE FROM LOCATION 
       01 ARFS.
           15 ARFS-HIGH-LOCATION         PIC G(15).               
           15 ARFS-LOW-LOCATION          PIC G(15).               
           15 ARFS-END-BEGIN-DIRECTION.                        
              20 ARFS-END-DIRECTION     PIC G(1).               
              20 ARFS-BEGIN-DIRECTION   PIC G(1).                                                                
           15 FILLER REDEFINES     ARFS-END-BEGIN-DIRECTION.    
              20 ARFS-EB-DIRECTION      PIC G(1) OCCURS 2      
                            INDEXED BY ARFS-EBD-INDEX.             
           15 ARFS-END-X                 PIC 9(4)V9 COMP-3.     
           15 ARFS-BEGIN-X               PIC 9(4)V9 COMP-3.     
           15 ARFS-END-Y                 PIC 9(4)V9 COMP-3.     
           15 ARFS-BEGIN-Y               PIC 9(4)V9 COMP-3.     
           15 ARFS-END-Z                 PIC 9(4)V9 COMP-3.     
           15 ARFS-BEGIN-Z               PIC 9(4)V9 COMP-3.     
           15 ARFS-ACCESS-AREA           PIC G(15).                                                   
           15 ARFS-BAY-LENGTH            PIC 9(2)V9(1).                         
           15 ARFS-UPDATE-CODE           PIC G(1).  
           15 ARFS-BEGIN-DISTANCE        PIC S9(05)V9. 
           15 ARFS-END-DISTANCE          PIC S9(05)V9.
           15 ARFS-BEGIN-TIME            PIC S99V99. 
           15 ARFS-END-TIME              PIC S99V99. 
      
      
      **** ACCESS POINT INFORMATION FOR THE TO LOCATION
       01 ARTS.
           15 ARTS-HIGH-LOCATION         PIC G(15).               
           15 ARTS-LOW-LOCATION          PIC G(15).               
           15 ARTS-END-BEGIN-DIRECTION.                        
              20 ARTS-END-DIRECTION     PIC G(1).               
              20 ARTS-BEGIN-DIRECTION   PIC G(1).                                                                
           15 FILLER REDEFINES     ARTS-END-BEGIN-DIRECTION.    
              20 ARTS-EB-DIRECTION      PIC G(1) OCCURS 2      
                            INDEXED BY ARTS-EBD-INDEX.             
           15 ARTS-END-X                 PIC 9(4)V9 COMP-3.     
           15 ARTS-BEGIN-X               PIC 9(4)V9 COMP-3.     
           15 ARTS-END-Y                 PIC 9(4)V9 COMP-3.     
           15 ARTS-BEGIN-Y               PIC 9(4)V9 COMP-3.     
           15 ARTS-END-Z                 PIC 9(4)V9 COMP-3.     
           15 ARTS-BEGIN-Z               PIC 9(4)V9 COMP-3.     
           15 ARTS-ACCESS-AREA           PIC G(15).                                                   
           15 ARTS-BAY-LENGTH            PIC 9(2)V9(1).                         
           15 ARTS-UPDATE-CODE           PIC G(1).       
           15 ARTS-BEGIN-DISTANCE        PIC S9(05)V9. 
           15 ARTS-END-DISTANCE          PIC S9(05)V9. 
           15 ARTS-BEGIN-TIME            PIC S99V99. 
           15 ARTS-END-TIME              PIC S99V99.
      
       01 TRAVEL-ROUTES-TABLE. 
          05 TVL-FROM OCCURS 2 TIMES.
             10 TVL-TO   OCCURS 2 TIMES.
                15 TVL-FROM-ACCESS-DIST      PIC S9(05)V9. 
                15 TVL-TO-ACCESS-DIST        PIC S9(05)V9.
                15 TVL-PATH-DIST             PIC S9(05)V9. 
                15 TVL-FROM-ACCESS-TIME      PIC S9(05)V99. 
                15 TVL-TO-ACCESS-TIME        PIC S9(05)V99.
                15 TVL-PATH-TIME             PIC S9(05)V99. 
                
       01 TVL-TO   PIC S9.
       01 TVL-FROM PIC S9.
       01 WS-TO-AA-AREA PIC G(15). 
       01 WS-FROM-AA-AREA PIC G(15).
       
       01 WS-TRAVEL-RATE PIC S9(05)V9(03) COMP-3.
       01 EOA PIC G.
       01 WS-REM  PIC S9999.
       01 WS-JUNK PIC S9999.
       01 WS-SLOT-TYPE PIC G. 
          88 HAND-STACK   VALUE N"0".
          88 FLOOR-SLOT   VALUE N"1".
          88 PICK-RACK    VALUE N"2".
          88 RESERVE-SLOT VALUE N"3".
          88 STORAGE-SLOT VALUE N"4".
          88 PALLET-FLOW  VALUE N"5".
          88 CASE-PICK    VALUE N"6". 
          88 PICK-TYPE    VALUE N"0" N"1" N"2" N"5" N"6".
          88 RESERVE-TYPE VALUE N"3" "4".
              
      
      
       01  PROGRAM-LITERALS.
           05  WS-BAY-MOVE-LITERAL     PIC  G(30).
           05  WS-AISLE-MOVE-LITERAL   PIC  G(30).
           05  WS-ACCESS-MOVE-LITERAL  PIC  G(30).
           05  WS-ACCESS-TIME-LITERAL  PIC  G(30).
      
          
          
       COPY "SCWNODE.cpy".
      
       LINKAGE SECTION.
       COPY "SCWLS10L.cpy".
      
       COPY "SCWSQLEL.cpy".
      
       PROCEDURE DIVISION USING LS10-PARMS, ERR-RETURN-CODE.
      
       0000-LS10-MAIN SECTION. 
       0000-LS10-BEGIN.
           IF WF-DEBUG                  = N"Y" OR N"P"
              DISPLAY "******************************************"
              DISPLAY "* SCWLS10  STARTED                       *"
              DISPLAY "******************************************"
           END-IF.   

      * Create a connetion to DB2          
           Copy "SCWCNCTP.cpy".
           Copy "SCWCNTDB.cpy".

         INVOKE "com.aquitecintl.ewms.common.facades.DateService"
            "getInstance"               RETURNING INT-VAL
           ON EXCEPTION
               MOVE "INVK0002"         TO ERR-RTN-CODE           
               MOVE "getInstance"      TO WX-ERROR-METHOD   
           END-INVOKE.

      ******************
      
      *     INITIALIZE ERR-RETURN-CODE REPLACING NUMERIC DATA BY 0.
      *     MOVE ZEROES                 TO ERR-RTN-CODE.
	    MOVE N"0000000000"		TO ERR-RTN-CODE.
      
      ***
      *** TEST CODE THAT SIMPLY RETURNS PTP DISTANCE BETWEEN TWO POINTS 
      *** THIS CODE IS ONLY HERE TO ENABLE DEVELOPMENT OF OTHER PGMS 
      *** TO CONTINUE -- ** IT MUST BE REMOVED WHEN COMPLETED ***
      *** 
      *      if wf-debug = N"Y"
      *          display "we are using standard PTP distance between"
      *          display "two points.  this needs to be removed when"
      *          display "ready to test LS10. rates = .5 - N=direction"
      *          display "ls10-from-location=" ls10-from-location
      *                  " ls10-from-x-cord= " ls10-from-x-cord
      *                  " ls10-from-y-cord= " ls10-from-y-cord
      *          display "ls10-to-location" ls10-to-location
      *                  " ls10-to-x-cord= " ls10-from-x-cord
      *                  " ls10-to-y-cord= " ls10-from-y-cord
      *       end-if
      *       MOVE LS10-FROM-X-CORD    TO WS-PTP-FROM-X 
      *       MOVE LS10-FROM-Y-CORD    TO WS-PTP-FROM-Y 
      *       MOVE LS10-TO-X-CORD      TO WS-PTP-TO-X 
      *       MOVE LS10-TO-Y-CORD      TO WS-PTP-TO-Y 
      *       MOVE 0.008    TO  TRAVEL-RATE-EMPTY
      *       MOVE 0.0077   TO  TRAVEL-RATE-SINGLE
      *       MOVE 0.0077   TO  TRVL-RATE-MULTIPLE 
      *       PERFORM 4200-PTP-DISTANCE 
      *       MOVE WS-PTP-DISTANCE     TO LS10-TRAVEL-DISTANCE
      *       MOVE WS-PTP-TIME         TO LS10-TRAVEL-TIME
      *       MOVE LS10-FROM-LOCATION TO LS10-AT-TO-SLOT  (1)  
      *       MOVE LS10-TO-LOCATION TO LS10-AT-FROM-SLOT  (1)    
      *       MOVE N"N" TO LS10-AT-DIRECTION  (1)   
      *       MOVE N"TRAVEL" TO LS10-AT-DESCRIPTION (1)
      *       MOVE WS-PTP-DISTANCE     TO LS10-AT-DISTANCE (1)
      *       MOVE WS-PTP-TIME         TO LS10-AT-TIME (1) 
      *       MOVE LS10-FROM-X-CORD    TO LS10-AT-FROM-X (1)   
      *       MOVE LS10-FROM-Y-CORD    TO LS10-AT-FROM-Y (1)   
      *       MOVE LS10-TO-X-CORD      TO LS10-AT-TO-X (1)   
      *       MOVE LS10-TO-Y-CORD      TO LS10-AT-TO-Y (1)
      *       MOVE ZERO TO  LS10-AT-TURN-TIME    (1)
      *       MOVE 1 TO LS10-AT-SUB
      *       GO TO 0000-LS10-EXIT.
      *** replace debug code with code to select LS10 variables rather than used what's passed
      *** only use the from-location and to-location. get the rest from location table, as dble-byte
      *** has corrupted the values
      *** temp logic until we figure out the issue
             PERFORM VARYING LS10-AT-SUB FROM 1 BY 1 
               UNTIL LS10-AT-SUB > 19
               INITIALIZE LS10-AT-TO-SLOT(LS10-AT-SUB)
                          LS10-AT-FROM-SLOT(LS10-AT-SUB)  
                          LS10-AT-DIRECTION(LS10-AT-SUB)
                          LS10-AT-DESCRIPTION(LS10-AT-SUB)
                          LS10-AT-DISTANCE(LS10-AT-SUB)
                          LS10-AT-TIME(LS10-AT-SUB)
                          LS10-AT-FROM-X(LS10-AT-SUB)
                          LS10-AT-FROM-Y(LS10-AT-SUB)
                          LS10-AT-TO-X(LS10-AT-SUB)
                          LS10-AT-TO-Y(LS10-AT-SUB)
                          LS10-AT-TURN-TIME(LS10-AT-SUB)
             END-PERFORM.
             
             MOVE 0 TO LS10-AT-SUB
             EXEC SQL
                   Select aisle
                         ,bay
                         ,x_cord_refl
                         ,y_cord_refl
                         ,location_type
                     into :LS10-FROM-AISLE
                         ,:LS10-FROM-BAY
                         ,:LS10-FROM-X-CORD
                         ,:LS10-FROM-Y-CORD
                         ,:LS10-FROM-TYPE
                     from scwt_location
                    where cpy_cd = :LS10-CPY-CD
                      and loc_facl = :LS10-FACL
                      and loc_whse = :LS10-WHSE
                      and location = :LS10-FROM-LOCATION
             END-EXEC.

             if wf-debug = N"Y"
                display "sqlcode/sqlstate = " sqlcode "/" sqlstate

                display "from location lookup"
                display "from-location = " LS10-FROM-LOCATION
                display "from-aisle = " ls10-from-aisle
                display "from-bay = " ls10-from-bay
                display "from xcord = " ls10-from-x-cord
                display "from ycord = " ls10-from-y-cord
                display "from type = " ls10-from-type
             end-if.

                EXEC SQL
                   Select aisle
                         ,bay
                         ,x_cord_refl
                         ,y_cord_refl
                         ,z_cord_refl
                         ,location_type
                         ,slot_type
                         ,slot_config
                     into :LS10-TO-AISLE
                         ,:LS10-TO-BAY
                         ,:LS10-TO-X-CORD
                         ,:LS10-TO-Y-CORD
                         ,:LS10-TO-Z-CORD
                         ,:LS10-TO-TYPE
                         ,:LS10-TO-SLOT-TYPE
                         ,:LS10-TO-SLOT-CONFIG
                     from scwt_location
                    where cpy_cd = :LS10-CPY-CD
                      and loc_facl = :LS10-FACL
                      and loc_whse = :LS10-WHSE
                      and location = :LS10-to-location
                END-EXEC

             if wf-debug = N"Y"
                display "sqlcode/sqlstate = " sqlcode "/" sqlstate

                display "to location lookup"
                display "to-location = " LS10-to-LOCATION
                display "to-aisle = " ls10-to-aisle
                display "to-bay = " ls10-to-bay
                display "to xcord = " ls10-to-x-cord
                display "to ycord = " ls10-to-y-cord
                display "to zcord = " ls10-to-y-cord
                display "to type = " ls10-to-type
                display "to slot type = " ls10-to-slot-type
                display "to slot config = " ls10-to-slot-config
             end-if.
   
      *** INITIALIZE THE VARIABLES
          PERFORM 1000-INITIALIZE
            
      *** FIND THE ACCESS POINTS FOR OUT FROM AND TO LOCATIONS 
          PERFORM 0500-FIND-LOCATION-AREAS
             
      *** IF ACCESS AREAS ARE DIFFERENT NEED TO FIND PATHS 
          IF ARFS-ACCESS-AREA NOT = ARTS-ACCESS-AREA 
             PERFORM 3000-AREA-MOVE 
          ELSE  
          
      *** IF OUR ACCESS AREAS HAVE NOT CHANGED THEN SIMPLY 
      *** CALCULATE THE BEST ROUTE WITHIN THE AREA  
             EVALUATE LS10-FROM-TYPE ALSO LS10-TO-TYPE
          
      *** MOVE FROM SLOT TO SLOT 
             WHEN N"01" ALSO N"01"
               PERFORM 0100-SLOT-TO-SLOT
           
      *** MOVE FROM SLOT TO NON SLOT 
             WHEN N"01" ALSO ANY     
               PERFORM 0200-SLOT-TO-NON
      
      *** MOVE FROM NON SLOT TO SLOT 
             WHEN ANY ALSO N"01"
               PERFORM 0300-NON-TO-SLOT 
          
      *** MOVE FROM NON SLOT TO NON SLOT 
             WHEN OTHER 
               PERFORM 0400-NON-TO-NON
          END-EVALUATE.
          
          IF WF-DEBUG = N"Y"
             DISPLAY "******About to leave ls10"
             DISPLAY "LS10-TRAVEL-TIME= " LS10-TRAVEL-TIME
             perform varying ls10-at-sub from 1 by 1 
                until ls10-at-sub > 19
                   or ls10-at-time(ls10-at-sub) = 0
                   or ls10-at-distance(ls10-at-sub) = 0
               display "ls10-at-sub = " ls10-at-sub
               DISPLAY "LS10-AT-DISTANCE" 
                        LS10-AT-DISTANCE(LS10-AT-SUB)
               DISPLAY "LS10-AT-TIME" LS10-AT-TIME(LS10-AT-SUB)
               DISPLAY "LS10-AT-FROM-X(" LS10-AT-FROM-X(LS10-AT-SUB)
               DISPLAY "LS10-AT-FROM-Y" LS10-AT-FROM-Y(LS10-AT-SUB)
               DISPLAY "LS10-AT-TO-X" LS10-AT-TO-X(LS10-AT-SUB)
               DISPLAY "LS10-AT-TO-Y" LS10-AT-TO-Y(LS10-AT-SUB)
             DISPLAY "******LEAVING LS10"
             end-perform             
             
          END-IF.
       0000-LS10-EXIT.
            GOBACK.
      
      
       0100-SLOT-TO-SLOT SECTION.
       0100-BEGIN.
           MOVE "0100-SLOT-TO-SLOT" TO WX-PARA.
           IF WF-DEBUG                  = N"Y" OR N"P"
              DISPLAY "* SCWLS10: " WX-PARA
           END-IF.   
      *** IF THE AISLES ARE THE SAME CALCULATE SIMPLE BAY MOVE 
      *** DISTANCE AND TIME 
            IF LS10-TO-AISLE = LS10-FROM-AISLE 
              MOVE LS10-TO-AISLE   TO WS-BAY-AISLE
              MOVE LS10-FROM-X-CORD TO WS-BAY-FROM-X 
              MOVE LS10-FROM-Y-CORD TO WS-BAY-FROM-Y 
              MOVE LS10-TO-X-CORD  TO WS-BAY-TO-X
              MOVE LS10-TO-Y-CORD  TO WS-BAY-TO-Y 
              PERFORM 4300-BAY-TRAVEL 
              MOVE WS-BAY-DISTANCE TO LS10-TRAVEL-DISTANCE 
              MOVE WS-BAY-TIME     TO LS10-TRAVEL-TIME
            ELSE   
      
      *** IF THE AISLES ARE DIFFERENT NEED TO INCLUDE ACCESS TIMES AND 
      *** BAY TRAVELS 
              PERFORM 2000-BEST-NO-AREA-CHANGE
              MOVE WS-BEST-DISTANCE TO LS10-TRAVEL-DISTANCE 
              MOVE WS-BEST-TIME    TO LS10-TRAVEL-TIME 
            END-IF.       
      
            if wf-debug = N"Y"
               display "LS10-TRAVEL-TIME=" LS10-TRAVEL-TIME
            end-if.
       0100-EXIT.
            EXIT.
      
      
            
       0200-SLOT-TO-NON SECTION.
       0200-BEGIN.
           MOVE "0200-SLOT-TO-NON" TO WX-PARA.
           IF WF-DEBUG                  = N"Y" OR N"P"
              DISPLAY "* SCWLS10: " WX-PARA
           END-IF.   
            PERFORM 2000-BEST-NO-AREA-CHANGE
            MOVE WS-BEST-DISTANCE TO LS10-TRAVEL-DISTANCE 
            MOVE WS-BEST-TIME    TO LS10-TRAVEL-TIME 
            .     
            if wf-debug = N"Y"
               display "LS10-TRAVEL-TIME=" LS10-TRAVEL-TIME
            end-if.
       0200-EXIT.
            EXIT.
      
         
      
       0300-NON-TO-SLOT SECTION.
       0300-BEGIN.
           MOVE "0300-NON-TO-SLOT" TO WX-PARA.
           IF WF-DEBUG                  = N"Y" OR N"P"
              DISPLAY "* SCWLS10: " WX-PARA
           END-IF.   
            PERFORM 2000-BEST-NO-AREA-CHANGE
            MOVE WS-BEST-DISTANCE TO LS10-TRAVEL-DISTANCE 
            MOVE WS-BEST-TIME    TO LS10-TRAVEL-TIME 
            .    
            if wf-debug = N"Y"
               display "LS10-TRAVEL-TIME=" LS10-TRAVEL-TIME
            end-if.
       0300-EXIT.
            EXIT.
      
            
      
       0400-NON-TO-NON SECTION.
      *** THIS SECTION COMPUTES THE DISTANCE TO MOVE FROM TWO NON 
      *** SLOT LOCATIONS 
       0400-BEGIN. 
           MOVE "0400-NON-TO-NON" TO WX-PARA.
           IF WF-DEBUG                  = N"Y" OR N"P"
              DISPLAY "* SCWLS10: " WX-PARA
           END-IF.   
            MOVE LS10-FROM-X-CORD TO WS-PTP-FROM-X 
            MOVE LS10-FROM-Y-CORD TO WS-PTP-FROM-Y 
            MOVE LS10-TO-X-CORD  TO WS-PTP-TO-X 
            MOVE LS10-TO-Y-CORD  TO WS-PTP-TO-Y 
            PERFORM 4200-PTP-DISTANCE 
            COMPUTE LS10-TRAVEL-DISTANCE ROUNDED = 
               WS-PTP-DISTANCE 
            END-COMPUTE 
            COMPUTE LS10-TRAVEL-TIME ROUNDED = 
              WS-PTP-TIME 
            END-COMPUTE.
            if wf-debug = N"Y"
               display "LS10-TRAVEL-TIME=" LS10-TRAVEL-TIME
            end-if.
       0400-EXIT.
            EXIT.
      
      
       0500-FIND-LOCATION-AREAS SECTION. 
       0500-BEGIN.
           MOVE "0500-FIND-LOCATION-AREAS" TO WX-PARA.
           IF WF-DEBUG                  = N"Y" OR N"P"
              DISPLAY "* SCWLS10: " WX-PARA
              DISPLAY "* SCWLS10:FROM-LOCATION ="
                         LS10-FROM-LOCATION 
              DISPLAY "* SCWLS10:TO-LOCATION ="
                         LS10-TO-LOCATION 
              DISPLAY "* SCWLS10:FACL ="
                         LS10-FACL
              DISPLAY "* SCWLS10:WHSE ="
                         LS10-WHSE
              
           END-IF.   
      ***
      *** LOAD THE ACCESS POINT INFOMRATION FOR THE FROM LOCATION 
      ***
      
           EXEC SQL 
              SELECT    HIGH_X_COORD,
                        LOW_X_COORD,
                        HIGH_Y_COORD,
                        LOW_Y_COORD,
                        BAY_LENGTH,
                        ACCESS_AREA,
                        HIGH_LOCATION,
                        LOW_LOCATION
               FROM SCWT_ACCESS_POINTS  
               INTO     :ARFS-END-X,
                        :ARFS-BEGIN-X,
                        :ARFS-END-Y,
                        :ARFS-BEGIN-Y,
                        :ARFS-BAY-LENGTH,
                        :ARFS-ACCESS-AREA,
                        :ARFS-HIGH-LOCATION, 
                        :ARFS-LOW-LOCATION
               WHERE CPY_CD = :LS10-CPY-CD
                 AND FACL   = :LS10-FACL
                 AND WHSE   = :LS10-WHSE 
                 AND RECORD_STATUS = 'A'
                 AND LOW_LOCATION <= :LS10-FROM-LOCATION 
                 AND HIGH_LOCATION  >= :LS10-FROM-LOCATION 
           END-EXEC.
      
           IF SQLCODE NOT = ZERO 
              INITIALIZE ARFS-END-X,
                         ARFS-BEGIN-X,
                         ARFS-END-Y,
                         ARFS-BEGIN-Y,
                         ARFS-BAY-LENGTH,
                         ARFS-ACCESS-AREA
                         ARFS-HIGH-LOCATION 
                         ARFS-LOW-LOCATION
                         ARFS-BEGIN-DISTANCE 
                         ARFS-BEGIN-TIME
                         ARFS-END-DISTANCE
                         ARFS-END-TIME
           
           ELSE
      
      *** FIND THE TRAVEL TIMES AND DISTANCES TO EACH ACCESS POINT 
               IF LS10-FROM-TYPE NOT = N"01" 
                  MOVE ZEROES TO ARFS-BEGIN-DISTANCE 
                                 ARFS-BEGIN-TIME 
                                 ARFS-END-DISTANCE 
                                 ARFS-END-TIME 
               ELSE 
                  MOVE LS10-FROM-AISLE TO WS-BAY-AISLE
                  MOVE LS10-FROM-X-CORD TO WS-BAY-FROM-X 
                  MOVE LS10-FROM-Y-CORD TO WS-BAY-FROM-Y 
                  MOVE ARFS-END-X      TO WS-BAY-TO-X
                  MOVE ARFS-END-Y      TO WS-BAY-TO-Y 
                  if wf-debug = N"Y"
                     display "ls10 500- performing 4300-bay-travel-1"
                     display "ws-bay-aisle= "  ws-bay-aisle
                     display "ws-bay-from-x= " ws-bay-from-x
                     display "ws-bay-from-y= " ws-bay-from-y
                     display "ws-bay-to-x= "   ws-bay-to-x
                     display "ws-bay-to-y= "   ws-bay-to-y
                  end-if
                  PERFORM 4300-BAY-TRAVEL 
                  MOVE WS-BAY-DISTANCE TO ARFS-END-DISTANCE 
                  MOVE WS-BAY-TIME     TO ARFS-END-TIME
                  MOVE LS10-FROM-X-CORD TO WS-BAY-FROM-X 
                  MOVE LS10-FROM-Y-CORD TO WS-BAY-FROM-Y 
                  MOVE ARFS-BEGIN-X    TO WS-BAY-TO-X
                  MOVE ARFS-BEGIN-Y    TO WS-BAY-TO-Y 
                  if wf-debug = N"Y"
                     display "ls10 500- performing 4300-bay-travel-2"
                     display "ws-bay-aisle= "  ws-bay-aisle
                     display "ws-bay-from-x= " ws-bay-from-x
                     display "ws-bay-from-y= " ws-bay-from-y
                     display "ws-bay-to-x= "   ws-bay-to-x
                     display "ws-bay-to-y= "   ws-bay-to-y
                  end-if
                  PERFORM 4300-BAY-TRAVEL 
                  MOVE WS-BAY-DISTANCE TO ARFS-BEGIN-DISTANCE 
                  MOVE WS-BAY-TIME     TO ARFS-BEGIN-TIME 
               END-IF
           END-IF.
              
      
      
      *** CHECK IF THIS AISLE HAS A SUBSTITUTE DEFINED            
           MOVE LS10-TO-SLOT-TYPE TO WS-SLOT-TYPE 
           IF PICK-TYPE 
              PERFORM 0510-AISLE-SUBS                       
           END-IF. 
           
      ***     
      *** LOAD THE ACCESS POINT INFORMATION FOR THE TO LOCATION 
      ***
           EXEC SQL 
              SELECT    HIGH_X_COORD,
                        LOW_X_COORD,
                        HIGH_Y_COORD,
                        LOW_Y_COORD,
                        BAY_LENGTH,
                        ACCESS_AREA,
                        HIGH_LOCATION,
                        LOW_LOCATION
               FROM SCWT_ACCESS_POINTS  
               INTO     :ARTS-END-X,
                        :ARTS-BEGIN-X,
                        :ARTS-END-Y,
                        :ARTS-BEGIN-Y,
                        :ARTS-BAY-LENGTH,
                        :ARTS-ACCESS-AREA,
                        :ARTS-HIGH-LOCATION,
                        :ARTS-LOW-LOCATION
               WHERE CPY_CD = :LS10-CPY-CD
                 AND FACL   = :LS10-FACL
                 AND WHSE   = :LS10-WHSE 
                 AND RECORD_STATUS = 'A'
                 AND LOW_LOCATION <= :LS10-TO-LOCATION 
                 AND HIGH_LOCATION  >= :LS10-TO-LOCATION 
           END-EXEC   
      
           IF SQLCODE NOT = ZERO 
              INITIALIZE ARTS-END-X,
                         ARTS-BEGIN-X,
                         ARTS-END-Y,
                         ARTS-BEGIN-Y,
                         ARTS-BAY-LENGTH,
                         ARTS-ACCESS-AREA,
                         ARTS-HIGH-LOCATION,
                         ARTS-LOW-LOCATION,
                         ARTS-BEGIN-DISTANCE,
                         ARTS-BEGIN-TIME,
                         ARTS-END-TIME,
                         ARTS-END-DISTANCE
           ELSE 
      
      *** FIND THE TRAVEL TIMES AND DISTANCES TO EACH ACCESS POINT 
              IF LS10-TO-TYPE NOT = N"01" 
                 MOVE ZEROES TO ARTS-BEGIN-DISTANCE 
                                ARTS-BEGIN-TIME 
                                ARTS-END-DISTANCE 
                                ARTS-END-TIME 
              ELSE 
                 MOVE LS10-TO-AISLE   TO WS-BAY-AISLE
                 MOVE LS10-TO-X-CORD  TO WS-BAY-FROM-X 
                 MOVE LS10-TO-Y-CORD  TO WS-BAY-FROM-Y 
                 MOVE ARTS-END-X      TO WS-BAY-TO-X
                 MOVE ARTS-END-Y      TO WS-BAY-TO-Y 
                  if wf-debug = N"Y"
                     display "ls10 500- performing 4300-bay-travel-3"
                     display "ws-bay-aisle= "  ws-bay-aisle
                     display "ws-bay-from-x= " ws-bay-from-x
                     display "ws-bay-from-y= " ws-bay-from-y
                     display "ws-bay-to-x= "   ws-bay-to-x
                     display "ws-bay-to-y= "   ws-bay-to-y
                  end-if
                 PERFORM 4300-BAY-TRAVEL 
                 MOVE WS-BAY-DISTANCE TO ARTS-END-DISTANCE 
                 MOVE WS-BAY-TIME     TO ARTS-END-TIME
              
                 MOVE LS10-TO-X-CORD TO WS-BAY-FROM-X 
                 MOVE LS10-TO-Y-CORD TO WS-BAY-FROM-Y 
                 MOVE ARTS-BEGIN-X    TO WS-BAY-TO-X
                 MOVE ARTS-BEGIN-Y    TO WS-BAY-TO-Y 
                  if wf-debug = N"Y"
                     display "ls10 500- performing 4300-bay-travel-4"
                     display "ws-bay-aisle= "  ws-bay-aisle
                     display "ws-bay-from-x= " ws-bay-from-x
                     display "ws-bay-from-y= " ws-bay-from-y
                     display "ws-bay-to-x= "   ws-bay-to-x
                     display "ws-bay-to-y= "   ws-bay-to-y
                  end-if
                 PERFORM 4300-BAY-TRAVEL 
                 MOVE WS-BAY-DISTANCE TO ARTS-BEGIN-DISTANCE 
                 MOVE WS-BAY-TIME     TO ARTS-BEGIN-TIME 
              END-IF
           END-IF.
       
       0500-EXIT. 
            EXIT. 
            
      
       0510-AISLE-SUBS SECTION.
      *** THIS SECTION CHECKS TO SEE IF AISLE SUBSTITUTION EXISTS FOR THIS AISLE 
       0510-BEGIN. 
           MOVE "0510-AISLE-SUBS" TO WX-PARA.
           IF WF-DEBUG                  = N"Y" OR N"P"
              DISPLAY "* SCWLS10: " WX-PARA
           END-IF.   
           MOVE N"N" TO WS-SUB-FOUND 
      *** FIND THE SIDE OF THE AISLE 
           DIVIDE LS10-TO-BAY BY 2 
             GIVING WS-JUNK REMAINDER WS-REM 
           END-DIVIDE 
           IF WS-REM = 0 
              MOVE N"E" TO EOA 
           ELSE 
              MOVE N"O" TO EOA 
           END-IF
                      
      *** DECLARE OUR CURSOR 
      * OT Upgrade - Specify fields on select
      *    EXEC SQL 
      *       DECLARE AisleSubs CURSOR FOR 
      *        SELECT  *
      *          FROM    SCWT_AISLE_SUBS
      *          WHERE   CPY_CD           =  :LS10-CPY-CD
      *          AND     facl             =  :LS10-FACL
      *          AND     whse             =  :LS10-WHSE
      *          AND   substr(:LS10-TO-AISLE,1,3) concat 
      *                substr(digits(:LS10-TO-BAY),8,3)
      *           BETWEEN HIGH_AISLE CONCAT substr(digits(HIGH_BAY),8,3)
      *                 AND LOW_AISLE CONCAT substr(digits(LOW_BAY),8,3)                    
      *          AND     RECORD_STATUS    =  'A'   
      *          AND     ODD_OR_EVEN      in (:EOA, 'A')
      *          AND     PICK_SLOT_TYPE   IN (:LS10-TO-SLOT-TYPE, ' ')
      *          AND     PICK_SLOT_CONFIG IN (:LS10-TO-SLOT-CONFIG, ' ')
      *          ORDER BY ODD_OR_EVEN DESC,
      *                   PICK_SLOT_TYPE DESC, 
      *                   PICK_SLOT_CONFIG DESC
      *     END-EXEC 
          invoke int-val "getParsedString"
                 using LS10-TO-AISLE
                 returning wx-aa
          end-invoke
          if wf-debug = N"Y"
            display "ls10-to-aisle=" ls10-to-aisle "!"
            display "wx-aa=" wx-aa "!"
          end-if
          move ls10-to-bay to w9-bb
          
          EXEC SQL 
             DECLARE AisleSubs CURSOR FOR 
              SELECT  CPY_CD
                       ,FACL
                       ,WHSE
                       ,HIGH_AISLE
                       ,HIGH_BAY
                       ,LOW_AISLE
                       ,LOW_BAY
                       ,ODD_OR_EVEN
                       ,MAX_Z_COORD
                       ,PICK_SLOT_TYPE
                       ,PICK_SLOT_CONFIG
                       ,SUB_AISLE
                       ,RECORD_STATUS                                         
                FROM    SCWT_AISLE_SUBS
                WHERE   CPY_CD           =  :LS10-CPY-CD
                AND     facl             =  :LS10-FACL
                AND     whse             =  :LS10-WHSE
                AND   :WS-aaabbb
                 BETWEEN HIGH_AISLE CONCAT substr(digits(HIGH_BAY),8,3)
                       AND LOW_AISLE CONCAT substr(digits(LOW_BAY),8,3)                    
                AND     RECORD_STATUS    =  'A'   
                AND     ODD_OR_EVEN      in (:EOA, 'A')
                AND     PICK_SLOT_TYPE   IN (:LS10-TO-SLOT-TYPE, ' ')
                AND     PICK_SLOT_CONFIG IN (:LS10-TO-SLOT-CONFIG, ' ')
                ORDER BY ODD_OR_EVEN DESC,
                         PICK_SLOT_TYPE DESC, 
                         PICK_SLOT_CONFIG DESC
           END-EXEC 
      * OT Upgrade - Specify fields on select
           
      ** OPEN THE CURSOR 
           IF SQLCODE NOT = 0 
              GO TO 0510-EXIT
           ELSE 
              EXEC SQL 
                OPEN AisleSubs 
              END-EXEC 
      * begin OT upgrade cursor check
           if wf-debug = N"Y" or N"P"
               display "OT upgrade cursor check 034 cursor AisleSubs"
               display "sqlcode = " sqlcode ";sqlstate= " sqlstate
           END-IF           
      * end   OT upgrade cursor check
              IF SQLCODE NOT = 0 
                 GO TO 0510-EXIT 
              END-IF 
           END-IF 
           
      
      *** LOOP THRU THE ROWS UNTIL WE FIND THE RECORD THAT MEETS OUR CRITERIA 
            MOVE N"N" TO WS-END-QUEST 
            PERFORM UNTIL WS-END-QUEST = N"Y" 
      * OT Upgrade - Specify fields on select
      *        EXEC SQL 
      *           FETCH AisleSubs 
      *            INTO :SCWT-AISLE-SUBS 
      *        END-EXEC 
              EXEC SQL 
                 FETCH AisleSubs 
                  INTO  :SCWT-AISLE-SUBS.CPY-CD
                       ,:SCWT-AISLE-SUBS.FACL
                       ,:SCWT-AISLE-SUBS.WHSE
                       ,:SCWT-AISLE-SUBS.HIGH-AISLE
                       ,:SCWT-AISLE-SUBS.HIGH-BAY
                       ,:SCWT-AISLE-SUBS.LOW-AISLE
                       ,:SCWT-AISLE-SUBS.LOW-BAY
                       ,:SCWT-AISLE-SUBS.ODD-OR-EVEN
                       ,:SCWT-AISLE-SUBS.MAX-Z-COORD
                       ,:SCWT-AISLE-SUBS.PICK-SLOT-TYPE
                       ,:SCWT-AISLE-SUBS.PICK-SLOT-CONFIG
                       ,:SCWT-AISLE-SUBS.SUB-AISLE
                       ,:SCWT-AISLE-SUBS.RECORD-STATUS                                              
              END-EXEC 
      * OT Upgrade - Specify fields on select
      
              IF SQLCODE NOT = 0 
                MOVE N"Y" TO WS-END-QUEST 
              ELSE 
               IF  (ODD-OR-EVEN OF SCWT-AISLE-SUBS = N"A" 
                   OR ODD-OR-EVEN OF SCWT-AISLE-SUBS = EOA) 
               AND (PICK-SLOT-CONFIG OF SCWT-AISLE-SUBS = N"A"
                   OR  PICK-SLOT-CONFIG OF SCWT-AISLE-SUBS 
                       = LS10-TO-SLOT-CONFIG) 
               AND ( PICK-SLOT-TYPE OF SCWT-AISLE-SUBS = N"A"
                   OR PICK-SLOT-TYPE OF SCWT-AISLE-SUBS 
                       = LS10-TO-SLOT-TYPE) 
                    MOVE N"Y" TO WS-END-QUEST 
                    MOVE N"Y" TO WS-SUB-FOUND 
                END-IF 
              END-IF 

      *******CDIDBCS-START-ECOBOL2                                              
	        INVOKE INT-VAL "expandString"                                           
                               USING maint-date of SCWT-AISLE-SUBS   
                               RETURNING maint-date of SCWT-AISLE-SUBS                     
      *******CDIDBCS-END-ECOBOL2                                              
            END-PERFORM.
            
            EXEC SQL 
              CLOSE AisleSubs 
            END-EXEC
            IF SQLCODE NOT = 0 
               CONTINUE 
            END-IF 
            .
            
       0510-EXIT.
            EXIT. 
             
             
                               
       1000-INITIALIZE SECTION. 
      ** THIS SECTION INITIALIZES THE WS VARAIBLES USED IN THE PROGRAM
      ** AND FINDS THE GENERIC MESSAGE TEXT  
       1000-BEGIN.
           MOVE "1000-INITIALIZE SECTION" TO WX-PARA.
           IF WF-DEBUG                  = N"Y" OR N"P"
              DISPLAY "* SCWLS10: " WX-PARA
           END-IF.   
            INITIALIZE WS-PATHS
                       SCWT-FK-BAY-TRAVEL
                       WS-TO-AA-AREA 
                       WS-FROM-AA-AREA
      *** FIND THE SYSTEM LANGUAGE 
           EXEC SQL 
               SELECT SYSTEM_LANGUAGE 
                 FROM SCWT_SYS_PARMS
                 INTO :SCWT-SYS-PARMS.SYSTEM-LANGUAGE
                WHERE CPY_CD = :LS10-CPY-CD
                  AND FACL   = :LS10-FACL 
                  AND WHSE   = :LS10-WHSE  
           END-EXEC
           
      **** GET THE BAY MESSAGE 
           EXEC SQL 
               SELECT SUBSTR(MESSAGE_TEXT,11,30) 
                 FROM SCWT_MESSAGE_TEXT
                 INTO :WS-BAY-MOVE-LITERAL
                WHERE CPY_CD = :LS10-CPY-CD 
                  AND SYSTEM_LANGUAGE = SCWT-SYS-PARMS.SYSTEM_LANGUAGE
                  AND MESSAGE_ID = "RFI0071" 
           END-EXEC
      
      **** DEFAULT TO SPACES IF NOT FOUND 
           IF SQLCODE NOT = 0 
              MOVE SPACES TO WS-BAY-MOVE-LITERAL
           END-IF
                 
           EXEC SQL 
               SELECT SUBSTR(MESSAGE_TEXT,11,30) 
                 FROM SCWT_MESSAGE_TEXT
                 INTO :WS-AISLE-MOVE-LITERAL
                WHERE CPY_CD = :LS10-CPY-CD 
                  AND SYSTEM_LANGUAGE = SCWT-SYS-PARMS.SYSTEM_LANGUAGE
                  AND MESSAGE_ID = "RFI0072" 
           END-EXEC    
      
      
      **** DEFAULT TO SPACES IF NOT FOUND 
           IF SQLCODE NOT = 0 
              MOVE SPACES TO WS-AISLE-MOVE-LITERAL
           END-IF
      
      **** ACCESS MOVE LITERAL                
           EXEC SQL 
               SELECT SUBSTR(MESSAGE_TEXT,11,30) 
                 FROM SCWT_MESSAGE_TEXT
                 INTO :WS-ACCESS-MOVE-LITERAL
                WHERE CPY_CD = :LS10-CPY-CD 
                  AND SYSTEM_LANGUAGE = SCWT-SYS-PARMS.SYSTEM_LANGUAGE
                  AND MESSAGE_ID = "RFI0073" 
           END-EXEC    
      
      **** DEFAULT TO SPACES IF NOT FOUND 
           IF SQLCODE NOT = 0 
              MOVE SPACES TO WS-ACCESS-MOVE-LITERAL
           END-IF
           
      **** ACCESS TIME LITERAL      
           EXEC SQL 
               SELECT SUBSTR(MESSAGE_TEXT,11,30) 
                 FROM SCWT_MESSAGE_TEXT
                 INTO :WS-ACCESS-TIME-LITERAL
                WHERE CPY_CD = :LS10-CPY-CD 
                  AND SYSTEM_LANGUAGE = SCWT-SYS-PARMS.SYSTEM_LANGUAGE
                  AND MESSAGE_ID = "RFI0074" 
           END-EXEC      
      
      **** DEFAULT TO SPACES IF NOT FOUND 
           IF SQLCODE NOT = 0 
              MOVE SPACES TO WS-ACCESS-TIME-LITERAL
           END-IF    
      
      **** FIND THE EQUIPMENT RECORD 
      * OT Upgrade - Specify fields on select
      *     EXEC SQL 
      *       SELECT * FROM SCWT_EQP_CODE 
      *       INTO :SCWT-EQP-CODE 
      *       WHERE CPY_CD         = :LS10-CPY-CD 
      *         AND FACL           = :LS10-FACL 
      *         AND WHSE           = :LS10-WHSE 
      *         AND EQUIPMENT_CODE = :LS10-EQUIP-CODE 
      *     END-EXEC 
           EXEC SQL 
             SELECT    c.CPY_CD
                      ,c.FACL
                      ,c.WHSE
                      ,c.EQUIPMENT_CODE
                      ,c.EQUIPMENT_DESC
                      ,c.WEIGHT_LIMIT
                      ,c.HEIGHT_STK_LIMIT
                      ,c.HEIGHT_LIMIT
                      ,c.EQUIPMENT_TYPE
                      ,c.RF_TYPE
                      ,c.EQP_HAS_SCALES
                      ,c.TURN_TIME_EMPTY
                      ,c.TURN_TIME_SINGLE
                      ,c.TURN_TIME_MULTIPLE
                      ,c.TRAVEL_RATE_EMPTY
                      ,c.TRAVEL_RATE_SINGLE
                      ,c.TRVL_RATE_MULTIPLE
                      ,c.ACCEL_RATE_EMPTY
                      ,c.ACCEL_RATE_SINGLE
                      ,c.ACCEL_RATE_MULTI
                      ,c.TURNING_DISTANCE
             INTO      :SCWT-EQP-CODE.CPY-CD
                      ,:SCWT-EQP-CODE.FACL
                      ,:SCWT-EQP-CODE.WHSE
                      ,:SCWT-EQP-CODE.EQUIPMENT-CODE
                      ,:SCWT-EQP-CODE.EQUIPMENT-DESC
                      ,:SCWT-EQP-CODE.WEIGHT-LIMIT
                      ,:SCWT-EQP-CODE.HEIGHT-STK-LIMIT
                      ,:SCWT-EQP-CODE.HEIGHT-LIMIT
                      ,:SCWT-EQP-CODE.EQUIPMENT-TYPE
                      ,:SCWT-EQP-CODE.RF-TYPE
                      ,:SCWT-EQP-CODE.EQP-HAS-SCALES
                      ,:SCWT-EQP-CODE.TURN-TIME-EMPTY
                      ,:SCWT-EQP-CODE.TURN-TIME-SINGLE
                      ,:SCWT-EQP-CODE.TURN-TIME-MULTIPLE
                      ,:SCWT-EQP-CODE.TRAVEL-RATE-EMPTY
                      ,:SCWT-EQP-CODE.TRAVEL-RATE-SINGLE
                      ,:SCWT-EQP-CODE.TRVL-RATE-MULTIPLE
                      ,:SCWT-EQP-CODE.ACCEL-RATE-EMPTY
                      ,:SCWT-EQP-CODE.ACCEL-RATE-SINGLE
                      ,:SCWT-EQP-CODE.ACCEL-RATE-MULTI
                      ,:SCWT-EQP-CODE.TURNING-DISTANCE
             FROM SCWT_EQUIPMENT e
                 ,SCWT_EQP_CODE c
             WHERE c.CPY_CD         = e.cpy_cd 
               AND c.FACL           = e.facl 
               AND c.WHSE           = e.whse
               AND c.EQUIPMENT_CODE = e.equipment_code
               AND e.CPY_CD         = :LS10-CPY-CD 
               AND e.FACL           = :LS10-FACL 
               AND e.WHSE           = :LS10-WHSE 
               AND e.EQUIPMENT_ID   = :LS10-EQUIP-CODE                
           END-EXEC 
      * OT Upgrade - Specify fields on select
           
           IF SQLCODE NOT = 0 
              INITIALIZE SCWT-EQP-CODE 
           END-IF 
           
            .
       1000-EXIT. 
            EXIT.
      
      
      
       2000-BEST-NO-AREA-CHANGE SECTION.
      **** THIS SECTION FINDS THE BEST TRAVEL ROUTE BETWEEN TWO POINTS 
      **** IN THE SAME ACCESS AREA 
       2000-BEGIN.
           MOVE "2000-BEST-NO-AREA-CHANGE" TO WX-PARA.
           IF WF-DEBUG                  = N"Y" OR N"P"
              DISPLAY "* SCWLS10: " WX-PARA
           END-IF.   
      
            INITIALIZE WS-BEST      
            
      *** INITIALIZE OUR TRAVEL TABLE 
              MOVE ALL N"0" TO TRAVEL-ROUTES-TABLE  
      
      *** CALCULATE THE DISTANCES FOR THE FROM LOCATION TO ITS ACCESS POINT 
              MOVE ARFS-BEGIN-DISTANCE 
               TO TVL-FROM-ACCESS-DIST(1,2) 
                   TVL-FROM-ACCESS-DIST(1,1)
              MOVE ARFS-END-DISTANCE 
               TO TVL-FROM-ACCESS-DIST(2,1) 
                   TVL-FROM-ACCESS-DIST(2,2)
              MOVE ARFS-BEGIN-TIME
               TO TVL-FROM-ACCESS-TIME(1,2) 
                   TVL-FROM-ACCESS-TIME(1,1)
              MOVE ARFS-END-TIME 
               TO TVL-FROM-ACCESS-TIME(2,1) 
                   TVL-FROM-ACCESS-TIME(2,2)
                   
              
      **** BEGIN AP TO BEGIN AP 
              MOVE ARFS-BEGIN-X        TO WS-PTP-FROM-X 
              MOVE ARFS-BEGIN-Y        TO WS-PTP-FROM-Y 
              MOVE ARTS-BEGIN-X        TO WS-PTP-TO-X 
              MOVE ARTS-BEGIN-Y        TO WS-PTP-TO-Y 
              PERFORM 4200-PTP-DISTANCE 
              MOVE WS-PTP-DISTANCE     TO TVL-PATH-DIST (1,1)
              MOVE WS-PTP-TIME         TO TVL-PATH-TIME (1,1)
      
      **** BEGIN AP TO END AP 
              MOVE ARFS-BEGIN-X        TO WS-PTP-FROM-X 
              MOVE ARFS-BEGIN-Y        TO WS-PTP-FROM-Y 
              MOVE ARTS-END-X          TO WS-PTP-TO-X 
              MOVE ARTS-END-Y          TO WS-PTP-TO-Y 
              PERFORM 4200-PTP-DISTANCE 
              MOVE WS-PTP-DISTANCE     TO TVL-PATH-DIST (1,2)
              MOVE WS-PTP-TIME         TO TVL-PATH-TIME (1,2)
      
      **** END AP TO BEGIN AP 
              MOVE ARFS-END-X          TO WS-PTP-FROM-X 
              MOVE ARFS-END-Y          TO WS-PTP-FROM-Y 
              MOVE ARTS-BEGIN-X        TO WS-PTP-TO-X 
              MOVE ARTS-BEGIN-Y        TO WS-PTP-TO-Y 
              PERFORM 4200-PTP-DISTANCE 
              MOVE WS-PTP-DISTANCE     TO TVL-PATH-DIST (2,1)
              MOVE WS-PTP-TIME         TO TVL-PATH-TIME (2,1)
      
      **** END AP TO END AP 
              MOVE ARFS-END-X          TO WS-PTP-FROM-X 
              MOVE ARFS-END-Y          TO WS-PTP-FROM-Y 
              MOVE ARTS-END-X          TO WS-PTP-TO-X 
              MOVE ARTS-END-Y          TO WS-PTP-TO-Y 
              PERFORM 4200-PTP-DISTANCE 
              MOVE WS-PTP-DISTANCE     TO TVL-PATH-DIST (2,2)
              MOVE WS-PTP-TIME         TO TVL-PATH-TIME (2,2)
      
      *** CALCULATE THE DISTANCE FOR THE TO LOCATION TO ITS ACCESS POINT 
              MOVE ARTS-BEGIN-DISTANCE 
               TO TVL-TO-ACCESS-DIST(1,1), 
                   TVL-TO-ACCESS-DIST(2,1)
              MOVE ARTS-END-DISTANCE 
               TO TVL-TO-ACCESS-DIST(1,2),
                   TVL-TO-ACCESS-DIST(2,2)
              MOVE ARTS-BEGIN-TIME 
               TO TVL-TO-ACCESS-TIME(1,1), 
                   TVL-TO-ACCESS-TIME(2,1)
              MOVE ARTS-END-DISTANCE 
               TO TVL-TO-ACCESS-TIME(1,2),
                   TVL-TO-ACCESS-TIME(2,2)
                       
      
      *** CALCULATE THE TOTAL TRAVEL DISTANCES FOR EACH TRAVEL COMBINATION 
      *** IF THIS IS BETTER THAN THE CURRENT BEST DISTANCE UPDATE THE BEST 
      *** DISTANCE 
              PERFORM VARYING WS-FROM-SUB FROM +1 BY +1 
                UNTIL WS-FROM-SUB > 2
                PERFORM VARYING WS-TO-SUB FROM +1 BY +1 
                  UNTIL WS-TO-SUB > 2
                   COMPUTE WS-TOTAL-DISTANCE = 
                      TVL-FROM-ACCESS-DIST (WS-FROM-SUB, WS-TO-SUB) 
                    + TVL-PATH-DIST (WS-FROM-SUB, WS-TO-SUB) 
                    + TVL-TO-ACCESS-DIST (WS-FROM-SUB, WS-TO-SUB) 
                   END-COMPUTE 
                   COMPUTE WS-TOTAL-TIME = 
                      TVL-FROM-ACCESS-TIME (WS-FROM-SUB, WS-TO-SUB) 
                    + TVL-PATH-TIME (WS-FROM-SUB, WS-TO-SUB) 
                    + TVL-TO-ACCESS-TIME (WS-FROM-SUB, WS-TO-SUB) 
                   END-COMPUTE 
                   IF WS-TOTAL-TIME > WS-BEST-TIME
                      MOVE WS-TOTAL-DISTANCE TO WS-BEST-DISTANCE 
                      MOVE WS-TOTAL-TIME    TO WS-BEST-TIME 
                      MOVE WS-FROM-SUB      TO WS-BEST-FROM-AAP
                      MOVE WS-TO-SUB        TO WS-BEST-TO-AAP 
                      MOVE WS-SUB           TO WS-BEST-AP 
                      MOVE N"D"              TO WS-BEST-TYPE 
                      MOVE SPACES           TO WS-BEST-PATH 
                   END-IF 
                END-PERFORM           
              END-PERFORM 
      
            .
       2000-EXIT. 
            EXIT.
      
            
                 
      ** THIS SECTION CALCULATES THE DIFFERENT PATHS BETWEEN TWO AREAS AND 
      ** DETERMINES THE TRAVEL DISTANCE FOR EACH PATH     
       3000-AREA-MOVE SECTION. 
       3000-BEGIN. 
           MOVE "3000-AREA-MOVE" TO WX-PARA.
           IF WF-DEBUG                  = N"Y" OR N"P"
              DISPLAY "* SCWLS10: " WX-PARA
              display "ARFS-ACCESS-AREA=" ARFS-ACCESS-AREA "!"
              display "ARTS-ACCESS-AREA=" ARTS-ACCESS-AREA "!"
           END-IF.   
      *** FISRT LOCATE ALL COMMON DOORS BETWEEN THE AREAS 
            MOVE 0 TO WS-AP-COUNT 
            MOVE ARFS-ACCESS-AREA TO WS-COM-FROM-AREA 
            MOVE ARTS-ACCESS-AREA TO WS-COM-TO-AREA 
            PERFORM 4100-COMMON-DOORS 
      
      *** IF WE HAVE SOME COMMON DOORS THEN FIND THE BEST ONE 
            IF WS-AP-COUNT > 0 
               PERFORM 3200-BEST-COMMON-AP 
            END-IF 
                
      *** FIND ALL POSSIBLE PATHS BETWEEN THE AREAS 
            MOVE ZEROES TO WS-PATH-COUNT.     
            MOVE WS-COM-FROM-AREA TO WS-PATH-FROM-AREA 
            MOVE WS-COM-TO-AREA  TO WS-PATH-TO-AREA
            PERFORM 3100-BUILD-PATHS-TABLE 
      
      *** IF THERE ARE OTHER TRAVEL PATHS THEN EVALUATE THEM 
            IF WS-PATH-COUNT > 0 
               PERFORM 3300-BEST-ROUTE 
            END-IF 
            .
      *** NOW 
            
       3000-EXIT. 
            EXIT. 
         
            
            
       3100-BUILD-PATHS-TABLE SECTION. 
      *** THIS SECTION BUILDS THE WS TRAVEL PATHS TABLE OF ALL PATHS BETWEEN 
      *** TWO AREAS
       3100-BEGIN.
           MOVE "3100-BUILD-PATHS-TABLE" TO WX-PARA.
           IF WF-DEBUG                  = N"Y" OR N"P"
              DISPLAY "* SCWLS10: " WX-PARA
           END-IF.   
      *** FIRST LOAD ANY TRAVEL PATH INFORMATION 
            MOVE N"N" TO WS-END-QUEST 
                        WS-SQL-ERROR 
            PERFORM 3110-DECLARE-TRAVELPATH
            IF WS-SQL-ERROR = N"Y" 
               CONTINUE 
            ELSE 
               PERFORM UNTIL WS-END-QUEST = N"Y" 
                 PERFORM 3120-FETCH-TRAVELPATH 
                 IF WS-SQL-ERROR = N"Y" 
                    MOVE N"Y" TO WS-END-QUEST 
                 ELSE 
                    PERFORM 3130-LOAD-PATH-TABLE 
                    IF WS-PATH-COUNT = 10 
                       MOVE N"Y" TO WS-END-QUEST 
                    END-IF
                 END-IF 
               END-PERFORM 
               PERFORM 3140-CLOSE-TRAVELPATH
            END-IF   
            .
            
       3100-EXIT. 
            EXIT. 
      
      
            
       3200-BEST-COMMON-AP SECTION. 
      *** THIS SECTION FINDS THE BEST TRAVEL DISTANCE BETWEEN THE TWO POINTS 
      *** USING COMMON ACCESS POINTS BETWEEN THE AREAS
       3200-BEGIN.
           MOVE "3200-BEST-COMMON-AP" TO WX-PARA.
           IF WF-DEBUG                  = N"Y" OR N"P"
              DISPLAY "* SCWLS10: " WX-PARA
           END-IF.   
      *** NOW WE NEED TO GET CLEVER AND FIGURE OUT THE TOTAL TRAVEL DISTANCES FOR OUR 
      *** DIFFERENT ROUTES 
            INITIALIZE WS-BEST      
            
      *** NOW FIRST LOOP THRU THE COMMON DOORS TO FIGURE OUT TOTAL TRAVEL DISTANCES 
      *** FOR OUR FOUR POSSIBLE ACCESS POINTS       
            PERFORM VARYING WS-SUB FROM +1 BY +1 
              UNTIL WS-SUB > WS-AP-COUNT 
      *** INITIALIZE OUR TRAVEL TABLE 
              MOVE ALL N"0" TO TRAVEL-ROUTES-TABLE  
      
      *** CALCULATE THE DISTANCES FOR THE FROM LOCATION TO ITS ACCESS POINT 
              MOVE ARFS-BEGIN-DISTANCE TO TVL-FROM-ACCESS-DIST(1,2) 
                   TVL-FROM-ACCESS-DIST(1,1)
              MOVE ARFS-END-DISTANCE TO TVL-FROM-ACCESS-DIST(2,1) 
                   TVL-FROM-ACCESS-DIST(2,2)
              MOVE ARFS-BEGIN-TIME TO TVL-FROM-ACCESS-TIME(1,2) 
                   TVL-FROM-ACCESS-TIME(1,1)
              MOVE ARFS-END-TIME TO TVL-FROM-ACCESS-TIME(2,1) 
                   TVL-FROM-ACCESS-TIME(2,2)
                   
              
      *** NOW CALCULATE THE DISTANCE FROM THE FROM AISLE APS TO THE DOOR
      *** BEGIN ACCESS POINT 
              MOVE ARFS-BEGIN-X        TO WS-PTP-FROM-X 
              MOVE ARFS-BEGIN-Y        TO WS-PTP-FROM-Y 
              MOVE WS-AP-XCORD (WS-SUB) TO WS-PTP-TO-X 
              MOVE WS-AP-YCORD (WS-SUB) TO WS-PTP-TO-Y 
              PERFORM 4200-PTP-DISTANCE 
              MOVE WS-PTP-DISTANCE     TO TVL-PATH-DIST (1,1)
                                           TVL-PATH-DIST (1,2)   
              MOVE WS-PTP-TIME         TO TVL-PATH-TIME (1,1)
                                           TVL-PATH-TIME (1,2) 
                                       
      *** END ACCESS POINT 
              MOVE ARFS-END-X          TO WS-PTP-FROM-X 
              MOVE ARFS-END-Y          TO WS-PTP-FROM-Y 
              MOVE WS-AP-XCORD (WS-SUB) TO WS-PTP-TO-X 
              MOVE WS-AP-YCORD (WS-SUB) TO WS-PTP-TO-Y 
              PERFORM 4200-PTP-DISTANCE 
              MOVE WS-PTP-DISTANCE     TO TVL-PATH-DIST (2,1)
                                           TVL-PATH-DIST (2,2)   
              MOVE WS-PTP-TIME         TO TVL-PATH-TIME (2,1)
                                           TVL-PATH-TIME (2,2)     
                                       
      *** NOW CALCULATE THE DISTANCE FROM THE DOOR TO THE TO LOCATION APS
      *** BEGIN ACCESS POINT 
              MOVE ARTS-BEGIN-X        TO WS-PTP-FROM-X 
              MOVE ARTS-BEGIN-Y        TO WS-PTP-FROM-Y 
              MOVE WS-AP-XCORD (WS-SUB) TO WS-PTP-TO-X 
              MOVE WS-AP-YCORD (WS-SUB) TO WS-PTP-TO-Y 
              PERFORM 4200-PTP-DISTANCE 
              ADD WS-PTP-DISTANCE    TO TVL-PATH-DIST (1,1)
              ADD WS-PTP-DISTANCE    TO TVL-PATH-DIST (2,1) 
              ADD WS-PTP-TIME        TO TVL-PATH-TIME (1,1)
              ADD WS-PTP-TIME        TO TVL-PATH-TIME (2,1) 
      
      *** END ACCESS POINT 
              MOVE ARTS-END-X          TO WS-PTP-FROM-X 
              MOVE ARTS-END-Y          TO WS-PTP-FROM-Y 
              MOVE WS-AP-XCORD (WS-SUB) TO WS-PTP-TO-X 
              MOVE WS-AP-YCORD (WS-SUB) TO WS-PTP-TO-Y 
              PERFORM 4200-PTP-DISTANCE 
              ADD WS-PTP-DISTANCE    TO TVL-PATH-DIST (1,2)
              ADD WS-PTP-DISTANCE    TO TVL-PATH-DIST (2,2) 
              ADD WS-PTP-TIME        TO TVL-PATH-TIME (1,2)
              ADD WS-PTP-TIME        TO TVL-PATH-TIME (2,2) 
      
      *** CALCULATE THE DISTANCE FOR THE TO LOCATION TO ITS ACCESS POINT 
              MOVE ARTS-BEGIN-DISTANCE 
               TO TVL-TO-ACCESS-DIST(1,1), 
                   TVL-TO-ACCESS-DIST(2,1)
              MOVE ARTS-END-DISTANCE 
               TO TVL-TO-ACCESS-DIST(1,2),
                   TVL-TO-ACCESS-DIST(2,2)
              MOVE ARTS-BEGIN-TIME 
               TO TVL-TO-ACCESS-TIME(1,1), 
                   TVL-TO-ACCESS-TIME(2,1)
              MOVE ARTS-END-DISTANCE 
               TO TVL-TO-ACCESS-TIME(1,2),
                   TVL-TO-ACCESS-TIME(2,2)
                       
      
      *** CALCULATE THE TOTAL TRAVEL DISTANCES FOR EACH TRAVEL COMBINATION 
      *** IF THIS IS BETTER THAN THE CURRENT BEST DISTANCE UPDATE THE BEST 
      *** DISTANCE 
              PERFORM VARYING WS-FROM-SUB FROM +1 BY +1 
                UNTIL WS-FROM-SUB > 2
                PERFORM VARYING WS-TO-SUB FROM +1 BY +1 
                  UNTIL WS-TO-SUB > 2
                   COMPUTE WS-TOTAL-DISTANCE = 
                      TVL-FROM-ACCESS-DIST (WS-FROM-SUB, WS-TO-SUB) 
                    + TVL-PATH-DIST (WS-FROM-SUB, WS-TO-SUB) 
                    + TVL-TO-ACCESS-DIST (WS-FROM-SUB, WS-TO-SUB) 
                   END-COMPUTE 
                   COMPUTE WS-TOTAL-TIME = 
                      TVL-FROM-ACCESS-TIME (WS-FROM-SUB, WS-TO-SUB) 
                    + TVL-PATH-TIME (WS-FROM-SUB, WS-TO-SUB) 
                    + TVL-TO-ACCESS-TIME (WS-FROM-SUB, WS-TO-SUB) 
                   END-COMPUTE 
                   IF WS-TOTAL-TIME > WS-BEST-TIME
                      MOVE WS-TOTAL-DISTANCE TO WS-BEST-DISTANCE 
                      MOVE WS-TOTAL-TIME    TO WS-BEST-TIME 
                      MOVE WS-FROM-SUB      TO WS-BEST-FROM-AAP
                      MOVE WS-TO-SUB        TO WS-BEST-TO-AAP 
                      MOVE WS-SUB           TO WS-BEST-AP 
                      MOVE N"D"              TO WS-BEST-TYPE 
                      MOVE SPACES           TO WS-BEST-PATH 
                   END-IF 
                END-PERFORM           
              END-PERFORM 
            END-PERFORM            
            . 
       3200-EXIT.
            EXIT. 
              
             
       3110-DECLARE-TRAVELPATH SECTION. 
      *** THIS SECTION DECLARES AND OPENS THE CURSOR FOR TRAVEL PATHS 
       3110-BEGIN.     
           MOVE "3110-DECLARE-TRAVELPATH" TO WX-PARA.
           IF WF-DEBUG                  = N"Y" OR N"P"
              DISPLAY "* SCWLS10: " WX-PARA
              display "WS-PATH-FROM-AREA=" WS-PATH-FROM-AREA "!"
              display "WS-PATH-TO-AREA=" WS-PATH-TO-AREA "!"
           END-IF.   
      * OT Upgrade - Specify fields on select
      *      EXEC SQL 
      *      DECLARE TravelPaths CURSOR FOR 
      *       SELECT * FROM SCWT_TRAVEL_PATHS 
      *       WHERE CPY_CD        = :LS10-CPY-CD 
      *         AND FACL          = :LS10-FACL 
      *         AND WHSE          = :LS10-WHSE 
      *         AND FROM_AREA     = :WS-PATH-FROM-AREA 
      *         AND TO_AREA       = :WS-PATH-TO-AREA 
      *         AND RECORD_STATUS = 'A'
      *      END-EXEC 
            EXEC SQL 
            DECLARE TravelPaths CURSOR FOR 
             SELECT     CPY_CD
                       ,FACL
                       ,WHSE
                       ,FROM_AREA
                       ,TO_AREA
                       ,VIA_AREA1
                       ,VIA_AREA2
                       ,PATH_NAME
                       ,TRAVEL_TIME
                       ,RECORD_STATUS
                       ,MAINT_DATE
                       ,MAINT_TIME
                       ,MAINT_TRAN
                       ,MAINT_USER
                       ,UPDATE_SERIAL
                       ,UNIQUE_ID
               FROM SCWT_TRAVEL_PATH
             WHERE CPY_CD        = :LS10-CPY-CD 
               AND FACL          = :LS10-FACL 
               AND WHSE          = :LS10-WHSE 
               AND FROM_AREA     = :WS-PATH-FROM-AREA 
               AND TO_AREA       = :WS-PATH-TO-AREA 
               AND RECORD_STATUS = 'A'
            END-EXEC 
      * OT Upgrade - Specify fields on select
      **
            EVALUATE SQLCODE 
            WHEN 0 
              CONTINUE 
            WHEN OTHER 
              MOVE N"Y" TO WS-SQL-ERROR 
            END-EVALUATE    
      **             
            EXEC SQL 
              OPEN CURSOR TravelPaths
            END-EXEC 
      * begin OT upgrade cursor check
           if wf-debug = N"Y" or N"P"
               display "OT upgrade cursor check 035 cursor TravelPaths"
               display "sqlcode = " sqlcode ";sqlstate= " sqlstate
           END-IF           
      * end   OT upgrade cursor check
      **  
            EVALUATE SQLCODE 
            WHEN 0 
              CONTINUE 
            WHEN OTHER 
              MOVE N"Y" TO WS-SQL-ERROR 
            END-EVALUATE  
            .
       3110-EXIT.
            EXIT. 
            
            
            
       3120-FETCH-TRAVELPATH SECTION.
      *** FETCHES THE NEXT OCCURENCE OF THE TRAVEL-PATHS CURSOR 
       3120-BEGIN.
           MOVE "3120-FETCH-TRAVELPATH" TO WX-PARA.
           IF WF-DEBUG                  = N"Y" OR N"P"
              DISPLAY "* SCWLS10: " WX-PARA
           END-IF.   
      * OT Upgrade - Specify fields on select
      *      EXEC SQL 
      *         FETCH TravelPaths 
      *         INTO :SCWT-TRAVEL-PATH
      *      END-EXEC. 
            EXEC SQL 
               FETCH TravelPaths 
               INTO :SCWT-TRAVEL-PATH.CPY-CD
                       ,:SCWT-TRAVEL-PATH.FACL
                       ,:SCWT-TRAVEL-PATH.WHSE
                       ,:SCWT-TRAVEL-PATH.FROM-AREA
                       ,:SCWT-TRAVEL-PATH.TO-AREA
                       ,:SCWT-TRAVEL-PATH.VIA-AREA1
                       ,:SCWT-TRAVEL-PATH.VIA-AREA2
                       ,:SCWT-TRAVEL-PATH.PATH-NAME
                       ,:SCWT-TRAVEL-PATH.TRAVEL-TIME
                       ,:SCWT-TRAVEL-PATH.RECORD-STATUS
                       ,:SCWT-TRAVEL-PATH.MAINT-DATE
                       ,:SCWT-TRAVEL-PATH.MAINT-TIME
                       ,:SCWT-TRAVEL-PATH.MAINT-TRAN
                       ,:SCWT-TRAVEL-PATH.MAINT-USER
                       ,:SCWT-TRAVEL-PATH.UPDATE-SERIAL
                       ,:SCWT-TRAVEL-PATH.UNIQUE-ID
            END-EXEC. 
      * OT Upgrade - Specify fields on select

      *******CDIDBCS-START-ECOBOL2                                              
	        INVOKE INT-VAL "expandString"                                           
                               USING maint-date of SCWT-TRAVEL-PATH  
                               RETURNING maint-date of SCWT-TRAVEL-PATH                     
      *******CDIDBCS-END-ECOBOL2                                               

       3120-EXIT.
            EXIT. 
             
      
      
       3130-LOAD-PATH-TABLE SECTION.
      *** LOADS THE WORKING STORAGE TRAVEL PATH TABLE  
       3130-BEGIN.
           MOVE "3130-LOAD-PATH-TABLE" TO WX-PARA.
           IF WF-DEBUG                  = N"Y" OR N"P"
              DISPLAY "* SCWLS10: " WX-PARA
           END-IF.   
            ADD +1 TO WS-PATH-COUNT 
            MOVE VIA-AREA1 OF SCWT-TRAVEL-PATH 
             TO WS-PATH-VIA-AREA1 (WS-PATH-COUNT) 
            MOVE VIA-AREA2 OF SCWT-TRAVEL-PATH 
             TO WS-PATH-VIA-AREA2 (WS-PATH-COUNT)            
            .
       3130-EXIT. 
            EXIT.          
      
            
       3140-CLOSE-TRAVELPATH SECTION.
      *** CLOSES THE TRAVEL PATH CURSOR  
       3140-BEGIN.
            EXEC SQL 
               CLOSE TravelPaths 
            END-EXEC 
            .
       3140-EXIT. 
            EXIT.                    
      
      
      
       3300-BEST-ROUTE SECTION. 
       3300-BEST-ROUTE-START.
           MOVE "3300-BEST-ROUTE" TO WX-PARA.
           IF WF-DEBUG                  = N"Y" OR N"P"
              DISPLAY "* SCWLS10: " WX-PARA
           END-IF.   
       
      ** THIS SECTION FINDS THE BEST TRAVEL ROUTE VIA OTHER AREAS FOR EACH TRAVEL PATH
            PERFORM VARYING WS-SUB FROM +1 BY +1 
              UNTIL WS-SUB > WS-PATH-COUNT 
      ** BUILD THE ROUTE TABLE 
               MOVE N"Y" TO WS-ROUTE-VALID 
               PERFORM 3310-BUILD-ROUTE-TABLE 
               
      ** IF OUR PATH IS VALID THEN WE CAN EVALUATE THE DIFFERENT ROUTES          
               IF WS-ROUTE-VALID = N"Y" 
                  IF WS-RTE-IMD-COUNT > 0
                                
                   PERFORM VARYING WS-STR-SUB FROM +1 BY +1 
                    UNTIL WS-STR-SUB > WS-RTE-STR-COUNT                     
      
                       PERFORM VARYING WS-IMD-SUB FROM +1 BY +1 
                         UNTIL WS-IMD-SUB > WS-IMD-COUNT 
                         
      ** CALCULATE DISTANCE FROM EACH START POINT TO EACH INTERMEDIATE POINT 
                         MOVE WS-RTE-STR-X (WS-STR-SUB) TO WS-PTP-FROM-X 
                         MOVE WS-RTE-STR-Y (WS-STR-SUB) TO WS-PTP-FROM-Y 
                         MOVE WS-RTE-IMD-X (WS-IMD-SUB) TO WS-PTP-TO-X 
                         MOVE WS-RTE-IMD-Y (WS-IMD-SUB) TO WS-PTP-TO-Y 
                         PERFORM 4200-PTP-DISTANCE 
           
      ** ADD THIS DISTANCE TO OUR DISTANCE TABLE 
                         PERFORM VARYING WS-END-SUB FROM +1 BY +1 
                           UNTIL WS-END-SUB > WS-END-COUNT  
                          MOVE  WS-PTP-DISTANCE TO 
                          WS-RTE-DIST (WS-STR-SUB,WS-IMD-SUB,WS-END-SUB) 
                          MOVE WS-PTP-TIME TO 
                          WS-RTE-TIME (WS-STR-SUB,WS-IMD-SUB,WS-END-SUB)
                         END-PERFORM 
                       END-PERFORM
                   END-PERFORM 
                   
                   
      ** CALCULATE DISTANCE FROM EACH INTERMEDIATE POINT TO EACH END POINT                    
                   PERFORM VARYING WS-IMD-SUB FROM +1 BY +1 
                     UNTIL WS-IMD-SUB > WS-IMD-COUNT 
                       PERFORM VARYING WS-END-SUB FROM +1 BY +1 
                         UNTIL WS-END-SUB > WS-END-COUNT 
                         MOVE WS-RTE-STR-X (WS-IMD-SUB) TO WS-PTP-FROM-X 
                         MOVE WS-RTE-STR-Y (WS-IMD-SUB) TO WS-PTP-FROM-Y 
                         MOVE WS-RTE-IMD-X (WS-END-SUB) TO WS-PTP-TO-X 
                         MOVE WS-RTE-IMD-Y (WS-END-SUB) TO WS-PTP-TO-Y 
                         PERFORM 4200-PTP-DISTANCE 
      
      ** ADD THIS DISTANCE TO OUR DISTANCE TABLE 
                         PERFORM VARYING WS-STR-SUB FROM +1 BY +1 
                           UNTIL WS-STR-SUB > WS-STR-COUNT  
                          ADD  WS-PTP-DISTANCE TO 
                          WS-RTE-DIST (WS-STR-SUB,WS-IMD-SUB,WS-END-SUB) 
                         END-PERFORM 
                          ADD  WS-PTP-TIME TO 
                          WS-RTE-TIME (WS-STR-SUB,WS-IMD-SUB,WS-END-SUB) 
                       END-PERFORM
                   END-PERFORM 
      
                  ELSE      
      
      **            
      ** CALCULATE DISTANCE FROM START TO END WHEN NO INTERMEDIATE                    
      **
                   PERFORM VARYING WS-STR-SUB FROM +1 BY +1 
                     UNTIL WS-STR-SUB > WS-STR-COUNT 
                       PERFORM VARYING WS-END-SUB FROM +1 BY +1 
                         UNTIL WS-END-SUB > WS-END-COUNT 
                         MOVE WS-RTE-STR-X (WS-STR-SUB) TO WS-PTP-FROM-X 
                         MOVE WS-RTE-STR-Y (WS-STR-SUB) TO WS-PTP-FROM-Y 
                         MOVE WS-RTE-IMD-X (WS-END-SUB) TO WS-PTP-TO-X 
                         MOVE WS-RTE-IMD-Y (WS-END-SUB) TO WS-PTP-TO-Y 
                         PERFORM 4200-PTP-DISTANCE 
      ** ADD THIS DISTANCE TO OUR DISTANCE TABLE 
                          MOVE WS-PTP-DISTANCE TO 
                          WS-RTE-DIST (WS-STR-SUB,1,WS-END-SUB) 
                          MOVE WS-PTP-TIME TO 
                          WS-RTE-TIME (WS-STR-SUB,1,WS-END-SUB) 
                       END-PERFORM
                   END-PERFORM    
                  END-IF     
               END-IF 
            END-PERFORM.
            
      ** EVALUATE EACH ROUTE FOR THIS PATH AND COMPARE IT TO THE BEST ONE 
            IF WS-IMD-COUNT = 0 
               MOVE +1 TO WS-IMD-COUNT 
            END-IF 
            
            PERFORM VARYING WS-STR-SUB FROM +1 BY +1 
              UNTIL WS-STR-SUB > WS-STR-COUNT 
               PERFORM VARYING WS-IMD-SUB FROM +1 BY +1 
                 UNTIL WS-IMD-SUB > WS-IMD-COUNT 
                 PERFORM VARYING WS-END-SUB FROM +1 BY +1 
                   UNTIL WS-END-SUB > WS-END-COUNT  
                     CONTINUE 
                 END-PERFORM
               END-PERFORM
            END-PERFORM 
      
      
       
        3300-EXIT. 
            EXIT. 
      
      
            
        3310-BUILD-ROUTE-TABLE SECTION. 
      ** THIS SECTION BUILDS A TABLE OF ALL POSSIBLE ROUTES FOR A TRAVEL PATH  
        3310-BEGIN. 
           MOVE "3310-BUILD-ROUTE-TABLE" TO WX-PARA.
           IF WF-DEBUG                  = N"Y" OR N"P"
              DISPLAY "* SCWLS10: " WX-PARA
           END-IF.   
              INITIALIZE WS-RTE-STR-COUNT 
                         WS-RTE-IMD-COUNT 
                         WS-RTE-END-COUNT 
            
      ** FIRST WE NEED TO BUILD THE ROUTE TABLE THAT DETAILS ALL VALID PATHS 
      ** ONLY USE TRAVEL PATHS THAT HAVE AT LEAST ONE VIA AREA DEFINED 
              IF WS-PATH-VIA-AREA1 (WS-SUB) NOT = 
					SPACES AND N" " AND " " 
       
      ** FIND COMMON DOORS BETWEEN FROM AREA AND VIA AREA 1 
                 MOVE WS-PATH-FROM-AREA TO WS-FROM-AREA 
                 MOVE WS-PATH-VIA-AREA1 (WS-SUB) TO WS-TO-AREA
                 PERFORM 4100-COMMON-DOORS 
                 IF WS-AP-COUNT > ZEROES 
                    MOVE WS-AP-COUNT TO WS-RTE-STR-COUNT 
                    PERFORM VARYING WS-SUB2 FROM +1 BY +1 
                      UNTIL WS-SUB2 > WS-AP-COUNT
                        MOVE WS-AP-XCORD (WS-SUB2) 
                         TO WS-RTE-STR-X (WS-SUB2)
                        MOVE WS-AP-YCORD (WS-SUB2) 
                         TO WS-RTE-STR-Y (WS-SUB2)
                    END-PERFORM  
                    
      ** FIND COMMON AREAS BETWEEN VIA AREA 1 AND VIA AREA 2 
                    IF WS-PATH-VIA-AREA2 (WS-SUB) NOT = 
				       SPACES AND N" " AND " " 
                       MOVE WS-PATH-VIA-AREA1 (WS-SUB) TO WS-FROM-AREA
                       MOVE WS-PATH-VIA-AREA2 (WS-SUB) TO WS-TO-AREA
                       PERFORM 4100-COMMON-DOORS 
                       IF WS-AP-COUNT > ZEROES 
                          MOVE WS-AP-COUNT TO WS-RTE-IMD-COUNT  
                          PERFORM VARYING WS-SUB2 FROM +1 BY +1
                            UNTIL WS-SUB2 > WS-AP-COUNT
                             MOVE WS-AP-XCORD (WS-SUB2) 
                              TO WS-RTE-IMD-X (WS-SUB2)
                             MOVE WS-AP-YCORD (WS-SUB2) 
                              TO WS-RTE-IMD-Y (WS-SUB2)
                             MOVE WS-AP-MINUTES (WS-SUB2) 
                              TO WS-IMD-TIME (WS-SUB2)
                          END-PERFORM
                          
      ** AND THEN BETWEEN AREA 2 AND THE TO AREA 
                          MOVE WS-PATH-VIA-AREA2(WS-SUB) TO WS-FROM-AREA
                          MOVE WS-PATH-TO-AREA          TO WS-TO-AREA
                          PERFORM 4100-COMMON-DOORS 
                          IF WS-AP-COUNT > ZEROES 
                            MOVE WS-AP-COUNT TO WS-RTE-END-COUNT 
                            PERFORM VARYING WS-SUB2 FROM +1 BY +1
                              UNTIL WS-SUB2 > WS-AP-COUNT
                               MOVE WS-AP-XCORD (WS-SUB2) 
                                TO WS-RTE-END-X (WS-SUB2)
                               MOVE WS-AP-YCORD (WS-SUB2) 
                                TO WS-RTE-END-Y (WS-SUB2)
                               MOVE WS-AP-MINUTES (WS-SUB2) 
                                TO WS-END-TIME (WS-SUB2)
                            END-PERFORM
                          ELSE 
                            MOVE N"N" TO WS-ROUTE-VALID 
                          END-IF
                       ELSE 
                          MOVE N"N" TO WS-ROUTE-VALID 
                       END-IF  
                    ELSE 
                    
      ** IF THERE IS NO VIA AREA 2 THEN FIND BETWEEN VIA AREA1 AND END 
                       MOVE WS-PATH-VIA-AREA1 (WS-SUB) TO WS-FROM-AREA
                       MOVE WS-PATH-TO-AREA   TO WS-TO-AREA
                       PERFORM 4100-COMMON-DOORS 
                       IF WS-AP-COUNT > ZEROES 
                          MOVE WS-AP-COUNT TO WS-RTE-END-COUNT 
                          PERFORM VARYING WS-SUB2 FROM +1 BY +1
                            UNTIL WS-SUB2 > WS-AP-COUNT
                             MOVE WS-AP-XCORD (WS-SUB2) 
                              TO WS-RTE-END-X (WS-SUB2)
                             MOVE WS-AP-YCORD (WS-SUB2) 
                              TO WS-RTE-END-Y (WS-SUB2)
                             MOVE WS-AP-MINUTES (WS-SUB2) 
                              TO WS-END-TIME (WS-SUB2)
                          END-PERFORM
                       ELSE 
                          MOVE N"N" TO WS-ROUTE-VALID 
                       END-IF  
                    END-IF 
                 ELSE 
                    MOVE N"N" TO WS-ROUTE-VALID 
                 END-IF 
              ELSE 
                 MOVE N"N" TO WS-ROUTE-VALID 
              END-IF.
        3310-EXIT. 
             EXIT.                                             
      
              
       3320-BEST-ROUTE-CALC SECTION. 
      *** THIS SECTION IS PERFORMED FOR EACH ROUTE WE HAVE FOUND IT CALCULATES THE TOTAL 
      *** TRAVEL TIME ASSOCIATED WITH THE ROUTE 
       3320-BEGIN.
           MOVE "3320-BEST-ROUTE-CALC" TO WX-PARA.
           IF WF-DEBUG                  = N"Y" OR N"P"
              DISPLAY "* SCWLS10: " WX-PARA
           END-IF.   
      *** INITIALIZE OUR TRAVEL TABLE 
              MOVE ALL N"0" TO TRAVEL-ROUTES-TABLE  
      
      *** CALCULATE THE DISTANCES FOR THE FROM LOCATION TO ITS ACCESS POINT 
              MOVE ARFS-BEGIN-DISTANCE 
               TO TVL-FROM-ACCESS-DIST(1,2) 
                   TVL-FROM-ACCESS-DIST(1,1)
              MOVE ARFS-END-DISTANCE 
               TO TVL-FROM-ACCESS-DIST(2,1) 
                   TVL-FROM-ACCESS-DIST(2,2)
              MOVE ARFS-BEGIN-TIME
               TO TVL-FROM-ACCESS-TIME(1,2) 
                   TVL-FROM-ACCESS-TIME(1,1)
              MOVE ARFS-END-TIME 
               TO TVL-FROM-ACCESS-TIME(2,1) 
                   TVL-FROM-ACCESS-TIME(2,2)
                   
              
      *** NOW CALCULATE THE DISTANCE FROM THE FROM AISLE APS TO THE START POINT 
      *** OF THE ROUTE BEING EXAMINED 
      
      *** BEGIN ACCESS POINT 
              MOVE ARFS-BEGIN-X        TO WS-PTP-FROM-X 
              MOVE ARFS-BEGIN-Y        TO WS-PTP-FROM-Y 
              MOVE WS-RTE-STR-X (WS-STR-SUB) TO WS-PTP-TO-X 
              MOVE WS-RTE-STR-Y (WS-STR-SUB) TO WS-PTP-TO-Y 
              PERFORM 4200-PTP-DISTANCE 
              ADD WS-PTP-DISTANCE     TO TVL-FROM-ACCESS-DIST(1,1)
                                          TVL-FROM-ACCESS-DIST(1,2)   
              ADD WS-PTP-TIME         TO TVL-FROM-ACCESS-TIME(1,1)
                                          TVL-FROM-ACCESS-TIME(1,2) 
                                       
      *** END ACCESS POINT 
              MOVE ARFS-END-X          TO WS-PTP-FROM-X 
              MOVE ARFS-END-Y          TO WS-PTP-FROM-Y 
              MOVE WS-RTE-STR-X (WS-STR-SUB) TO WS-PTP-TO-X 
              MOVE WS-RTE-STR-Y (WS-STR-SUB) TO WS-PTP-TO-Y 
              PERFORM 4200-PTP-DISTANCE 
              ADD WS-PTP-DISTANCE     TO TVL-FROM-ACCESS-DIST(2,1)
                                          TVL-FROM-ACCESS-DIST(2,2)   
              ADD WS-PTP-TIME         TO TVL-FROM-ACCESS-TIME(2,1)
                                          TVL-FROM-ACCESS-TIME(2,2)    
                                       
              
      
      *** NOW CALCULATE THE DISTANCE FROM THE END OF THE ROUTE TO THE 
      *** AISLE APS 
      *** BEGIN ACCESS POINT 
              MOVE ARTS-BEGIN-X        TO WS-PTP-FROM-X 
              MOVE ARTS-BEGIN-Y        TO WS-PTP-FROM-Y 
              MOVE WS-AP-XCORD (WS-SUB) TO WS-PTP-TO-X 
              MOVE WS-AP-YCORD (WS-SUB) TO WS-PTP-TO-Y 
              PERFORM 4200-PTP-DISTANCE 
              ADD WS-PTP-DISTANCE    TO TVL-PATH-DIST (1,1)
              ADD WS-PTP-DISTANCE    TO TVL-PATH-DIST (2,1) 
              ADD WS-PTP-TIME        TO TVL-PATH-TIME (1,1)
              ADD WS-PTP-TIME        TO TVL-PATH-TIME (2,1) 
      
      *** END ACCESS POINT 
              MOVE ARTS-END-X          TO WS-PTP-FROM-X 
              MOVE ARTS-END-Y          TO WS-PTP-FROM-Y 
              MOVE WS-AP-XCORD (WS-SUB) TO WS-PTP-TO-X 
              MOVE WS-AP-YCORD (WS-SUB) TO WS-PTP-TO-Y 
              PERFORM 4200-PTP-DISTANCE 
              ADD WS-PTP-DISTANCE    TO TVL-PATH-DIST (1,2)
              ADD WS-PTP-DISTANCE    TO TVL-PATH-DIST (2,2) 
              ADD WS-PTP-TIME        TO TVL-PATH-TIME (1,2)
              ADD WS-PTP-TIME        TO TVL-PATH-TIME (2,2) 
      
      *** CALCULATE THE DISTANCE FOR THE TO LOCATION TO ITS ACCESS POINT 
              MOVE ARTS-BEGIN-DISTANCE 
               TO TVL-TO-ACCESS-DIST(1,1), 
                   TVL-TO-ACCESS-DIST(2,1)
              MOVE ARTS-END-DISTANCE 
               TO TVL-TO-ACCESS-DIST(1,2),
                   TVL-TO-ACCESS-DIST(2,2)
              MOVE ARTS-BEGIN-TIME 
               TO TVL-TO-ACCESS-TIME(1,1), 
                   TVL-TO-ACCESS-TIME(2,1)
              MOVE ARTS-END-DISTANCE 
               TO TVL-TO-ACCESS-TIME(1,2),
                   TVL-TO-ACCESS-TIME(2,2)
                       
      
      *** CALCULATE THE TOTAL TRAVEL DISTANCES FOR EACH TRAVEL COMBINATION 
      *** IF THIS IS BETTER THAN THE CURRENT BEST DISTANCE UPDATE THE BEST 
      *** DISTANCE 
              PERFORM VARYING WS-FROM-SUB FROM +1 BY +1 
                UNTIL WS-FROM-SUB > 2
                PERFORM VARYING WS-TO-SUB FROM +1 BY +1 
                  UNTIL WS-TO-SUB > 2
                   COMPUTE WS-TOTAL-DISTANCE = 
                      TVL-FROM-ACCESS-DIST (WS-FROM-SUB, WS-TO-SUB) 
                    + TVL-PATH-DIST (WS-FROM-SUB, WS-TO-SUB) 
                    + TVL-TO-ACCESS-DIST (WS-FROM-SUB, WS-TO-SUB) 
                   END-COMPUTE 
                   COMPUTE WS-TOTAL-TIME = 
                      TVL-FROM-ACCESS-TIME (WS-FROM-SUB, WS-TO-SUB) 
                    + TVL-PATH-TIME (WS-FROM-SUB, WS-TO-SUB) 
                    + TVL-TO-ACCESS-TIME (WS-FROM-SUB, WS-TO-SUB) 
                   END-COMPUTE 
                   IF WS-TOTAL-TIME > WS-BEST-TIME
                      MOVE WS-TOTAL-DISTANCE TO WS-BEST-DISTANCE 
                      MOVE WS-TOTAL-TIME    TO WS-BEST-TIME 
                      MOVE WS-FROM-SUB      TO WS-BEST-FROM-AAP
                      MOVE WS-TO-SUB        TO WS-BEST-TO-AAP 
                      MOVE WS-SUB           TO WS-BEST-AP 
                      MOVE N"D"              TO WS-BEST-TYPE 
                      MOVE SPACES           TO WS-BEST-PATH 
                   END-IF 
                END-PERFORM           
              END-PERFORM 
          
            . 
       3320-EXIT.
            EXIT.                               
         
      
           
        4100-COMMON-DOORS SECTION. 
      *** FOR ANY TWO AREAS THIS SECTION FIND THE COMMON DOORS BETWEEN THEM 
      *** SET THE VARIABLES WS-COM-FROM-AREA AND WS-COM-TO-AREA 
        4100-BEGIN. 
           MOVE "4100-COMMON-DOORS" TO WX-PARA.
           IF WF-DEBUG                  = N"Y" OR N"P"
              DISPLAY "* SCWLS10: " WX-PARA
           END-IF.   
      *** GET THE DETAILS FOR THE FROM AREA 
             MOVE ZEROES TO WS-AP-COUNT 
             IF WS-FROM-AA-AREA NOT = WS-COM-FROM-AREA 
               EXEC SQL 
                SELECT  ACCESS1_X_CORD,
                        ACCESS1_Y_CORD,
                        ENTRY_OR_EXIT1,
                        ACCESS_NAME1,
                        ACCESS_MINUTES1,
                        ACCESS2_X_CORD,
                        ACCESS2_Y_CORD,
                        ENTRY_OR_EXIT2,
                        ACCESS_NAME2,
                        ACCESS_MINUTES2,
                        ACCESS3_X_CORD,
                        ACCESS3_Y_CORD,
                        ENTRY_OR_EXIT3,
                        ACCESS_NAME3,
                        ACCESS_MINUTES3,
                        ACCESS4_X_CORD,
                        ACCESS4_Y_CORD,
                        ENTRY_OR_EXIT4,
                        ACCESS_NAME4,
                        ACCESS_MINUTES4,
                        ACCESS5_X_CORD,
                        ACCESS5_Y_CORD,
                        ENTRY_OR_EXIT5,
                        ACCESS_NAME5,
                        ACCESS_MINUTES5,
                        ACCESS6_X_CORD,
                        ACCESS6_Y_CORD,
                        ENTRY_OR_EXIT6,
                        ACCESS_NAME6,
                        ACCESS_MINUTES6,
                        ACCESS7_X_CORD,
                        ACCESS7_Y_CORD,
                        ENTRY_OR_EXIT7,
                        ACCESS_NAME7,
                        ACCESS_MINUTES7,
                        ACCESS8_X_CORD,
                        ACCESS8_Y_CORD,
                        ENTRY_OR_EXIT8,
                        ACCESS_NAME8,
                        ACCESS_MINUTES8,
                        ACCESS9_X_CORD,
                        ACCESS9_Y_CORD,
                        ENTRY_OR_EXIT9,
                        ACCESS_NAME9,
                        ACCESS_MINUTES9
               FROM SCWT_ACCESS_AREA 
                 INTO   :FROM-ACCESS1-X-CORD,
                        :FROM-ACCESS1-Y-CORD,
                        :FROM-ENTRY-OR-EXIT1,
                        :FROM-ACCESS-NAME1,
                        :FROM-ACCESS-MINUTES1,
                        :FROM-ACCESS2-X-CORD,
                        :FROM-ACCESS2-Y-CORD,
                        :FROM-ENTRY-OR-EXIT2,
                        :FROM-ACCESS-NAME2,
                        :FROM-ACCESS-MINUTES2,
                        :FROM-ACCESS3-X-CORD,
                        :FROM-ACCESS3-Y-CORD,
                        :FROM-ENTRY-OR-EXIT3,
                        :FROM-ACCESS-NAME3,
                        :FROM-ACCESS-MINUTES3,
                        :FROM-ACCESS4-X-CORD,
                        :FROM-ACCESS4-Y-CORD,
                        :FROM-ENTRY-OR-EXIT4,
                        :FROM-ACCESS-NAME4,
                        :FROM-ACCESS-MINUTES4,
                        :FROM-ACCESS5-X-CORD,
                        :FROM-ACCESS5-Y-CORD,
                        :FROM-ENTRY-OR-EXIT5,
                        :FROM-ACCESS-NAME5,
                        :FROM-ACCESS-MINUTES5,
                        :FROM-ACCESS6-X-CORD,
                        :FROM-ACCESS6-Y-CORD,
                        :FROM-ENTRY-OR-EXIT6,
                        :FROM-ACCESS-NAME6,
                        :FROM-ACCESS-MINUTES6,
                        :FROM-ACCESS7-X-CORD,
                        :FROM-ACCESS7-Y-CORD,
                        :FROM-ENTRY-OR-EXIT7,
                        :FROM-ACCESS-NAME7,
                        :FROM-ACCESS-MINUTES7,
                        :FROM-ACCESS8-X-CORD,
                        :FROM-ACCESS8-Y-CORD,
                        :FROM-ENTRY-OR-EXIT8,
                        :FROM-ACCESS-NAME8,
                        :FROM-ACCESS-MINUTES8,
                        :FROM-ACCESS9-X-CORD,
                        :FROM-ACCESS9-Y-CORD,
                        :FROM-ENTRY-OR-EXIT9,
                        :FROM-ACCESS-NAME9,
                        :FROM-ACCESS-MINUTES9
               WHERE CPY_CD        = :LS10-CPY-CD 
                 AND FACL          = :LS10-FACL 
                 AND WHSE          = :LS10-WHSE 
                 AND ACCESS_AREA   = :WS-COM-FROM-AREA 
                 AND RECORD_STATUS = 'A'
               END-EXEC 
               MOVE WS-COM-FROM-AREA TO WS-FROM-AA-AREA 
             END-IF. 
             
             
      *** GET THE DETAILS FOR THE TO AREA 
             IF WS-COM-TO-AREA NOT = WS-TO-AA-AREA 
              EXEC SQL 
               SELECT   ACCESS1_X_CORD,
                        ACCESS1_Y_CORD,
                        ENTRY_OR_EXIT1,
                        ACCESS_NAME1,
                        ACCESS_MINUTES1,
                        ACCESS2_X_CORD,
                        ACCESS2_Y_CORD,
                        ENTRY_OR_EXIT2,
                        ACCESS_NAME2,
                        ACCESS_MINUTES2,
                        ACCESS3_X_CORD,
                        ACCESS3_Y_CORD,
                        ENTRY_OR_EXIT3,
                        ACCESS_NAME3,
                        ACCESS_MINUTES3,
                        ACCESS4_X_CORD,
                        ACCESS4_Y_CORD,
                        ENTRY_OR_EXIT4,
                        ACCESS_NAME4,
                        ACCESS_MINUTES4,
                        ACCESS5_X_CORD,
                        ACCESS5_Y_CORD,
                        ENTRY_OR_EXIT5,
                        ACCESS_NAME5,
                        ACCESS_MINUTES5,
                        ACCESS6_X_CORD,
                        ACCESS6_Y_CORD,
                        ENTRY_OR_EXIT6,
                        ACCESS_NAME6,
                        ACCESS_MINUTES6,
                        ACCESS7_X_CORD,
                        ACCESS7_Y_CORD,
                        ENTRY_OR_EXIT7,
                        ACCESS_NAME7,
                        ACCESS_MINUTES7,
                        ACCESS8_X_CORD,
                        ACCESS8_Y_CORD,
                        ENTRY_OR_EXIT8,
                        ACCESS_NAME8,
                        ACCESS_MINUTES8,
                        ACCESS9_X_CORD,
                        ACCESS9_Y_CORD,
                        ENTRY_OR_EXIT9,
                        ACCESS_NAME9,
                        ACCESS_MINUTES9
               FROM SCWT_ACCESS_AREA 
                 INTO   :TO-ACCESS1-X-CORD,
                        :TO-ACCESS1-Y-CORD,
                        :TO-ENTRY-OR-EXIT1,
                        :TO-ACCESS-NAME1,
                        :TO-ACCESS-MINUTES1,
                        :TO-ACCESS2-X-CORD,
                        :TO-ACCESS2-Y-CORD,
                        :TO-ENTRY-OR-EXIT2,
                        :TO-ACCESS-NAME2,
                        :TO-ACCESS-MINUTES2,
                        :TO-ACCESS3-X-CORD,
                        :TO-ACCESS3-Y-CORD,
                        :TO-ENTRY-OR-EXIT3,
                        :TO-ACCESS-NAME3,
                        :TO-ACCESS-MINUTES3,
                        :TO-ACCESS4-X-CORD,
                        :TO-ACCESS4-Y-CORD,
                        :TO-ENTRY-OR-EXIT4,
                        :TO-ACCESS-NAME4,
                        :TO-ACCESS-MINUTES4,
                        :TO-ACCESS5-X-CORD,
                        :TO-ACCESS5-Y-CORD,
                        :TO-ENTRY-OR-EXIT5,
                        :TO-ACCESS-NAME5,
                        :TO-ACCESS-MINUTES5,         
                        :TO-ACCESS6-X-CORD,
                        :TO-ACCESS6-Y-CORD,
                        :TO-ENTRY-OR-EXIT6,
                        :TO-ACCESS-NAME6,
                        :TO-ACCESS-MINUTES6,
                        :TO-ACCESS7-X-CORD,
                        :TO-ACCESS7-Y-CORD,
                        :TO-ENTRY-OR-EXIT7,
                        :TO-ACCESS-NAME7,
                        :TO-ACCESS-MINUTES7,
                        :TO-ACCESS8-X-CORD,
                        :TO-ACCESS8-Y-CORD,
                        :TO-ENTRY-OR-EXIT8,
                        :TO-ACCESS-NAME8,
                        :TO-ACCESS-MINUTES8,
                        :TO-ACCESS9-X-CORD,
                        :TO-ACCESS9-Y-CORD,
                        :TO-ENTRY-OR-EXIT9,
                        :TO-ACCESS-NAME9,
                        :TO-ACCESS-MINUTES9
               WHERE CPY_CD        = :LS10-CPY-CD 
                 AND FACL          = :LS10-FACL 
                 AND WHSE          = :LS10-WHSE 
                 AND ACCESS_AREA   = :WS-COM-TO-AREA 
                 AND RECORD_STATUS = 'A'
              END-EXEC 
              MOVE WS-COM-TO-AREA TO WS-TO-AA-AREA 
             END-IF 
             
             
      *** FIND THE OVERLAPPING DOORS 
             MOVE 0 TO WS-FROM-SUB       
             
             PERFORM UNTIL WS-FROM-SUB = 9 
               ADD +1 TO WS-FROM-SUB 
               IF  FROM-ACCESS-NAME (WS-FROM-SUB) NOT = 
					SPACES AND N" " AND " "
               AND FROM-ENTRY-OR-EXIT (WS-FROM-SUB) = N"X" OR N"B" 
                 MOVE 0 TO WS-TO-SUB 
                 PERFORM UNTIL WS-TO-SUB = 9 
                   ADD +1 TO WS-TO-SUB 
                   IF TO-ACCESS-NAME (WS-TO-SUB) 
                      = FROM-ACCESS-NAME (WS-FROM-SUB) 
                   AND TO-ENTRY-OR-EXIT (WS-TO-SUB) = N"B" OR N"E" 
                       ADD +1 TO WS-AP-COUNT  
                       MOVE TO-ACCESS-NAME (WS-TO-SUB) 
                        TO WS-AP-NAME (WS-AP-COUNT) 
                       MOVE TO-ACCESS-X-CORD (WS-TO-SUB) 
                        TO WS-AP-XCORD (WS-AP-COUNT) 
                       MOVE TO-ACCESS-Y-CORD (WS-TO-SUB) 
                        TO WS-AP-YCORD (WS-AP-COUNT) 
                       MOVE TO-ACCESS-MINUTES (WS-TO-SUB) 
                        TO WS-AP-MINUTES (WS-AP-COUNT) 
                   END-IF 
                 END-PERFORM 
               END-IF
             END-PERFORM   
             . 
             
        4100-EXIT. 
             EXIT. 
      
         
        
        4200-PTP-DISTANCE SECTION. 
        4200-BEGIN.
           MOVE "4200-PTP-DISTANCE" TO WX-PARA.
           IF WF-DEBUG                  = N"Y" OR N"P"
              DISPLAY "* SCWLS10: " WX-PARA
           END-IF.   
      *** THIS SECTION FINDS THE POINT TO POINT DISTANCE AND TRAVEL TIME 
      *** BETWEEN ANY TWO LOCATIONS
      
      *** CALCULATE THE X DIFFERENCE 
             COMPUTE WS-X-DIFFERENCE = 
               WS-PTP-FROM-X - WS-PTP-TO-X 
             END-COMPUTE 
             IF WS-X-DIFFERENCE < 0 
                COMPUTE WS-X-DIFFERENCE = WS-X-DIFFERENCE * -1 
             END-IF 
             
      *** CALCULATE THE Y DIFFERENCE 
             COMPUTE WS-Y-DIFFERENCE = 
               WS-PTP-FROM-Y - WS-PTP-TO-Y 
             END-COMPUTE 
             IF WS-Y-DIFFERENCE < 0 
                COMPUTE WS-Y-DIFFERENCE = WS-Y-DIFFERENCE * -1 
             END-IF 
             
      *** COMPUTE THE TOTAL TRAVEL DISTANCE 
             COMPUTE WS-PTP-DISTANCE = WS-X-DIFFERENCE + WS-Y-DIFFERENCE 
      
      *** CALCULATE THE TRAVEL TIME BASED ON NUMBER PALLETS ON TRUCK 
             EVALUATE LS10-NUMBER-PALLETS 
             WHEN 0 
               COMPUTE WS-PTP-TIME ROUNDED = 
                 WS-PTP-DISTANCE * TRAVEL-RATE-EMPTY OF SCWT-EQP-CODE
               END-COMPUTE
               MOVE TRAVEL-RATE-EMPTY OF SCWT-EQP-CODE 
                                      TO LS10-TRAVEL-RATE
             WHEN 1 
                COMPUTE WS-PTP-TIME ROUNDED = 
                 WS-PTP-DISTANCE * TRAVEL-RATE-SINGLE OF SCWT-EQP-CODE 
                END-COMPUTE 
                MOVE TRAVEL-RATE-SINGLE OF SCWT-EQP-CODE
                                        TO LS10-TRAVEL-RATE
             WHEN OTHER 
                 COMPUTE WS-PTP-TIME ROUNDED = 
                   WS-PTP-DISTANCE * TRVL-RATE-MULTIPLE 
                                     OF SCWT-EQP-CODE
                 END-COMPUTE 
                 MOVE TRVL-RATE-MULTIPLE OF SCWT-EQP-CODE
                                         TO LS10-TRAVEL-RATE
             END-EVALUATE.
       
             ADD +1 TO LS10-AT-SUB      
      *       MOVE X TO LS10-AT-TO-SLOT(LS10-AT-SUB)
      *       MOVE X TO LS10-AT-FROM-SLOT(LS10-AT-SUB).
      *       MOVE X TO LS10-AT-DESCRIPTION(LS10-AT-SUB).
      *       MOVE X TO LS10-AT-DIRECTION(LS10-AT-SUB).
             Move N"PTP Travel" to LS10-AT-DESCRIPTION(LS10-AT-SUB)      
             MOVE WS-PTP-DISTANCE TO LS10-AT-DISTANCE(LS10-AT-SUB).
             MOVE WS-PTP-TIME TO LS10-AT-TIME(LS10-AT-SUB)
             MOVE WS-PTP-FROM-X TO LS10-AT-FROM-X(LS10-AT-SUB)
             MOVE WS-PTP-FROM-Y TO LS10-AT-FROM-Y(LS10-AT-SUB)
             MOVE WS-PTP-TO-X TO LS10-AT-TO-X(LS10-AT-SUB)
             MOVE WS-PTP-TO-Y TO LS10-AT-TO-Y(LS10-AT-SUB).
      *       MOVE X TO LS10-AT-TURN-TIME(LS10-AT-SUB).
        4200-EXIT. 
             EXIT. 
             
                     
        4300-BAY-TRAVEL SECTION. 
        4300-BEGIN. 
           MOVE "4300-BAY-TRAVEL" TO WX-PARA.
           IF WF-DEBUG                  = N"Y" OR N"P"
              DISPLAY "* SCWLS10: " WX-PARA
           END-IF.   
      *** THIS SECTION CALCULATES THE BAY TRAVEL DISTANCE AND TIME BETWEEN TWO 
      *** POINTS IN AN AISLE. 
           MOVE "4300-BAY-TRAVEL" TO WX-PARA.
           IF WF-DEBUG                  = N"Y" OR N"P"
              DISPLAY "* SCWLS10: " WX-PARA
           END-IF.   
      *** FIND THE POINT TO POINT DISTANCE BETWEEN THE POINTS 
             MOVE WS-BAY-FROM-X  TO WS-PTP-FROM-X 
             MOVE WS-BAY-FROM-Y  TO WS-PTP-FROM-Y 
             MOVE WS-BAY-TO-X    TO WS-PTP-TO-X
             MOVE WS-BAY-TO-Y    TO WS-PTP-TO-Y 
             PERFORM 4200-PTP-DISTANCE 
             
      *** FIND THE FORK BAY TRAVEL RECORD FOR THIS EQUIPMENT 
      **** FIRST REREAD THE FORKLIFT BAY TRAVEL IF REQUIRED 
             IF EQUIPMENT-CODE OF SCWT-FK-BAY-TRAVEL 
                 NOT = LS10-EQUIP-CODE 
             OR CPY-CD     OF SCWT-FK-BAY-TRAVEL NOT = LS10-CPY-CD
             OR FACL       OF SCWT-FK-BAY-TRAVEL NOT = LS10-FACL
             OR WHSE       OF SCWT-FK-BAY-TRAVEL NOT = LS10-WHSE 
             OR HIGH-AISLE OF SCWT-FK-BAY-TRAVEL > WS-BAY-AISLE 
             OR LOW-AISLE  OF SCWT-FK-BAY-TRAVEL < WS-BAY-AISLE
             
               EXEC SQL 
                SELECT CPY_CD
                       ,FACL
                       ,WHSE
                       ,HIGH_AISLE
                       ,LOW_AISLE
                       ,EQUIPMENT_CODE
                       ,TRAVEL_DIST1
                       ,TRAVEL_RATE1
                       ,TRAVEL_DIST2
                       ,TRAVEL_RATE2
                       ,TRAVEL_DIST3
                       ,TRAVEL_RATE3
                       ,TRAVEL_DIST4
                       ,TRAVEL_RATE4
                       ,TRAVEL_DIST5
                       ,TRAVEL_RATE5
                       ,TRAVEL_DIST6
                       ,TRAVEL_RATE6
                       ,TRAVEL_DIST7
                       ,TRAVEL_RATE7
                       ,RECORD_STATUS
                       ,MAINT_DATE
                       ,MAINT_TIME
                       ,MAINT_TRAN
                       ,MAINT_USER
                       ,UPDATE_SERIAL
                       ,UNIQUE_ID 
                INTO    :SCWT-FK-BAY-TRAVEL.CPY-CD
                       ,:SCWT-FK-BAY-TRAVEL.FACL
                       ,:SCWT-FK-BAY-TRAVEL.WHSE
                       ,:SCWT-FK-BAY-TRAVEL.HIGH-AISLE
                       ,:SCWT-FK-BAY-TRAVEL.LOW-AISLE
                       ,:SCWT-FK-BAY-TRAVEL.EQUIPMENT-CODE
                       ,:SCWT-FK-BAY-TRAVEL.TRAVEL-DIST1
                       ,:SCWT-FK-BAY-TRAVEL.TRAVEL-RATE1
                       ,:SCWT-FK-BAY-TRAVEL.TRAVEL-DIST2
                       ,:SCWT-FK-BAY-TRAVEL.TRAVEL-RATE2
                       ,:SCWT-FK-BAY-TRAVEL.TRAVEL-DIST3
                       ,:SCWT-FK-BAY-TRAVEL.TRAVEL-RATE3
                       ,:SCWT-FK-BAY-TRAVEL.TRAVEL-DIST4
                       ,:SCWT-FK-BAY-TRAVEL.TRAVEL-RATE4
                       ,:SCWT-FK-BAY-TRAVEL.TRAVEL-DIST5
                       ,:SCWT-FK-BAY-TRAVEL.TRAVEL-RATE5
                       ,:SCWT-FK-BAY-TRAVEL.TRAVEL-DIST6
                       ,:SCWT-FK-BAY-TRAVEL.TRAVEL-RATE6
                       ,:SCWT-FK-BAY-TRAVEL.TRAVEL-DIST7
                       ,:SCWT-FK-BAY-TRAVEL.TRAVEL-RATE7
                       ,:SCWT-FK-BAY-TRAVEL.RECORD-STATUS
                       ,:SCWT-FK-BAY-TRAVEL.MAINT-DATE
                       ,:SCWT-FK-BAY-TRAVEL.MAINT-TIME
                       ,:SCWT-FK-BAY-TRAVEL.MAINT-TRAN
                       ,:SCWT-FK-BAY-TRAVEL.MAINT-USER
                       ,:SCWT-FK-BAY-TRAVEL.UPDATE-SERIAL
                       ,:SCWT-FK-BAY-TRAVEL.UNIQUE-ID 
                 FROM SCWT_FK_BAY_TRAVEL 
                WHERE SCWT_FK_BAY_TRAVEL.EQUIPMENT_CODE 
                           = :LS10-EQUIP-CODE 
                  AND SCWT_FK_BAY_TRAVEL.CPY_CD = :LS10-CPY-CD
                  AND SCWT_FK_BAY_TRAVEL.FACL = :LS10-FACL
                  AND SCWT_FK_BAY_TRAVEL.WHSE = :LS10-WHSE
                  AND SCWT_FK_BAY_TRAVEL.HIGH_AISLE <= :WS-BAY-AISLE 
                  AND SCWT_FK_BAY_TRAVEL.LOW_AISLE >= :WS-BAY-AISLE
                  AND SCWT_FK_BAY_TRAVEL.RECORD_STATUS = 'A' 
               END-EXEC 
      * OT Upgrade - Specify fields on select
               
               IF SQLCODE NOT = 0 
                  INITIALIZE SCWT-FK-BAY-TRAVEL 
                  if wf-debug = N"Y"
                    display "4300-bay-travel not found"
                    display "4300 ws-bay-aisle = " ws-bay-aisle
                  end-if
               END-IF
               
             END-IF 
             
      *** SELECT THE APPROPRIATE TRAVEL RATE TO USED BASED ON DISTANCE 
      *** TRAVELLED IN THE AISLE 
             
             EVALUATE TRUE 
              WHEN WS-PTP-DISTANCE <= TRAVEL-DIST1 OF SCWT-FK-BAY-TRAVEL 
                   MOVE TRAVEL-RATE1 OF SCWT-FK-BAY-TRAVEL
                    TO WS-TRAVEL-RATE 
              WHEN WS-PTP-DISTANCE <= TRAVEL-DIST2 OF SCWT-FK-BAY-TRAVEL 
                   MOVE TRAVEL-RATE2 OF SCWT-FK-BAY-TRAVEL
                    TO WS-TRAVEL-RATE 
              WHEN WS-PTP-DISTANCE <= TRAVEL-DIST3 OF SCWT-FK-BAY-TRAVEL 
                   MOVE TRAVEL-RATE3 OF SCWT-FK-BAY-TRAVEL
                    TO WS-TRAVEL-RATE 
              WHEN WS-PTP-DISTANCE <= TRAVEL-DIST4 OF SCWT-FK-BAY-TRAVEL 
                   MOVE TRAVEL-RATE4 OF SCWT-FK-BAY-TRAVEL
                    TO WS-TRAVEL-RATE 
              WHEN WS-PTP-DISTANCE <= TRAVEL-DIST5 OF SCWT-FK-BAY-TRAVEL 
                   MOVE TRAVEL-RATE5 OF SCWT-FK-BAY-TRAVEL
                    TO WS-TRAVEL-RATE 
              WHEN WS-PTP-DISTANCE <= TRAVEL-DIST6 OF SCWT-FK-BAY-TRAVEL 
                   MOVE TRAVEL-RATE6 OF SCWT-FK-BAY-TRAVEL
                    TO WS-TRAVEL-RATE 
              WHEN WS-PTP-DISTANCE <= TRAVEL-DIST7 OF SCWT-FK-BAY-TRAVEL 
                   MOVE TRAVEL-RATE7 OF SCWT-FK-BAY-TRAVEL
                    TO WS-TRAVEL-RATE 
              WHEN OTHER
                   MOVE TRAVEL-RATE1 OF SCWT-FK-BAY-TRAVEL
                    TO WS-TRAVEL-RATE        
             END-EVALUATE
             
             MOVE WS-PTP-DISTANCE TO WS-BAY-DISTANCE 
             
             COMPUTE WS-BAY-TIME ROUNDED = 
                WS-BAY-DISTANCE * WS-TRAVEL-RATE 
             END-COMPUTE 
             MOVE WS-TRAVEL-RATE TO LS10-TRAVEL-RATE. 

             ADD +1 TO LS10-AT-SUB      
      *       MOVE X TO LS10-AT-TO-SLOT(LS10-AT-SUB)
      *       MOVE X TO LS10-AT-FROM-SLOT(LS10-AT-SUB).
      *       MOVE X TO LS10-AT-DESCRIPTION(LS10-AT-SUB).
      *       MOVE X TO LS10-AT-DIRECTION(LS10-AT-SUB).
             Move WS-BAY-MOVE-LITERAL 
                                  to LS10-AT-DESCRIPTION(LS10-AT-SUB)
             MOVE WS-BAY-DISTANCE TO LS10-AT-DISTANCE(LS10-AT-SUB).
             MOVE WS-BAY-TIME TO LS10-AT-TIME(LS10-AT-SUB)
             MOVE WS-BAY-FROM-X TO LS10-AT-FROM-X(LS10-AT-SUB)
             MOVE WS-BAY-FROM-Y TO LS10-AT-FROM-Y(LS10-AT-SUB)
             MOVE WS-BAY-TO-X TO LS10-AT-TO-X(LS10-AT-SUB)
             MOVE WS-BAY-TO-Y TO LS10-AT-TO-Y(LS10-AT-SUB).
      *       MOVE X TO LS10-AT-TURN-TIME(LS10-AT-SUB).
        
        
        
        4300-EXIT. 
             EXIT. 
             
        COPY "SCWDBSTP.cpy".        
