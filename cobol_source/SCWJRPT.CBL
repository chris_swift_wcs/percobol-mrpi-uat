       IDENTIFICATION DIVISION.
       PROGRAM-ID. SCWJRPT.
      /
      ******************************************************************
      *                                                                *
      *  Copyright 2002 Aquitec International, Inc.                    *
      *  All rights reserved                                           *
      * --------------------------------------------------------------*
      *  Version 1  Release  0                                         *
      *                                                                *
      ******************************************************************
      
      *================================================================* 
      * PROGRAM/MODULE  NAME........SCWJRPT                            *
      *                                                                *  
      *              (REQ)  JAVA REPORT PROGRAM CONTROLLER             *
      *                                                                *
      * DESIGNED BY.........Aquitec Inc.                               *
      * PROGRAMMED BY.......Aquitec Inc.                               *
      * Date coded..........13 DECEMBER 2002                           *
      * System..............eWMS                                       *
      * Frequency used......ON REQUEST                                 *
      *                                                                *
      * Input Tables.                                                  *
      * ------------                                                   *
      *  SCWT_RPT_REQUEST                                              *
      *  SCWT_SYS_PARMS                                                *
      *  SCWT_PRINTER_MAP                                              *
      *                                                                *
      * Output Tables.                                                 *
      * -------------                                                  *
      *                                                                *
      *  Processing/Description                                        *
      *  ----------------------                                        *
      *  This PROGRAM will be used by the majority of programs that    *
      *  generates Java reports. It will be passed a report name and   *
      *  user. From this it will:                                      *
      *                                                                *
      *      1.  Route the report to the appropriate printer           *
      *      2.  Generate a unique report id                           *
      *                                                                *
      ******************************************************************
      
      *================================================================* 
      *                   PROGRAM CHANGE HISTORY                       * 
      *----------------------------------------------------------------* 
      *  CHANGE �CHANGE �                                              * 
      *   DATE  �NUMBER �      DETAILED CHANGE DESCRIPTION             * 
      *----------------------------------------------------------------*  
      * 13DEC02 |02-0001| Create Program                               *
      *         |       |                                              *   
      *================================================================*
           EJECT
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
      
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
      
       DATA DIVISION.
       FILE SECTION.
      
       WORKING-STORAGE SECTION.
      **************************************************************************
      *   General copybook that holds field sizes.                             *
      **************************************************************************
            COPY "SCWAAAW.cpy".              
      
      **************************************************************************
      *  FLAGS / Switches                                                      * 
      **************************************************************************
       01  WF-DEBUG                     PIC G VALUE N"Y"
           GET-PROPERTY SET-PROPERTY.
       01  WX-ERROR-METHOD              LIKE AA-INVOKE-ERROR.
       01  WS-SEPARATOR                 PIC X.
       01  WS-SEPARATOR1                PIC G.
       01  W9-REPORT-NUM                LIKE AA-INTEGER.
       01  WZ-JRPT-REPORT-SEQ-PICS9     PIC 9(9).
       01  WX-JRPT-REPORT-SEQ           PIC G(9).
       01  WX-jrpt-report-name-PICX     PIC X(256).  
       01  INT-VAL    object reference 
          "com.aquitecintl.ewms.common.facades.DateService".
      ****************************************************************
      *    TEMPORARY FIELDS          (-WST)                          *
      ****************************************************************
      *  Get current date and time. 
            Copy "SCWCDATL.cpy".
      * Copybook to return to java any database errors received.
            COPY "SCWSQLEL.cpy".          
                                            
      
      ****************************************************************
      *   DATABASE Tables and Copybooks                              *
      ****************************************************************
           EXEC SQL        INCLUDE SQLCA.cpy    END-EXEC.
      
      *  SCWT_SYS_PARMS Table.
            COPY "SCWSYSPD.cpy".
      *  SCWT_RPT_REQUEST Table.
            COPY "SCWRPTRD.cpy".
      *  Next number linkage                                  *
       COPY "SCWNXTNL.cpy".

       01  INVOKER-SERVICE              OBJECT REFERENCE 
          "com.aquitecintl.ewms.common.process.ProcessInvokerService".
       01  LINK-CONTENTS    object reference 
          "com.aquitecintl.ewms.common.facades.DateService".   
            
      ****************************************************************
      *   Linkage SECTION                                            *
      ****************************************************************
       LINKAGE SECTION.
       COPY "SCWNODE.cpy". 
       COPY "SCWJRPTL.cpy".
          
              

       PROCEDURE DIVISION using node-parms
                                jrpt-parms.
      ********************
       0000-JRPT-MAINLINE SECTION.
       0000-JRPT-MAINLINE-START.
           PERFORM 1000-INITIALIZE. 
                 
           PERFORM 2000-PROCESS-REPORT.
                      
           PERFORM 9000-END-RUN.
           
       0000-JRPT-MAINLINE-EXIT.
           PERFORM 9990-JRPT-TERMINATE-PROGRAM.
      
      ********************
      
       1000-INITIALIZE SECTION.
       1000-START.
       
           INITIALIZE ERR-RETURN-CODE.
           MOVE N"0000000000"                 TO ERR-RTN-CODE.
           MOVE N"N"                    TO ERR-RTN-POISON.
      
      * Create a connetion to DB2          
           Copy "SCWCNCTP.cpy".
           Copy "SCWCNTDB.cpy".
           
           EXEC SQL
               SELECT coalesce(debug_field,'N')
                INTO :wf-debug
                FROM scst_module_hdr
               WHERE module_name = 'SCWJRPT'
           END-EXEC.
      
           IF WF-DEBUG                  = N"Y" OR N"P"
              DISPLAY "******************************************"
              DISPLAY "* SCWJRPT  STARTED                       *"
              DISPLAY "******************************************"
           END-IF.         

           INITIALIZE Node-result-field
                      Node-error-message-data
                      Node-poison-message-flag.
       
           IF WF-DEBUG                  = N"Y"
               display "LINKAGE RECEIVED: " JRPT-PARMS
           END-IF.
                              
           INVOKE "com.aquitecintl.ewms.common.facades.DateService"
            "getInstance"               RETURNING INT-VAL
           ON EXCEPTION
               MOVE N"INVK0002"         TO ERR-RTN-CODE           
               MOVE N"getInstance"      TO WX-ERROR-METHOD
               perform 9999-error-handle
           END-INVOKE.                                       
       1000-EXIT.
           EXIT.
           
      * ================================================================
       2000-PROCESS-REPORT SECTION.
       2000-START.
      
      *  Step 1: Find the directory to write the report to.
      
      *   Find the report relating to the report control table for the
      *   report name and user passed.
           INVOKE INT-VAL "getParsedString"
               USING jrpt-report-name
               RETURNING WX-jrpt-report-name-picx.
      
           EXEC SQL
                SELECT  report_printer, report_type
                INTO   :report-printer, :report-type
                FROM    scwt_rpt_request
                WHERE   cpy_cd          = :node-company-code
                AND     facl            = :node-facility
                AND     whse            = :node-warehouse
                AND     report_program  = :wx-jrpt-report-name-picx
                AND     report_user     = :node-user
           END-EXEC.

           IF SQLCODE = 0 
              CONTINUE
           ELSE
              PERFORM 2100-GET-DEFAULT-PRINT-USER
              IF SQLCODE = 0
                  CONTINUE             
              ELSE
                 GO TO 2999-EXIT
              END-IF   
           END-IF.

           IF WF-DEBUG = N"Y"
              DISPLAY "REPORT PRINTER  : " REPORT-PRINTER
           END-IF.
           
           EXEC SQL
                SELECT  spool_path
                INTO   :jrpt-spool-path
                FROM    scwt_printer_map
                WHERE   printer_name    = :report-printer
           END-EXEC.
         
           IF SQLCODE = 0 
              CONTINUE
           ELSE
              MOVE N"JRPT1002"          TO ERR-RTN-CODE
              STRING N"ST:/"
                     SQLSTATE
                      N"/"
                     REPORT-PRINTER
                                    DELIMITED BY SIZE
                                    INTO ERR-RTN-DATA
              MOVE N"Y"              TO  ERR-RTN-POISON
              MOVE SQLCODE          TO  ERR-SQLCODE        
              MOVE SQLSTATE         TO  ERR-SQLSTATE
              PERFORM 9900-REPLACE-NODE
              PERFORM 9990-JRPT-TERMINATE-PROGRAM
           END-IF.
     
      *  Step 2: Build the report name
      
           CALL "SCWCDAT" using CDAT-PARMS
           ON OVERFLOW
               MOVE  N"JRPT1005"    TO ERR-RTN-CODE
               MOVE N"Y"            TO ERR-RTN-POISON
               PERFORM 9900-REPLACE-NODE
               PERFORM 9990-JRPT-TERMINATE-PROGRAM
           END-CALL.
                   
      * Get file separators N"/" for Unix/Linux
      *                     "\" for Dos/Windows
      
           INVOKE "java.lang.System" "getProperty" 
                                USING 
                                BY VALUE "file.separator"
                                RETURNING WS-SEPARATOR.
      *             
           IF WF-DEBUG                  = N"Y"
              DISPLAY "WS-SEPARATOR : " WS-SEPARATOR
           END-IF.        
                        
                        
           INVOKE "com.aquitecintl.ewms.common.facades.DateService"
            "getInstance"               RETURNING LINK-CONTENTS
           ON EXCEPTION
               MOVE "INVK0002"         TO ERR-RTN-CODE           
               MOVE "getInstance"      TO WX-ERROR-METHOD
               PERFORM 9990-JRPT-TERMINATE-PROGRAM
           END-INVOKE.   

           INVOKE LINK-CONTENTS "expandString"
                                        USING WS-SEPARATOR
                                              RETURNING WS-SEPARATOR1

           IF WF-DEBUG                  = N"Y"
              DISPLAY "WS-SEPARATOR1 : " WS-SEPARATOR1
           END-IF.                     
                                      
           MOVE node-company-code       TO NXTN-CPY-CD
           MOVE node-facility           TO NXTN-FACL
           MOVE node-warehouse          TO NXTN-WHSE
           MOVE N"RPT"                  TO NXTN-TYPE
           CALL "SCWNXTN"               USING NXTN-PARMS 
                                              ERR-RETURN-CODE
           ON OVERFLOW
              INITIALIZE NXTN-SEQUENCE-9
           END-CALL.
           INVOKE INT-VAL "expandString"
                                        USING NXTN-SEQUENCE-9
                               RETURNING WX-JRPT-REPORT-SEQ  
 
      * The STRING statement concatenates two or more data items
      * storing the result in another data item.
           MOVE SPACES                 TO JRPT-FILE-NAME
                                           JRPT-SPOOL-FILE.  

           STRING NODE-USER             DELIMITED BY SPACES
                  N"."                   DELIMITED BY SIZE
                  JRPT-REPORT-NAME      DELIMITED BY SPACES
                  N"."                   DELIMITED BY SIZE 
                  CDAT-DATE-8           DELIMITED BY SIZE 
                 
                  N"."                   DELIMITED BY SIZE 
                  CDAT-TIME             DELIMITED BY SIZE
                  N"."                  DELIMITED BY SIZE
                  WX-JRPT-REPORT-SEQ    DELIMITED BY SIZE
                 
                                        INTO JRPT-FILE-NAME
           END-STRING.

           STRING JRPT-SPOOL-PATH       DELIMITED BY SPACES
                  WS-SEPARATOR1          DELIMITED BY SIZE
                  NODE-USER             DELIMITED BY SPACES
                  N"."                   DELIMITED BY SIZE
                  JRPT-REPORT-NAME      DELIMITED BY SPACES
                  N"."                   DELIMITED BY SIZE 
                  CDAT-DATE-8           DELIMITED BY SIZE 
        
                  N"."                   DELIMITED BY SIZE 
                  CDAT-TIME             DELIMITED BY SIZE
                  N"."                  DELIMITED BY SIZE
                  WX-JRPT-REPORT-SEQ    DELIMITED BY SIZE
        
                                        INTO JRPT-SPOOL-FILE
           END-STRING.
          
      *  Java Report Parameter Variables.
           MOVE REPORT-PRINTER         TO JRPT-PRINTER.

      *04 Begin Change ***** djf *******
      *  Report Type 
           move report-type             to jrpt-type 
           if wf-debug = N"Y" 
              display "jrpt-type = " jrpt-type 
           end-if
      *04 End Change ***** djf *******

           IF WF-DEBUG                  = N"Y"
              DISPLAY "JRPT-FILE-NAME  : " JRPT-FILE-NAME(1:80)
              DISPLAY "JRPT-SPOOL-PATH : " JRPT-SPOOL-PATH(1:80)
              DISPLAY "JRPT-PRINTER    : " JRPT-PRINTER(1:80)
              DISPLAY "JRPT-SPOOL-FILE : " JRPT-SPOOL-FILE(1:80)
           END-IF.
                                
       2999-EXIT.
            EXIT.
      
      * ================================================================
      
       2100-GET-DEFAULT-PRINT-USER SECTION.
       2100-START.
      
      *  Get the default system user
       
           EXEC SQL
                SELECT  default_print_user
                INTO   :default-print-user
                FROM    scwt_sys_parms
                WHERE   cpy_cd       = :node-company-code
                AND     facl         = :node-facility
                AND     whse         = :node-warehouse
           END-EXEC.
      
           IF  SQLCODE = 0
               CONTINUE
           ELSE
               MOVE N"JRPT1003"   TO  ERR-RTN-CODE           
               PERFORM 9999-ERROR-HANDLE
           END-IF.
      
      * Now that you have the default user, get the default printer or
      * directory assinged to the default user.
      
           MOVE DEFAULT-PRINT-USER     TO   REPORT-USER.
           
           EXEC SQL
                SELECT  report_printer, report_type
                INTO   :report-printer, :report-type
                FROM    scwt_rpt_request
                WHERE   cpy_cd         = :node-company-code
                AND     facl           = :node-facility
                AND     whse           = :node-warehouse
                AND     report_program = :wx-jrpt-report-name-picx
                AND     report_user    = :report-user
           END-EXEC. 
           
           IF SQLCODE = 0
              CONTINUE
           ELSE
               MOVE N"JRPT1004  "   TO  ERR-RTN-CODE           
               PERFORM 9999-ERROR-HANDLE
           END-IF.
                                             
       2100-EXIT.
           EXIT.
           
      * ================================================================
       9000-END-RUN SECTION.
       9000-START.
           IF WF-DEBUG                  = N"Y"
              DISPLAY "******************************************"
              DISPLAY "* SCWJRPT  DONE                          *"
              DISPLAY "******************************************"
           END-IF.   
           
       9999-EXIT.
           EXIT.
      
      *=========================================================================
      * This section moves a description to the error message field sothat     *
      * the fronend can display and/or handle the error that occured.          *
      *=========================================================================
       9999-ERROR-HANDLE SECTION.
       9999-ERROR-HANDLE-START.
           
           MOVE SPACES                 TO ERR-RTN-DATA.
           EVALUATE ERR-RTN-CODE
           WHEN N"INVK0001"
           WHEN N"INVK0002"
           WHEN N"INVK0003"
           WHEN N"INVK0004"
                MOVE WX-ERROR-METHOD   TO ERR-RTN-DATA           
           WHEN OTHER   
                STRING N"ST:"
                       SQLSTATE
                       N" /"
                       NODE-COMPANY-CODE,
                        N"/",
                       NODE-FACILITY,
                        N"/",
                       NODE-WAREHOUSE,
                        N"/",
                       JRPT-REPORT-NAME
                        N"/",
                       NODE-USER
                                        DELIMITED BY SIZE 
                                        INTO ERR-RTN-DATA       
                END-STRING
                MOVE N"Y"               TO ERR-RTN-POISON
                MOVE SQLCODE           TO ERR-SQLCODE
                MOVE SQLSTATE          TO ERR-SQLSTATE
           END-EVALUATE.
      
           IF WF-DEBUG                  = N"Y" 
              DISPLAY " 9999-ERROR : " ERR-RETURN-CODE
           END-IF.    
      
           PERFORM 9900-REPLACE-NODE.
           PERFORM 9990-JRPT-TERMINATE-PROGRAM.
                   
       9999-ERROR-HANDLE-EXIT.
           EXIT. 
      
      *=========================================================================
      * This section move the values of the error that occured back to the     *
      * node for java to handle the error.                                     *
      *=========================================================================
       9900-REPLACE-NODE SECTION.
       9900-REPLACE-NODE-START.
      
           MOVE ERR-RTN-CODE          TO NODE-RESULT-FIELD.
           MOVE ERR-RTN-DATA          TO NODE-ERROR-MESSAGE-DATA.       
           MOVE ERR-RTN-POISON        TO NODE-POISON-MESSAGE-FLAG.
      
       9900-REPLACE-NODE-EXIT.
           EXIT.
      
      *=========================================================================
      * This section terminates and exits the program.                         *
      * This condition will be executed normally or if an error has occured    *
      * and processing needs to be terminated without finishing the program.   *
      *=========================================================================
       9990-JRPT-TERMINATE-PROGRAM SECTION.
       9990-JRPT-TERMINATE-START.
           IF WF-DEBUG                  = N"Y" OR N"P"
              DISPLAY "******************************************"
              DISPLAY "* SCWJRPT  GOBACK                        *"
              DISPLAY "******************************************"
           END-IF.

           GOBACK.

      
      
