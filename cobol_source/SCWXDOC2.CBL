000100 IDENTIFICATION DIVISION.
000200 PROGRAM-ID. SCWXDOC2.
      ******************************************************************
      *                                                                *
      *  Copyright 2002 Aquitec International, Inc.                    *
      *  All rights reserved                                           *
      * ---------------------------------------------------------------*
      *  Version 1  Release  0                                         *
      *                                                                *
      ******************************************************************
      
      *================================================================* 
      * PROGRAM/MODULE  NAME........SCWXDOC2                           *
      *                 Cross dock Task Creation.                      *
      * DESIGNED BY.........Aquitec Inc.                               *
      * PROGRAMMED BY.......Aquitec Inc.                               *
      * Date coded..........29 November 2002                           *
      * System..............eWarehousing                               *
      * Frequency used......ON REQUEST                                 *
      *                                                                *
      * Input Tables.                                                  *
      * -------------                                                  *
      * SCWT_PR_HEADER                                                 *
      * SCWT_PLAN_SHIP_HDR                                             *
      * SCWT_PALLET                                                    *
      * SCWT_PALLET_DETAIL                                             *
      *                                                                *   
      * Program Output                                                 *
      * --------------                                                 *
      *                                                                *
      *  Processing/Description                                        *
      *  ----------------------                                        *
      *  This program loops through all of the pallets created as part *
      *  of the XDOC receiving process and creates tasks to move the   *
      *  pallets to the customer load points.                          *
      *                                                                *
      *                                                                *
      ******************************************************************

      *================================================================* 
      *                   PROGRAM CHANGE HISTORY                       * 
      *----------------------------------------------------------------* 
      *  CHANGE �CHANGE �                                              * 
      *   DATE  �NUMBER �      DETAILED CHANGE DESCRIPTION             * 
      *----------------------------------------------------------------* 
      * 29NOV02 |..-....| New Program                                  *
      * 01DEC09 |07-1168|Get debug flag from db rather than front end *
      * 01DEC09 |09-1091|Only call MTKC once per pallet.  If multi-item*   
      *         |       |     pallet, initialise pr-line value.        *   
      *         |       |Also remove extra unnecessary dble-byte code*
      *================================================================*
       ENVIRONMENT DIVISION.
      *--------------------- 
      *
       DATA DIVISION. 
      *--------------
      ******************************************************************
      ******************************************************************
      *                                                              ***
      * W O R K I N G    S T O R A G E.                              ***
      *                                                              ***
      ******************************************************************
      ****************************************************************** 
       WORKING-STORAGE SECTION.
      *------------------------
      ********  General copybook that holds field sizes.                
       COPY "SCWAAAW.cpy". 
       01  WX-ERROR-METHOD              LIKE AA-INVOKE-ERROR.
      *
       01  INT-VAL    object reference 
          "com.aquitecintl.ewms.common.facades.DateService". 
       01  WF-DEBUG                     PIC G         VALUE N"Y"
           get-property  set-property.  
       01  WX-STORE-PALLET              LIKE AA-BIGINT VALUE ZEROES.  
       01  W9-ADD-SERIAL                LIKE AA-BIGINT VALUE ZEROES.
      *09 Begin 09-1091
       01  W9-PREV-PALLET               LIKE AA-BIGINT VALUE ZEROES.  
      *09 End   09-1091
      *                                                 
      ******************************************************************
      *         D a t a b a s e     T a b l e s                        *
      ******************************************************************
      *********     *******************     ***************     ********
      ********  Planned Receipt Header Table.                      
       COPY "SCWPRHRD.cpy".
      *       
      ********  Pallet table.
       COPY "SCWPALTD.cpy".
      ********  Pallet DETAIL table.
       COPY "SCWPALDD.cpy".
      * 
      ******************************************************************
      * O t h e r  w o r k i n g  s t o r a g e  c o p y b o o k s     * 
      ******************************************************************
      *********     *******************     ***************     ********
      */////////////
      ********  Standard error return code                                
      */////////////
       COPY "SCWSQLEL.cpy". 
      *
       COPY "SCWPALSL.cpy". 
      *
       COPY "SCWMTKCL.cpy".  
      ********  Sqlca                                                  
       EXEC SQL        INCLUDE SQLCA.cpy    END-EXEC.
      *
      ********  Java Node Copybook                                      
       COPY "SCWNODE.cpy".    
      *
      **************************************************************************
      * Receiving working storage common                                       * 
      **************************************************************************
       COPY "SCWRECVW.cpy". 
      ******************************************************************
      ******************************************************************
      *                                                              ***
      * P R O C E D U R E   D I V I S I O N.                         ***
      *                                                              ***
      ******************************************************************
      ******************************************************************
       PROCEDURE DIVISION.
      *-------------------
       0000-XDOC2-MAIN SECTION.
      *-----------------------------------------------------0000-START-*
       0000-XDOC2-START-PARA.
      *---------------------
      *****************

         INVOKE "com.aquitecintl.ewms.common.facades.DateService"
            "getInstance"               RETURNING INT-VAL
           ON EXCEPTION
               MOVE "INVK0002"         TO ERR-RTN-CODE
               MOVE "getInstance"      TO WX-ERROR-METHOD
               PERFORM 9999-TERMINATE
           END-INVOKE.
            if wf-debug = N"Y" 
               display "**********************************************"
               display "SCWXDOC2 is starting "
               display "**********************************************"
            end-if   

      ******************      
      *09 Begin 09-1091
      *09        INVOKE INT-VAL "getParsedString"
      *09                                        USING PALS-PALLET-SEQ
      *09                               RETURNING PALS-PALLET-SEQ-PICS9  
      *09        INVOKE INT-VAL "getParsedString"
      *09                                        USING PALS-UPDATE-SERIAL
      *09                               RETURNING PALS-UPDATE-SERIAL-PICS9  
      *09        INVOKE INT-VAL "getParsedString"
      *09                                        USING MTKC-PALLET-SEQ
      *09                               RETURNING MTKC-PALLET-SEQ-PICS9
      *09        INVOKE INT-VAL "getParsedString"
      *09                                        USING MTKC-FULL-PALT-PICK
      *09                               RETURNING MTKC-FULL-PALT-PICK-PICS9  
      *09        INVOKE INT-VAL "getParsedString"
      *09                                        USING MTKC-PICK-RELEASE
      *09                               RETURNING MTKC-PICK-RELEASE-PICS9  
      *09        INVOKE INT-VAL "getParsedString"
      *09                                        USING MTKC-PRE-REQ-TASK
      *09                               RETURNING MTKC-PRE-REQ-TASK-PICS9  
      *09        INVOKE INT-VAL "getParsedString"
      *09                                        USING MTKC-TRANS-ID
      *09                               RETURNING MTKC-TRANS-ID-PICS9  
      *09        INVOKE INT-VAL "getParsedString"
      *09                                        USING MTKC-PALLET-QTY
      *09                               RETURNING MTKC-PALLET-QTY-PICS9  
      *09        INVOKE INT-VAL "getParsedString"
      *09                                        USING MTKC-PR
      *09                               RETURNING MTKC-PR-PICS9  
      *09        INVOKE INT-VAL "getParsedString"
      *09                                        USING MTKC-PR-REF
      *09                               RETURNING MTKC-PR-REF-PICS9  
      *09        INVOKE INT-VAL "getParsedString"
      *09                                        USING MTKC-PR-LINE
      *09                               RETURNING MTKC-PR-LINE-PICS9  
      *09        INVOKE INT-VAL "getParsedString"
      *09                                        USING MTKC-UPDATE-SERIAL
      *09                               RETURNING MTKC-UPDATE-SERIAL-PICS9  
      *09 End   09-1091
        INVOKE INT-VAL "getParsedString"
                                        USING node-transaction-id
                               RETURNING node-transaction-id-PIC9  
        INVOKE INT-VAL "getParsedString"
                                        USING recv-pr
                               RETURNING recv-pr-PICS9  
        INVOKE INT-VAL "getParsedString"
                                        USING recv-pick-bseq
                               RETURNING recv-pick-bseq-PICS9  
            PERFORM 1000-INITIALIZE.
      *
            PERFORM 2000-PROCESS.
      *     
            PERFORM 9999-TERMINATE.
      *
       0000-XDOC2-MAIN-EXIT.
      *-----------------------------------------------------0000---END-*
            EXIT.
      *
       1000-INITIALIZE SECTION.
      *-----------------------------------------------------1000-START-*
       1000-INIT-START-PARA.
      *---------------------
      *      INITIALIZE ERR-RETURN-CODE.
            MOVE N"0000000000"      TO ERR-RTN-CODE.     
      * Create a connetion to DB2          
            Copy "SCWCNCTP.cpy".
            Copy "SCWCNTDB.cpy".
                  
      *&07 Begin 07-1168 
      ** Get the debug flag for slot from the module header table
           EXEC SQL
               SELECT coalesce(debug_field,'N')
                INTO :wf-debug
                FROM scst_module_hdr
               WHERE module_name = 'SCWXDOC2'
           END-EXEC.
      *&07 End   07-1168 

      *     EXEC SQL 
      *        DECLARE curpall1 CURSOR FOR
      *        SELECT *                             
      *        FROM scwt_pallet
      *        WHERE cpy_cd             = :node-company-code
      **         AND facl               = :node-facility
      **         AND whse               = :node-warehouse
      **         AND pr                 = :recv-pr-PICS9
      *     END-EXEC. 
            EXEC SQL 
               DECLARE curpall1 CURSOR FOR
               SELECT a.PALLET_ID
                     ,a.PALLET_SEQ
                     ,a.ITEM_OWNER
                     ,a.CURR_LOCATION
                     ,a.CUSTOMER_CODE
                     ,a.SHIP_DATE
                     ,a.SHIP_ROUTE
                     ,a.SHIP_STOP
                     ,a.PALLET_CASES
                     ,a.TO_TRADER_TYPE
                     ,a.LOAD_POINT
                     ,a.update_serial
                     ,b.pr_line
                     ,b.plan_shipment                            
               FROM scwt_pallet a
                   ,scwt_pallet_detail b               
              WHERE a.cpy_cd    = :node-company-code
                 AND a.facl     = :node-facility
                 AND a.whse     = :node-warehouse
                 AND a.pallet_seq        
                           = (select distinct b.pallet_seq
                                from scwt_pallet_detail
                               where b.cpy_cd = :node-company-code
                                 AND b.faCL = :node-facility
                                 AND b.whse = :node-warehouse
                                 AND b.pr   = :recv-pr-PICS9)
            END-EXEC. 
      *      
       1000-INITIALIZE-EXIT.
      *-----------------------------------------------------1000---END-*
            EXIT.
      *     
       2000-PROCESS SECTION.
      *-----------------------------------------------------2000-START-*
       2000-PROC-START-PARA.
      *---------------------
            EXEC sql
               OPEN curpall1
            END-EXEC.
      * begin OT upgrade cursor check
           if wf-debug = N"Y" or N"P"
              display "OT upgrade cursor check 130 cursor curpall1"
              display "sqlcode = " sqlcode ";sqlstate= " sqlstate
           END-IF           
      * end   OT upgrade cursor check
            MOVE N"XDOC2001" TO NODE-RESULT-FIELD
            PERFORM 9990-NO-ALLOWANCE-SQL-CHECK
            
            EXEC sql
               FETCH curpall1
               INTO   :scwt-pallet.PALLET-ID
                     ,:scwt-pallet.PALLET-SEQ
                     ,:scwt-pallet.ITEM-OWNER
                     ,:scwt-pallet.CURR-LOCATION
                     ,:scwt-pallet.CUSTOMER-CODE
                     ,:scwt-pallet.SHIP-DATE
                     ,:scwt-pallet.SHIP-ROUTE
                     ,:scwt-pallet.SHIP-STOP
                     ,:scwt-pallet.PALLET-CASES
                     ,:scwt-pallet.TO-TRADER-TYPE
                     ,:scwt-pallet.LOAD-POINT
                     ,:scwt-pallet.UPDATE-SERIAL
                     ,:SCWT-PALLET-DETAIL.PR-LINE
                     ,:SCWT-PALLET-DETAIL.PLAN-SHIPMENT
            END-EXEC.
            MOVE PALLET-SEQ OF SCWT-PALLET  TO WX-STORE-PALLET.
            IF WF-DEBUG = N"Y"
               DISPLAY " INITIAL PALLET-SEQ : " WX-STORE-PALLET
            END-IF.                              
            MOVE N"XDOC2002" TO NODE-RESULT-FIELD.
            PERFORM 9990-NO-ALLOWANCE-SQL-CHECK.
            INITIALIZE W9-PREV-PALLET
            INVOKE INT-VAL "expandString"
                   USING ship-date of scwt-pallet
               RETURNING ship-date of scwt-pallet
            
            PERFORM UNTIL SQLCODE = +100
             IF PALLET-ID OF SCWT-PALLET <> W9-PREV-PALLET
               MOVE PALLET-ID OF SCWT-PALLET TO W9-PREV-PALLET
               IF PALLET-LINES OF SCWT-PALLET > 1
                  MOVE 0 TO PR-LINE OF SCWT-PALLET-DETAIL
                  if wf-debug = N"Y" 
                     display ">1 line on pallet - init line"
                  end-if
               END-IF
               PERFORM 3000-MTKC-CALL
      *         PERFORM 4000-CALL-PALS
      *09 Begin 09-1091
             END-IF
      *09 End   09-1091          
               EXEC sql
               FETCH curpall1
               INTO   :scwt-pallet.PALLET-ID
                     ,:scwt-pallet.PALLET-SEQ
                     ,:scwt-pallet.ITEM-OWNER
                     ,:scwt-pallet.CURR-LOCATION
                     ,:scwt-pallet.CUSTOMER-CODE
                     ,:scwt-pallet.SHIP-DATE
                     ,:scwt-pallet.SHIP-ROUTE
                     ,:scwt-pallet.SHIP-STOP
                     ,:scwt-pallet.PALLET-CASES
                     ,:scwt-pallet.TO-TRADER-TYPE
                     ,:scwt-pallet.LOAD-POINT
                     ,:scwt-pallet.UPDATE-SERIAL
                     ,:SCWT-PALLET-DETAIL.PR-LINE
                     ,:SCWT-PALLET-DETAIL.PLAN-SHIPMENT
               END-EXEC
               
               MOVE N"XDOC2003" TO NODE-RESULT-FIELD
               IF SQLCODE NOT = ZERO AND +100
                  PERFORM 9998-ERROR-SETUP
                  PERFORM 9999-TERMINATE
               ELSE
                  MOVE N"0000000000" TO NODE-RESULT-FIELD 
                  INVOKE INT-VAL "expandString"
                          USING ship-date of scwt-pallet
                      RETURNING ship-date of scwt-pallet
                     
               END-IF
               
               IF PALLET-SEQ OF SCWT-PALLET = WX-STORE-PALLET
                  ADD 1                TO W9-ADD-SERIAL
                  ADD W9-ADD-SERIAL    TO UPDATE-SERIAL
                                        OF SCWT-PALLET
                  IF WF-DEBUG = N"Y"
                     DISPLAY " ADD 1 TO UPD SER : " W9-ADD-SERIAL
                     DISPLAY " PALLET SERIAL    : " UPDATE-SERIAL
                                        OF SCWT-PALLET
                  END-IF
               ELSE
                  MOVE PALLET-SEQ       OF SCWT-PALLET      
                                       TO WX-STORE-PALLET 
                  MOVE ZERO            TO W9-ADD-SERIAL                      
                  IF WF-DEBUG = N"Y"
                     DISPLAY " NEXT PALLET-SEQ : " WX-STORE-PALLET
                  END-IF
               END-IF  
                                      
           END-PERFORM.    
      *
       2000-PROCESS-EXIT.
      *-----------------------------------------------------2000---END-*
            EXIT.
      *
       3000-MTKC-CALL SECTION.
      *-----------------------------------------------------3000-START-*
       3000-MTKC-START-PARA.
      *---------------------
            EXEC sql
               SELECT pr_reference
                     ,xdoc_order
               INTO :scwt-pr-header.pr-reference
                   ,:scwt-pr-header.xdoc-order
               FROM scwt_pr_header
               WHERE cpy_cd             = :node-company-code
                 AND facl               = :node-facility
                 AND whse               = :node-warehouse
                 AND pr                 = :recv-pr-PICS9
            END-EXEC.
            
            MOVE N"XDOC2004" TO NODE-RESULT-FIELD.
            PERFORM 9990-NO-ALLOWANCE-SQL-CHECK.
                    
            IF RECV-PUTAWAY-RULE = SPACES or N" " 
               continue 
            ELSE 
               move RECV-PUTAWAY-RULE  to MTKC-to-location 
            end-if
               
            MOVE NODE-COMPANY-CODE     TO MTKC-CPY-CD.                  
            MOVE NODE-FACILITY         TO MTKC-FACL                              
                                          mtkc-pallet-facl
            MOVE NODE-WAREHOUSE        TO MTKC-WHSE                                 
                                          mtkc-pallet-whse
            MOVE N"P"                  TO MTKC-TASK-TYPE.                             
            MOVE PALLET-SEQ OF SCWT-PALLET 
                                       TO MTKC-PALLET-SEQ-PICS9.
            MOVE SHIP-ROUTE OF SCWT-PALLET 
                                       TO MTKC-ROUTE
            MOVE SHIP-STOP  OF SCWT-PALLET
                                       TO MTKC-STOP
            MOVE LOAD-POINT OF SCWT-PALLET
                                       TO MTKC-LOADPOINT
            MOVE UPDATE-SERIAL OF SCWT-PALLET 
                                       TO MTKC-UPDATE-SERIAL-PICS9. 
            MOVE PLAN-SHIPMENT OF SCWT-PALLET-DETAIL
                                       TO MTKC-PLAN-SHIPMENT-PICS9.             
            MOVE RECV-DOOR             TO MTKC-FROM-LOCATION.                               
            MOVE N"R"                  TO MTKC-FROM-POSITION.                      
            MOVE LOAD-POINT OF SCWT-PALLET  
                                       TO MTKC-TO-LOCATION
                                          MTKC-LOADPOINT.                         
            MOVE N"R"                  TO MTKC-TO-POSITION.        
            MOVE N"N"                  TO MTKC-FULL-PALT-PICK.                                               
      *      MOVE SHIP-CYCLE OF SCWT-PALLET
      *                                 TO MTKC-PICK-CYCLE.                           
      *  MOVE RECV-PICK-WAVE        TO MTKC-PICK-WAVE.                          
      *      MOVE recv-pick-bseq-PICS9  TO MTKC-PICK-BILL-SEQ. 
            MOVE 0                     TO MTKC-PICK-BILL-SEQ 
            IF RECV-RELEASE-TASK = N"Y"
               MOVE N"R"               TO MTKC-TASK-STATUS             
            ELSE
               MOVE N"P"                      TO MTKC-TASK-STATUS             
            END-IF.                             
            MOVE PALLET-CASES OF SCWT-PALLET
                                       TO MTKC-ORIG-PALT-QTY-PICS9.
            MOVE PALLET-CASES OF SCWT-PALLET TO MTKC-PALLET-QTY-PICS9.                   
            MOVE CUSTOMER_CODE OF SCWT-PALLET
                                       TO MTKC-CUST-CODE.                    
            MOVE RECV-PR-PICS9         TO MTKC-PR-PICS9.                       
            MOVE PR-REFERENCE OF SCWT-PR-HEADER TO MTKC-PR-REF-PICS9.                                    
            MOVE N"10"                 TO MTKC-FROM-LOCATION-TYPE.                
            MOVE N"10"                 TO MTKC-TO-LOCATION-TYPE.
            MOVE SHIP-DATE OF SCWT-PALLET 
                                       TO MTKC-SHIP-DATE.
            INITIALIZE                    MTKC-DEFAULT-REASON                                                 
                                          MTKC-SLOT-MOVE-TYPE
                                          MTKC-PICK-RELEASE-PICS9
                                          MTKC-PRE-REQ-TASK-PICS9
                                          MTKC-PR-STACK-FLAG                   
                                          MTKC-WHSE-DEPT                                            
                                          MTKC-EMPLOYEE-CODE 
                                          MTKC-FROM-SECTION.
            MOVE node-transaction-id-PIC9   TO MTKC-TRANS-ID-PICS9.   
            IF WF-DEBUG = N"Y"                             
               DISPLAY " PR-LINE " PR-LINE OF SCWT-PALLET-DETAIL
            END-IF.   
            MOVE PR-LINE OF SCWT-PALLET-DETAIL
                                       TO MTKC-PR-LINE-PICS9.  
      *****************     
        INVOKE INT-VAL "expandString"
                                        USING MTKC-PALLET-SEQ-PICS9
                               RETURNING MTKC-PALLET-SEQ  
        INVOKE INT-VAL "expandString"
                                        USING MTKC-FULL-PALT-PICK-PICS9
                               RETURNING MTKC-FULL-PALT-PICK  
        INVOKE INT-VAL "expandString"
                                        USING MTKC-PICK-RELEASE-PICS9
                               RETURNING MTKC-PICK-RELEASE  
        INVOKE INT-VAL "expandString"
                                        USING MTKC-PRE-REQ-TASK-PICS9
                               RETURNING MTKC-PRE-REQ-TASK  
        INVOKE INT-VAL "expandString"
                                        USING MTKC-TRANS-ID-PICS9
                               RETURNING MTKC-TRANS-ID  
        INVOKE INT-VAL "expandString"
                                        USING MTKC-PLAN-SHIPMENT-PICS9
                               RETURNING MTKC-PLAN-SHIPMENT
        INVOKE INT-VAL "expandString"
                                        USING MTKC-PALLET-QTY-PICS9
                               RETURNING MTKC-PALLET-QTY  
        INVOKE INT-VAL "expandString"
                                        USING MTKC-ORIG-PALT-QTY-PICS9
                               RETURNING MTKC-ORIG-PALT-QTY  
        INVOKE INT-VAL "expandString"
                                        USING MTKC-PR-PICS9
                               RETURNING MTKC-PR  
        INVOKE INT-VAL "expandString"
                                        USING MTKC-PR-REF-PICS9
                               RETURNING MTKC-PR-REF  
        INVOKE INT-VAL "expandString"
                                        USING MTKC-PR-LINE-PICS9
                               RETURNING MTKC-PR-LINE  
        INVOKE INT-VAL "expandString"
                                        USING MTKC-UPDATE-SERIAL-PICS9
                               RETURNING MTKC-UPDATE-SERIAL  
        INVOKE INT-VAL "expandString"
                                        USING  node-transaction-id-PIC9
                               RETURNING  node-transaction-id  
        INVOKE INT-VAL "expandString"
                                        USING recv-pr-PICS9
                               RETURNING recv-pr  
        INVOKE INT-VAL "expandString"
                                        USING recv-pick-bseq-PICS9
                               RETURNING recv-pick-bseq  
            CALL "SCWMTKC" USING MTKC-PARMS ERR-RETURN-CODE
            ON OVERFLOW
               MOVE N"XDOC2006"         TO NODE-RESULT-FIELD
               MOVE ERR-RTN-DATA       TO NODE-ERROR-MESSAGE-DATA
               MOVE N"Y"                TO NODE-POISON-MESSAGE-FLAG
               PERFORM 9999-TERMINATE
            END-CALL.
            CANCEL "SCWMTKC".
            IF ERR-RTN-CODE             NOT =SPACES AND N" " AND " "
                           AND N"0000000000" 
               MOVE ERR-RTN-CODE       TO NODE-RESULT-FIELD 
               MOVE ERR-RTN-DATA       TO NODE-ERROR-MESSAGE-DATA
               MOVE ERR-RTN-POISON     TO NODE-POISON-MESSAGE-FLAG
               PERFORM 9999-TERMINATE
            END-IF.
             
      *
       3000-MTKC-CALL-EXIT.
      *-----------------------------------------------------3000---END-*
            EXIT.
      *   
      * 4000-CALL-PALS SECTION.
      *-----------------------------------------------------4000-START-* 
      * 4000-PALS-START-PARA.
      *-----------------------            
      *      MOVE NODE-COMPANY-CODE     TO PALS-CPY-CD.  
      *      MOVE NODE-FACILITY         TO PALS-FACL.       
      *      MOVE NODE-WAREHOUSE        TO PALS-WHSE.             
      *      MOVE PALLET-SEQ OF SCWT-PALLET 
      *                                 TO PALS-PALLET-SEQ-PICS9. 
      *      INVOKE INT-VAL "expandString"
      *                                  USING PALS-PALLET-SEQ-PICS9
      *                         RETURNING PALS-PALLET-SEQ  
      *      INITIALIZE                     PALS-NEW-LOCATION
      *                                     PALS-NEW-POSITION
      *                                     PALS-NEW-INV-STAT. 
      *      MOVE N"S"                   TO PALS-NEW-INV-TYPE. 
      *      MOVE NODE-USER             TO PALS-USER.
      *      MOVE node-transaction-id-PIC9   TO PALS-TRANSACTION.
      *      MOVE UPDATE-SERIAL OF SCWT-PALLET 
      *                                 TO PALS-UPDATE-SERIAL-PICS9.   
      *      INVOKE INT-VAL "expandString"
      *            USING PALS-UPDATE-SERIAL-PICS9
      *            RETURNING PALS-UPDATE-SERIAL  
      *                                 
      *      CALL "SCWPALS" USING PALS-PARMS ERR-RETURN-CODE
      *      ON OVERFLOW
      *         MOVE N"XDOC2007"         TO NODE-RESULT-FIELD
      *         MOVE ERR-RTN-DATA       TO NODE-ERROR-MESSAGE-DATA
      *         MOVE N"Y"                TO NODE-POISON-MESSAGE-FLAG
      *         PERFORM 9999-TERMINATE
      *      END-CALL.
      *      CANCEL "SCWPALS".
      *      IF ERR-RTN-CODE             NOT = SPACES AND N" " AND " "
      *                     AND N"0000000000" 
      *         MOVE ERR-RTN-CODE       TO NODE-RESULT-FIELD 
      *         MOVE ERR-RTN-DATA       TO NODE-ERROR-MESSAGE-DATA
      *         MOVE ERR-RTN-POISON     TO NODE-POISON-MESSAGE-FLAG
      *         PERFORM 9999-TERMINATE
      *      END-IF.                                              
      **      
      * 4000-CALL-PALS-EXIT.
      *-----------------------------------------------------4000---END-* 
      *      EXIT.
      *    
       9990-NO-ALLOWANCE-SQL-CHECK SECTION.
      *-----------------------------------------------------9990-START-* 
       9990-NO-SQL-START-PARA.
      *-----------------------            
            IF SQLCODE NOT              = ZEROES
               PERFORM 9998-ERROR-SETUP
               PERFORM 9999-TERMINATE
            ELSE
               MOVE N"0000000000" TO NODE-RESULT-FIELD  
            END-IF.    
      *
       9990-NO-ALLOWANCE-SQL-CHECK-EXIT.
      *-----------------------------------------------------9990---END-* 
            EXIT.
      *
       9998-ERROR-SETUP SECTION.
      *-----------------------------------------------------9998-START-*
       9998-ERROR-START-PARA.
      *----------------------
      *****************     
        INVOKE INT-VAL "expandString"
                                        USING PALS-PALLET-SEQ-PICS9
                               RETURNING PALS-PALLET-SEQ  
        INVOKE INT-VAL "expandString"
                                        USING PALS-UPDATE-SERIAL-PICS9
                               RETURNING PALS-UPDATE-SERIAL  
        INVOKE INT-VAL "expandString"
                                        USING MTKC-PALLET-SEQ-PICS9
                               RETURNING MTKC-PALLET-SEQ  
        INVOKE INT-VAL "expandString"
                                        USING MTKC-FULL-PALT-PICK-PICS9
                               RETURNING MTKC-FULL-PALT-PICK  
        INVOKE INT-VAL "expandString"
                                        USING MTKC-PICK-RELEASE-PICS9
                               RETURNING MTKC-PICK-RELEASE  
        INVOKE INT-VAL "expandString"
                                        USING MTKC-PRE-REQ-TASK-PICS9
                               RETURNING MTKC-PRE-REQ-TASK  
        INVOKE INT-VAL "expandString"
                                        USING MTKC-TRANS-ID-PICS9
                               RETURNING MTKC-TRANS-ID  
        INVOKE INT-VAL "expandString"
                                        USING MTKC-PALLET-QTY-PICS9
                               RETURNING MTKC-PALLET-QTY  
        INVOKE INT-VAL "expandString"
                                        USING MTKC-PR-PICS9
                               RETURNING MTKC-PR  
        INVOKE INT-VAL "expandString"
                                        USING MTKC-PR-REF-PICS9
                               RETURNING MTKC-PR-REF  
        INVOKE INT-VAL "expandString"
                                        USING MTKC-PR-LINE-PICS9
                               RETURNING MTKC-PR-LINE  
        INVOKE INT-VAL "expandString"
                                        USING MTKC-UPDATE-SERIAL-PICS9
                               RETURNING MTKC-UPDATE-SERIAL  
        INVOKE INT-VAL "expandString"
                                        USING  node-transaction-id-PIC9
                               RETURNING  node-transaction-id  
        INVOKE INT-VAL "expandString"
                                        USING recv-pr-PICS9
                               RETURNING recv-pr  
        INVOKE INT-VAL "expandString"
                                        USING recv-pick-bseq-PICS9
                               RETURNING recv-pick-bseq  
           invoke int-val "expandString"
                   using sqlstate 
                   returning wg-sqlstate
            STRING N"ST:",
                   wg-SQLSTATE,  
                   N"/",  
                   NODE-COMPANY-CODE,
                   N"/",
                   NODE-FACILITY,
                   N"/",
                   NODE-WAREHOUSE,
                   N"/",
                   NODE-USER,
            DELIMITED BY SIZE 
            INTO   NODE-ERROR-MESSAGE-DATA.
            MOVE N"Y"                   TO NODE-POISON-MESSAGE-FLAG.
      *
       9998-ERROR-SETUP-EXIT.
      *-----------------------------------------------------9998---END-*    
            EXIT.
      *     
       9999-TERMINATE SECTION.
      *-----------------------------------------------------9999-START-*
       9999-TERM-START-PARA.
      *---------------------
      *****************     
        INVOKE INT-VAL "expandString"
                                        USING PALS-PALLET-SEQ-PICS9
                               RETURNING PALS-PALLET-SEQ  
        INVOKE INT-VAL "expandString"
                                        USING PALS-UPDATE-SERIAL-PICS9
                               RETURNING PALS-UPDATE-SERIAL  
        INVOKE INT-VAL "expandString"
                                        USING MTKC-PALLET-SEQ-PICS9
                               RETURNING MTKC-PALLET-SEQ  
        INVOKE INT-VAL "expandString"
                                        USING MTKC-FULL-PALT-PICK-PICS9
                               RETURNING MTKC-FULL-PALT-PICK  
        INVOKE INT-VAL "expandString"
                                        USING MTKC-PICK-RELEASE-PICS9
                               RETURNING MTKC-PICK-RELEASE  
        INVOKE INT-VAL "expandString"
                                        USING MTKC-PRE-REQ-TASK-PICS9
                               RETURNING MTKC-PRE-REQ-TASK  
        INVOKE INT-VAL "expandString"
                                        USING MTKC-TRANS-ID-PICS9
                               RETURNING MTKC-TRANS-ID  
        INVOKE INT-VAL "expandString"
                                        USING MTKC-PALLET-QTY-PICS9
                               RETURNING MTKC-PALLET-QTY  
        INVOKE INT-VAL "expandString"
                                        USING MTKC-PR-PICS9
                               RETURNING MTKC-PR  
        INVOKE INT-VAL "expandString"
                                        USING MTKC-PR-REF-PICS9
                               RETURNING MTKC-PR-REF  
        INVOKE INT-VAL "expandString"
                                        USING MTKC-PR-LINE-PICS9
                               RETURNING MTKC-PR-LINE  
        INVOKE INT-VAL "expandString"
                                        USING MTKC-UPDATE-SERIAL-PICS9
                               RETURNING MTKC-UPDATE-SERIAL  
        INVOKE INT-VAL "expandString"
                                        USING  node-transaction-id-PIC9
                               RETURNING  node-transaction-id  
        INVOKE INT-VAL "expandString"
                                        USING recv-pr-PICS9
                               RETURNING recv-pr  
        INVOKE INT-VAL "expandString"
                                        USING recv-pick-bseq-PICS9
                               RETURNING recv-pick-bseq.  
            GOBACK.
      *-----------------------------------------------------9999-GOBACK* 