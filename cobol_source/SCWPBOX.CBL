       IDENTIFICATION DIVISION.
       PROGRAM-ID. SCWPBOX.
      /
      ******************************************************************
      *                                                                *
      *  Copyright 2002 Aquitec International, Inc.                    *
      *  All rights reserved                                           *
      * ---------------------------------------------------------------*
      *  Version 1  Release  0                                         *
      *                                                                *
      ******************************************************************
      
      *================================================================* 
      * PROGRAM/MODULE  NAME........SCWPBOX                            *
      *                                                                *  
      *              (REQ)  DETERMINATION OF PENALTY BOX               *
      *                                                                *
      * DESIGNED BY.........Aquitec Inc.                               *
      * PROGRAMMED BY.......Aquitec Inc.                               *
      * Date coded..........08 July 2002                               *
      * System..............eWMS                                       *
      * Frequency used......ON REQUEST                                 *
      *                                                                *
      * Input Tables.                                                  *
      * ------------                                                   *
      *  SCWT_LOCATION                                                 *
      *  SCWT-AREA                                                     *
      *  SCCT-VALIDVALDSC                                              *
      *                                                                *
      * Output Tables.                                                 *
      * -------------                                                  *
      *                                                                *
      *  Processing/Description                                        *
      *  ----------------------                                        *
      *  This PROGRAM will determine a penalty box or pick overflow    *
      *  area for a specified location in the warehouse.               *
      *                                                                *
      ******************************************************************
      
      *================================================================* 
      *                   PROGRAM CHANGE HISTORY                       * 
      *----------------------------------------------------------------* 
      *  CHANGE �CHANGE �                                              * 
      *   DATE  �NUMBER �      DETAILED CHANGE DESCRIPTION             * 
      *----------------------------------------------------------------*  
      * 08JUL02 |..-....| New Program                                  *
      *05Jul06  |06-1398|Slot held pallets separately                 *  
      *         |       |  - including pbox                           *        
      * 21AUG07 |07-1168|Get debug flag from db rather than front end  *
      * 01MAY08 |08-0355|Getting wrong field for debug flag            *
      *         |       |                                              *   
      *================================================================*
           EJECT
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
      
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
      
       DATA DIVISION.
       FILE SECTION.
      
       WORKING-STORAGE SECTION.
     
       01  WF-DEBUG                     PIC G VALUE N"Y"
           GET-PROPERTY SET-PROPERTY.
       01 WF-CHOICE               PIC 9   VALUE ZERO COMP-5.
      
      ****************************************************************
      *                                                              *
      *        TEMPORARY FIELDS                                      *
      *                                                              *
      ****************************************************************
      *   General copybook that holds field sizes.      
            COPY "SCWAAAW.cpy".              
      ** CDIDBCS ** 
       01  INT-VAL    object reference 
          "com.aquitecintl.ewms.common.facades.DateService". 
       01  WX-ERROR-METHOD              LIKE AA-INVOKE-ERROR .
      ****************************************************************         
      ****************************************************************
      *                                                              *
      *        DATABASE Tables and Copybooks                         *
      *                                                              *
      ****************************************************************
      
          EXEC SQL        INCLUDE SQLCA.cpy    END-EXEC.
                        
      *  SCWT_AREA Table.
            COPY "SCWAREAD.cpy".
      *  SCWT_LOCATION Table.
            COPY "SCWLOCAD.cpy".
      
      
       01 WS-DBASE-FIELDS.
          05 WF-SLOT-TYPE               PIC G(1).
          05 WF-SLOT-CONFIG             PIC G(1).
          05 W9-BAY                     PIC 9(3).
          05 WX-BAY                     PIC G(3) REDEFINES W9-BAY.
      
       01  Even-or-Odd                  PIC G.
           88 Even-Side               VALUE N"0" N"2" N"4" N"6" N"8".
           88 Odd-Side                VALUE N"1" N"3" N"5" N"7" N"9".
       01  EOA                          PIC G. 

       01  W9-COUNT                     LIKE AA-INTEGER.
      *11 Begin Bug0803
       01  wx-lang-code                 PIC G(3)       VALUE N"en".  
      *11 End   Bug0803
      ****************************************************************
      *                                                              *
      *        Linkage SECTION                                       *
      *                                                              *
      ****************************************************************
       COPY "SCWNODE.cpy".   
       LINKAGE SECTION.
       
      *** Penalty box linkage area
       COPY "SCWPBOXL.cpy".
       
      *** Standard error return code
       COPY "SCWSQLEL.cpy".
                                                  
       PROCEDURE DIVISION USING PBOX-PARMS, ERR-RETURN-CODE.
      ********************
       0000-PBOX-MAINLINE SECTION.
       0000-PBOX-MAINLINE-START.
                                        
               
         INVOKE "com.aquitecintl.ewms.common.facades.DateService"
            "getInstance"               RETURNING INT-VAL
           ON EXCEPTION
               MOVE "INVK0002"         TO ERR-RTN-CODE           
               MOVE "getInstance"      TO WX-ERROR-METHOD
               PERFORM 9990-PBOX-TERMINATE-PROGRAM
           END-INVOKE.  
            
      ******************   
      
      *     INITIALIZE ERR-RETURN-CODE.
           MOVE N"0000000000"                 TO ERR-RTN-CODE.
           MOVE N"N"                    TO ERR-RTN-POISON.

      * Create a connetion to DB2          
           Copy "SCWCNCTP.cpy".
           Copy "SCWCNTDB.cpy".
           
           
           EXEC SQL
               SELECT coalesce(debug_field,'N')
                INTO :wf-debug
                FROM scst_module_hdr
               WHERE module_name = 'SCWPBOX'
           END-EXEC.
           IF WF-DEBUG                  = N"Y" OR N"P"
              DISPLAY "******************************************"
              DISPLAY "* SCWPBOX  STARTED                       *"
              DISPLAY "******************************************"
           END-IF.   

      * Get user's language.  If there isn't one, default max from db
            EXEC SQL
              Select default_lang_code
                into :wx-lang-code
                from users
               where username = :node-user
            END-EXEC.
            Evaluate sqlcode
               When 0
                  Continue
               When other
                 EXEC SQL
                   Select max(default_lang_code)
                     into :wx-lang-code
                     from users
                 END-EXEC 
            End-evaluate.

      *& begin 06-1398      
      *&06 IF pbox-area-type NOT = N"PO" AND N"PB" AND N"LO" AND N"WA"
           IF pbox-area-type NOT = N"PO" AND N"PB" AND N"LO" AND N"WA" 
                                         AND N"PH"
      *& end   06-1398      
              EXEC sql 
                  SELECT  COUNT(*)
                    INTO :w9-count
                    FROM  scct_validvaldsc     
                   WHERE  cpy_cd        = :pbox-cpy-cd
                     AND  vvd_code      = 'WAR'
                     AND  vvd_type      = :pbox-area-type
      *11 Begin Bug0803
                     AND language_code  = :wx-lang-code
      *11 End   Bug0803
              END-EXEC             

              EVALUATE SQLCODE
                WHEN 0
                     IF W9-COUNT        > 0 
                        CONTINUE
                     ELSE
                        MOVE N"PBOX1001" TO ERR-RTN-CODE
                        PERFORM 9100-ERR-PROCESS
                     END-IF
                WHEN OTHER
                     MOVE N"PBOX1003"   TO ERR-RTN-CODE
                     PERFORM 9100-ERR-PROCESS
              END-EVALUATE
           END-IF.   

           MOVE N"N"         TO PBOX-PBOX.   
           ADD 1               TO WF-CHOICE.
           
           PERFORM 2000-PROCESS-PBOX UNTIL WF-CHOICE > 5 
                                        OR PBOX-PBOX NOT = N"N".
      
           PERFORM 9990-PBOX-TERMINATE-PROGRAM.

       0000-PBOX-MAINLINE-EXIT.
           EXIT.
      
      ********************           
      
       2000-PROCESS-PBOX SECTION.
       2000-START.
              
           EVALUATE WF-CHOICE
               WHEN 1          
      ****************************************************************
      *  1st Choice: Record where the slot type and configuration 
      *              matches those passed.
      *  
      *                  odd_or_even = :pbox-slot-type
      *              and slot_config = :pbox-slot-config.  
      ****************************************************************
      
                  MOVE pbox-slot-type   TO  WF-SLOT-TYPE
                  MOVE pbox-slot-config TO  WF-SLOT-CONFIG
                  PERFORM 2100-GET-AREA
      
              WHEN 2
      ****************************************************************
      *  2nd Choice: Record where the slot type matches the value passed
      *              and configuration is spaces
      *
      *                  odd_or_even = :pbox-slot-type
      *              and slot_config = SPACES
      ****************************************************************
      
                  MOVE pbox-slot-type   TO  WF-SLOT-TYPE
                  MOVE N" "         TO  WF-SLOT-CONFIG
                  PERFORM 2100-GET-AREA
                  
              WHEN 3
      ****************************************************************
      *  3rd Choice: Record where the slot type is spaces and the 
      *              configuration matches values passed
      *
      *                  odd_or_even = SPACES
      *              and slot_config = :pbox-slot-config.  
      ****************************************************************
      
                  MOVE N" "           TO  WF-SLOT-TYPE
                  MOVE pbox-slot-config TO  WF-SLOT-CONFIG
                  PERFORM 2100-GET-AREA
      
              WHEN 4
      ****************************************************************
      *  4th Choice: Record where the slot type and configuration 
      *              are spaces. 
      *
      *                  odd_or_even = SPACES
      *              and slot_config = SPACES
      ****************************************************************
      
                  MOVE N" "           TO  WF-SLOT-TYPE
                  MOVE N" "           TO  WF-SLOT-CONFIG
                  PERFORM 2100-GET-AREA
      
              WHEN OTHER    
      ****************************************************************
      *  No record were found - Default overflow value.
      ****************************************************************
      *& begin 06-1398      
      *&          MOVE N"PBGEN"         TO  PBOX-PBOX
                  If pbox-area-type = N"PH"
                     MOVE N"PBGENH"        TO  PBOX-PBOX
                  Else
                     MOVE N"PBGEN"         TO  PBOX-PBOX                  
                  End-if
      *& end   06-1398      
           END-EVALUATE.
           
           ADD 1                       TO WF-CHOICE.
      
       2000-EXIT.
            EXIT.
      
      
       2100-GET-AREA SECTION.
       2100-START.
      
           IF WF-DEBUG                  = N"Y"
              DISPLAY "WF-CHOICE      : " WF-CHOICE 
              DISPLAY "WF-SLOT-TYPE   : " WF-SLOT-TYPE 
              DISPLAY "WF-SLOT-CONFIG : " WF-SLOT-CONFIG
              DISPLAY " "
           END-IF   
      
      * First get location's aisle/bay/slot
           EXEC SQL
                SELECT aisle, bay, slot_code 
                INTO :aisle
                    ,:bay
                    ,:slot-code
                FROM scwt_location
                WHERE cpy_cd            = :pbox-cpy-cd
                AND  location           = :pbox-location
                AND  loc_facl           = :pbox-facl
                AND  loc_whse           = :pbox-whse 
                AND  slot_type          = :WF-SLOT-TYPE 
                AND  slot_config        = :WF-SLOT-CONFIG
           END-EXEC.
      
           IF  SQLCODE = 100 or 0
               CONTINUE
           ELSE
               MOVE N"PBOX9000"         TO ERR-RTN-CODE 
               PERFORM 9100-ERR-PROCESS
           END-IF.
      
      
           IF WF-DEBUG                  = N"Y"
              DISPLAY "AISLE    : " aisle 
              DISPLAY "BAY      : " bay 
              DISPLAY "SLOT CODE: " SLOT-CODE
              DISPLAY " "
           END-IF   
           
      * Determine if target slot is Even or Odd 
           Move bay                    TO w9-bay
           Move wx-bay(3:1)            TO Even-Or-Odd.
           If Even-Side
              MOVE N"E" to EOA
           Else
              MOVE N"O" to EOA
           End-if.
       
           EXEC SQL
               SELECT  area_code
               INTO   :area-code
               FROM    scwt_area 
               WHERE   cpy_cd           =  :pbox-cpy-cd
               AND     facl             =  :pbox-facl
               AND     whse             =  :pbox-whse
               AND     area_type        =  :pbox-area-type 
               AND     high_location    >= :pbox-location
               AND     low_location     <= :pbox-location                        
               AND     record_status    =  'A'   
               AND     odd_or_even      in (:EOA, 'A')
               AND     slot_config      =  :WF-SLOT-CONFIG
           END-EXEC.  
      
           IF WF-DEBUG                  = N"Y"
              DISPLAY "SQLSTATE    : " SQLSTATE " /" SQLCODE
              DISPLAY "AREA-CODE   : " AREA-CODE
              DISPLAY " "
           END-IF   
      
           EVALUATE SQLCODE
           WHEN 0
              MOVE AREA-CODE           TO  PBOX-PBOX
           WHEN 100  
              CONTINUE
           WHEN OTHER
              MOVE N"PBOX1002"          TO  ERR-RTN-CODE
              PERFORM 9100-ERR-PROCESS
           END-EVALUATE.
      
       2100-EXIT.
            EXIT.
      
      * ================================================================
       9100-ERR-PROCESS SECTION.
       9100-START.
           If wf-debug = N"Y"
             Display "9100-Err-Process"
             Display "Err-rtn-code: " Err-Rtn-Code
           End-if.
           invoke int-val "expandString"
                   using sqlstate 
                   returning wg-sqlstate
       
            STRING N"ST:"
                   wg-SQLSTATE
                   N"/",
                   pbox-cpy-cd
                   N"/"
                   pbox-facl
                   N"/"
                   pbox-whse
                   N"/"
                   pbox-location
                   N"/"
                   pbox-slot-type
                   N"/"
                   pbox-slot-config
                   N"/"
                   pbox-area-type
                                      DELIMITED BY SIZE
                                      INTO ERR-RTN-DATA.
           MOVE N"Y"              TO ERR-RTN-POISON.
           MOVE SQLCODE          TO ERR-SQLCODE.
           MOVE SQLSTATE         TO ERR-SQLSTATE.

           IF ERR-RTN-CODE NOT = N"0000000000"  
              MOVE ERR-RTN-CODE        TO NODE-RESULT-FIELD 
              MOVE ERR-RTN-DATA        TO NODE-ERROR-MESSAGE-DATA
              MOVE ERR-RTN-POISON      TO NODE-POISON-MESSAGE-FLAG
           END-IF.   
 
           PERFORM 9990-PBOX-TERMINATE-PROGRAM.

       9100-EXIT.
            EXIT.

      
      *=========================================================================
      * This section terminates and exits the program.                         *
      * This condition will be executed normally or if an error has occured    *
      * and processing needs to be terminated without finishing the program.   *
      *=========================================================================
       9990-PBOX-TERMINATE-PROGRAM SECTION.
       9990-PBOX-TERMINATE-START.
           IF WF-DEBUG                  = N"Y" OR N"P"
              DISPLAY "******************************************"
              DISPLAY "* SCWPBOX  GOBACK                        *"
              DISPLAY "******************************************"
           END-IF.

           GOBACK.

            
      ******************************************************************
      ******************************************************************
