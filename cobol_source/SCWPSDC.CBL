       IDENTIFICATION DIVISION.
       PROGRAM-ID. SCWPSDC.
      ******************************************************************
      *                                                                *
      *  Copyright 2002 Aquitec International, Inc.                    *
      *  All rights reserved                                           *
      * ---------------------------------------------------------------*
      *  Version 1  Release  0                                         *
      *                                                                *
      ******************************************************************
      *
      *================================================================* 
      * PROGRAM/MODULE  NAME........SCWPSDC                            *
      *                                                                *
      *                Planned Shipment Drop Creation                  *
      *                                                                *
      * DESIGNED BY.........Aquitec Inc.                               *
      * PROGRAMMED BY.......Aquitec Inc.                               *
      * Date coded..........08 NOVEMBER 2002                           *
      * System..............eWarehousing                               *
      * Frequency used......ON REQUEST                                 *
      *                                                                *
      * Input Tables.                                                  *
      * ------------                                                   *
      * SCWT_SHIP_SCHEDULE                                             *
      * SCWT_PBL_DROPS                                                 *
      *                                                                *
      * I-O Tables.                                                    *
      * -----------                                                    *
      * SCWT_PLAN_SHIP_DRP                                             *
      *                                                                *
      *                                                                *
      * Processing/Description                                         *
      * ----------------------                                         *
      *  This program inserts a record into the SCWT_PLAN_SHIP_DRP     *
      *  table for values passed in linkage.                           * 
      *                                                                *
      ******************************************************************
      *
      *================================================================* 
      *                   PROGRAM CHANGE HISTORY                       * 
      *----------------------------------------------------------------* 
      *  CHANGE �CHANGE �                                              * 
      *   DATE  �NUMBER �      DETAILED CHANGE DESCRIPTION             * 
      *----------------------------------------------------------------*  
      * 08NOV02 |..-....|  NEW Program                                 *   
      *================================================================*
       
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SOURCE-COMPUTER. IBM-PC.
       OBJECT-COMPUTER. IBM-PC.
       SPECIAL-NAMES.   CONSOLE IS SYSLOG.
      
       DATA DIVISION.
      **************************************************************************
      *        General Working-Storage                                         *
      **************************************************************************
      *                                                                        *
      *  PREFIXES:      WX- Character                                          *
      *                 W9- Packed Numeric field                               *
      *                 WZ- Zoned Numeric field                                *
      *                 WF- Flags                                              *
      *                 WPH- Print Headers                                     *
      *                 WPD- Print Detail                                      *
      *                 WPT- Print Totals                                      *
      *                 WS- Other.                                             *
      *                                                                        *
      **************************************************************************
      **************************************************************************
       WORKING-STORAGE SECTION.
      **************************************************************************
      *   General copybook that holds field sizes.                             *
      **************************************************************************
            COPY "SCWAAAW.cpy".              
      
      **************************************************************************
      *  FLAGS / Switches                                                      *
       
      **************************************************************************
       01  WF-DEBUG                     PIC G VALUE N"Y"
           GET-PROPERTY SET-PROPERTY.
       01  WF-ERROR-FOUND               PIC G VALUE N"N".
      
      **************************************************************************
      *   General WORKING STORAGE fields                                       *
       
      **************************************************************************
      
      ** CDIDBCS ** 
       01  INT-VAL    object reference 
          "com.aquitecintl.ewms.common.facades.DateService". 
       01  WX-ERROR-METHOD              LIKE AA-INVOKE-ERROR .
      ****************************************************************       
      
      
      
      **************************************************************************
      *   Other General Copybooks                                              *
       
      **************************************************************************
      *  Java Fields and Node copybooks                                         
      *      
         COPY "SCWNODE.cpy".
      
      **************************************************************************
      * Table Copybooks                                                        *
      **************************************************************************
      
      
      
      **************************************************************************
      * Called Pgm Copybooks                                                   *
      **************************************************************************
      *  Standard Date copybook
         COPY "SCWDATEL.cpy".
      
      **************************************************************************
      * Procedure Division Routine Copybooks                                   *
      **************************************************************************
       
       
      
      
      
      **************************************************************************
      *        Database Tables                                                 *
       
      **************************************************************************
      * SQL Declare Statements                                                  
            EXEC SQL        INCLUDE SQLCA.cpy    END-EXEC.   
      
      * Planned Shipment Drop table                                            
        COPY "SCWPSHDD.cpy". 
      * Shipment Schedule table                                        
        COPY "SCWSHSCD.cpy".
      * Pick By Line Drop Plan table
        COPY "SCWPBDRD.cpy".
      
      **************************************************************************
      *        Linkage Section                                                 *
       
      **************************************************************************
      **************************************************************************
         
       LINKAGE SECTION.
      * Start Work Assignment Parameters                                      
       COPY "SCWPSDCL.cpy".
      * Standard error return code                                              
       COPY "SCWSQLEL.cpy".
        
      *=========================================================================
      *                                                                        *
      * P R O C E D U R E   D I V I S I O N.                                   *
      *                                                                        *
      *=========================================================================
       PROCEDURE DIVISION USING PSDC-PARMS, ERR-RETURN-CODE.
      * 
       0000-PSDC-MAINLINE SECTION.
       0000-PSDC-MAINLINE-START.
           IF WF-DEBUG                  = N"Y" OR N"P"
              DISPLAY "******************************************"
              DISPLAY "* SCWPSDC  STARTED                       *"
              DISPLAY "******************************************"
           END-IF.
      *****************                                  
                                        
               
         INVOKE "com.aquitecintl.ewms.common.facades.DateService"
            "getInstance"               RETURNING INT-VAL
           ON EXCEPTION
               MOVE "INVK0002"         TO ERR-RTN-CODE           
               MOVE "getInstance"      TO WX-ERROR-METHOD
               PERFORM 9990-PSDC-TERMINATE-PROGRAM
           END-INVOKE.  
            
      ******************   
      
          IF WF-DEBUG           = N"T"
             PERFORM 9991-SETUP-TESTDATA
          END-IF. 
      
      *** Initialise section
           PERFORM 1000-INITIALIZE.           
      
          IF WF-DEBUG           = N"T"
             MOVE N"Y"          TO WF-DEBUG
          END-IF. 
      
      *** Normal processing until end of loop  
           PERFORM 2000-PROCESS.           
      
      *     IF WF-DEBUG = N"Y"
      *         EXEC SQL
      *             COMMIT WORK
      *         END-EXEC    
      *     END-IF
      
      *** Termination section
           PERFORM 9990-PSDC-TERMINATE-PROGRAM.           
      
       0000-PSDC-MAINLINE-EXIT.
            EXIT
            
      *=========================================================================
      * This section clears all variable values and check if a connection to   *
      * the database exists. If no connection exist, then a connection will be *
      * made.                                                                  *
      *=========================================================================
       1000-INITIALIZE SECTION.
       1000-INITIALIZE-START.
       
      *     INITIALIZE ERR-RETURN-CODE.     
          MOVE N"0000000000" TO ERR-RTN-CODE.     
       
      * Create a connetion to DB2          
           Copy "SCWCNCTP.cpy".
           Copy "SCWCNTDB.cpy".
      
           IF  PSDC-PARMS = SPACES OR N" " OR " "
               MOVE N"PSDC1001"           TO ERR-RTN-CODE
               PERFORM 9999-ERROR-HANDLE           
           END-IF.     
       
       1000-INITIALIZE-EXIT.
            EXIT.
            
      *=========================================================================
      *                                                                        *
      *   2000-7999 = PROCESSING paragraphs                                    *
      *   8000-8999 = STANDARD I/O paragraphs                                  *
      *                                                                        *
      *=========================================================================
       2000-PROCESS SECTION.
       2000-PROCESS-START.
       
      ******************************************************************
      *  Find the day number for the SHIP_DATE passed as a parameter
      ******************************************************************
           INITIALIZE DATE-PARMS.
           MOVE PSDC-SHIP-DATE         TO DATE-USER.
           CALL "SCWDATE"               USING DATE-PARMS ERR-RETURN-CODE
           ON OVERFLOW
               MOVE  N"PSDC1005"    TO ERR-RTN-CODE
               MOVE N"Y"            TO ERR-RTN-POISON
               PERFORM 9999-ERROR-HANDLE
           END-CALL.
           CANCEL "SCWDATE".
           IF  ERR-RTN-CODE            NOT = SPACES AND N" " AND " "
                           AND N"0000000000" 
              PERFORM 9999-ERROR-HANDLE
           END-IF.
      
           IF WF-DEBUG = N"Y"
              DISPLAY "Day Number : " DATE-DOW
           END-IF.      
      
      ******************************************************************
      *  Find the current active ship plan by locating the row in the 
      *  SCWT_SHIP_SCHEDULE table with the greatest date that is less than
      *  or equal to the ship_date passed as a parameter.
      ******************************************************************
           IF WF-DEBUG          = N"Y"
              DISPLAY "Cpy cd        : " PSDC-cpy-cd
              DISPLAY "Facl          : " PSDC-facl
              DISPLAY "Whse          : " PSDC-whse
              DISPLAY "Ship date     : " PSDC-ship-date
           END-IF  
      
           EXEC sql
               SELECT a.start_date
                     ,a.ship_plan
                INTO :scwt-ship-schedule.start-date
                    ,:scwt-ship-schedule.ship-plan
                FROM  scwt_ship_schedule a
                WHERE a.cpy_cd              = :PSDC-cpy-cd
                AND   a.facl                = :PSDC-facl
                AND   a.whse                = :PSDC-whse
                AND   a.start_date =
                        (SELECT max(b.start_date)
                           FROM scwt_ship_schedule b
                          WHERE b.cpy_cd      = a.cpy_cd
                            AND   b.facl        = a.facl
                            AND   b.whse        = a.whse 
                            AND   b.start_date <= :PSDC-ship-date)
           END-EXEC.
      
      *******CDIDBCS-START-ECOBOL1**********
        INVOKE INT-VAL "expandString"
                               USING  START-DATE OF SCWT-SHIP-SCHEDULE
                           RETURNING  START-DATE OF SCWT-SHIP-SCHEDULE
      *******CDIDBCS-END-ECOBOL1**********
      ******************************************************************
      * If a row is not found default to "NRM"
      ******************************************************************
      
           EVALUATE SQLCODE
           WHEN 0
              CONTINUE
           WHEN 100
              MOVE N"NRM"               TO SHIP-PLAN 
                                           OF scwt_ship_schedule
           WHEN OTHER     
              if wf-debug = N"Y"
                 display "PSDC1002"
                 display "sqlcode= " sqlcode
                 display "sqlstate= " sqlstate
              end-if      
              MOVE N"PSDC1002"          TO ERR-RTN-CODE
              MOVE N"Y"                 TO ERR-RTN-POISON
              PERFORM 9999-ERROR-HANDLE           
           END-EVALUATE.
      
      ******************************************************************
      *  Locate the record in the SCWT_PBL_DROPS table 
      ******************************************************************
      
           IF WF-DEBUG = N"Y"
              DISPLAY "Cpy cd        : " PSDC-cpy-cd
              DISPLAY "Facl          : " PSDC-facl
              DISPLAY "Whse          : " PSDC-whse
              DISPLAY "Trader code   : " PSDC-trader-code
              DISPLAY "Trader type   : " PSDC-trader-type
              DISPLAY "Plan ship     : " ship-plan OF scwt-ship-schedule
              DISPLAY "Delivery_day  : " date-dow
              DISPLAY "Whse_sect     : " PSDC-whse-sect
           END-IF
           
           EXEC sql
               SELECT  drop_point
                 INTO :scwt-pbl-drops.drop-point
                 FROM  scwt_pbl_drops
                 WHERE cpy_cd              = :PSDC-cpy-cd
                 AND   facl                = :PSDC-facl
                 AND   whse                = :PSDC-whse
                 AND   trader_code         = :PSDC-trader-code
                 AND   trader_type         = :PSDC-trader-type
                 AND   ship_plan           =
                                           :scwt-ship-schedule.ship-plan
                 AND   delivery_day        = :date-dow
                 AND   whse_sect           = :PSDC-whse-sect
           END-EXEC.
      
      ******************************************************************
      *  If a row is not found default the drop_point to all 9's
      ******************************************************************
      
           DISPLAY "SQLCODE FOR PLB DROP IN PSDC " SQLCODE.
           EVALUATE SQLCODE
           WHEN 0
              CONTINUE
           WHEN 100
              MOVE N"99999999"          TO DROP-POINT OF scwt_pbl_drops
           WHEN OTHER           
              MOVE N"PSDC1003"          TO ERR-RTN-CODE
              MOVE N"Y"                 TO ERR-RTN-POISON
              PERFORM 9999-ERROR-HANDLE           
           END-EVALUATE.
      
      ******************************************************************
      *  Insert a row in the SCWT_PLAN_SHIP_DRP table.
      ******************************************************************
      
           EXEC SQL
               INSERT INTO scwt_plan_ship_drp
                      (  cpy_cd
                        ,facl
                        ,whse
                        ,trader_code
                        ,trader_type
                        ,ship_date
                        ,whse_sect
                        ,drop_point
                        ,curr_container
                        ,curr_weight
                        ,curr_cube
                        ,update_serial
                        ,maint_date
                        ,maint_time
                        ,maint_user
                        ,maint_tran
                        )
            VALUES 
                     (  :PSDC-cpy-cd
                       ,:PSDC-facl
                       ,:PSDC-whse
                       ,:PSDC-trader-code
                       ,:PSDC-trader-type
                       ,:PSDC-ship-date
                       ,:PSDC-whse-sect
                       ,:scwt-pbl-drops.drop-point
                       ,0
                       ,0
                       ,0
                       ,1
                       ,current date
                       ,current time
                       ,:PSDC-user
                       ,'SCWPSDC' )
           END-EXEC.
      
      ******************************************************************
      *  If a duplicate key - Continue.
      ******************************************************************
      
           IF SQLSTATE = "23505" 
              MOVE 0                   TO  SQLCODE
           END-IF.   
      
           EVALUATE SQLCODE
           WHEN 0
              CONTINUE
           WHEN OTHER           
              MOVE N"PSDC1004"          TO ERR-RTN-CODE
              MOVE N"Y"                 TO ERR-RTN-POISON
              PERFORM 9999-ERROR-HANDLE           
           END-EVALUATE.
      
       2000-PROCESS-EXIT.
            EXIT.        
      
      *=========================================================================
      * This section moves a description to the error message field sothat     *
      * the fronend can display and/or handle the error that occured.          *
      *=========================================================================
       9999-ERROR-HANDLE SECTION.
       9999-ERROR-HANDLE-START.
           
           MOVE SPACES                 TO ERR-RTN-DATA.
           invoke int-val "expandString"
                   using sqlstate 
                   returning wg-sqlstate
           
           EVALUATE ERR-RTN-CODE
           WHEN N"INVK0001"
           WHEN N"INVK0002"
           WHEN N"INVK0003"
           WHEN N"INVK0004"
                MOVE WX-ERROR-METHOD   TO ERR-RTN-DATA           
           WHEN OTHER   
                STRING N"ST:"
                       wg-SQLSTATE
                       N" /"
                       PSDC-CPY-CD,
                       N"/",
                       PSDC-FACL,
                       N"/",
                       PSDC-WHSE,
                       N"/",
                       PSDC-TRADER-CODE,
                                        DELIMITED BY SIZE 
                                        INTO ERR-RTN-DATA       
                END-STRING
                MOVE N"Y"               TO ERR-RTN-POISON
                MOVE SQLCODE           TO ERR-SQLCODE
                MOVE SQLSTATE          TO ERR-SQLSTATE
           END-EVALUATE.
      
           IF WF-DEBUG                  = N"Y" 
              DISPLAY " 9999-ERROR : " ERR-RETURN-CODE
           END-IF.    
      
           PERFORM 9990-PSDC-TERMINATE-PROGRAM.
                   
       9999-ERROR-HANDLE-EXIT.
           EXIT. 
                                                      
      
      *=========================================================================
      * This section terminates and exits the program.                         *
      * This condition will be executed normally or if an error has occured    *
      * and processing needs to be terminated without finishing the program.   *
      *=========================================================================
       9990-PSDC-TERMINATE-PROGRAM SECTION.
       9990-PSDC-TERMINATE-START.
           IF WF-DEBUG                  = N"Y" OR N"P"
              DISPLAY "******************************************"
              DISPLAY "* SCWPSDC  GOBACK                        *"
              DISPLAY "******************************************"
           END-IF.

           IF WF-DEBUG                  = N"Y"
              DISPLAY "POISON   : " ERR-RTN-POISON
              DISPLAY "ERR CODE : " ERR-RTN-CODE
              DISPLAY "ERR DATA : " ERR-RTN-DATA
              DISPLAY "SQLCODE  : " ERR-SQLCODE
              DISPLAY "SQLSTATE : " ERR-SQLSTATE
           END-IF.
       
       9990-PSDC-TERMINATE-GOBACK.
           GOBACK.
           
           
      *=========================================================================
      * This section sets up any testdata required to test the program logic.  *
      * Normally this is the input required by program to start.               *
      *=========================================================================
       9991-SETUP-TESTDATA SECTION.
       9991-SETUP-TESTDATA-START.
      
           MOVE N"WILLI"         TO PSDC-CPY-CD.
           MOVE N"1"             TO PSDC-FACL.
           MOVE N"01"            TO PSDC-WHSE.
           MOVE N"TRADER CO"     TO PSDC-TRADER-CODE.
           MOVE N"S"             TO PSDC-TRADER-TYPE.
           MOVE N"2002-11-14"    TO PSDC-SHIP-DATE.
           MOVE N"AA"            TO PSDC-WHSE-SECT.
           MOVE N"SCWPSDC"       TO PSDC-TRANSACTION.
           MOVE N"willie"        TO PSDC-USER.
       
       9991-SETUP-TESTDATA-EXIT.
            EXIT.
      
      
      
      ******************************************************************
      *  DataBase Lock check Copybook
        COPY "SCWDBSTP.cpy".
      ******************************************************************
