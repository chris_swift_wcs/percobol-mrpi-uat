       IDENTIFICATION DIVISION.
       PROGRAM-ID. SCWRAN1.
      ******************************************************************
      *                                                                *
      *  Copyright 2002 Aquitec International, Inc.                    *
      *  All rights reserved                                           *
      * ---------------------------------------------------------------*
      *  Version 1  Release  0                                         *
      *                                                                *
      ******************************************************************
      *
      *================================================================* 
      * PROGRAM/MODULE  NAME........SCWRANG                            *
      *                                                                *
      *                 Customer Item Ranging                          *
      *                                                                *
      * DESIGNED BY.........Aquitec Inc.                               *
      * PROGRAMMED BY.......Aquitec Inc.                               *
      * Date coded..........4 NOVEMBER 2002                            *
      * System..............eWarehousing                               *
      * Frequency used......ON REQUEST                                 *
      *                                                                *
      * Input Tables.                                                  *
      * ------------                                                   *
      *  SCWT_CUST_RANGE                                               *
      *  SCWT_ITEM                                                     *
      *  SCWT_CUSTOMER                                                 *
      *                                                                *
      * I-O Tables.                                                    *
      * ----------                                                     *
      *                                                                *
      *  Processing/Description                                        *
      *  ----------------------                                        *
      *  This program validates if a customer can order a specific     *
      *  item.                                                         * 
      *                                                                *
      ******************************************************************
      *
      *================================================================* 
      *                   PROGRAM CHANGE HISTORY                       * 
      *----------------------------------------------------------------* 
      *  CHANGE �CHANGE �                                              * 
      *   DATE  �NUMBER �      DETAILED CHANGE DESCRIPTION             * 
      *----------------------------------------------------------------*  
      * 04NOV02 |..-....|  NEW Program                                 *   
      *================================================================*
      *
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SOURCE-COMPUTER. IBM-PC.
       OBJECT-COMPUTER. IBM-PC.
       SPECIAL-NAMES.   CONSOLE IS SYSLOG.
      *
       DATA DIVISION.
       FILE SECTION.
      *
       WORKING-STORAGE SECTION.
      *------------------------
       COPY "SCWAAAW.cpy".
      ** CDIDBCS ** 
       01  INT-VAL    object reference 
          "com.aquitecintl.ewms.common.facades.DateService". 

      **************************************************************** 
      *
       01  WF-DEBUG                     PIC G         VALUE N"Y"
           get-property set-property.
      * 
       01  WX-ERROR-METHOD              LIKE AA-INVOKE-ERROR.
       01  WX-CURRENT-PARAGRAPH         PIC G(30)     VALUE " ".
       01  WX-CURRENT-SQL-STMT          PIC G(2).
       01  W9-CURRENT-SQL-STMT REDEFINES WX-CURRENT-SQL-STMT
                                        PIC 99        VALUE 0.
      *
       01  WX-VARIABLES.        
      *** Saved commodity and sub commodity from SCWT_ITEM 
           05  W9-COMMODITY             LIKE AA-INTEGER.
           05  W9-SUB-COMM              LIKE AA-INTEGER.
      *** Save group range from SCWT_CUSTOMER     
           05  WX-GROUP-RANGE           PIC G(3).
           05  WF-ALLOW                 PIC G         VALUE SPACES.
           05  wx-trader-is-cust        pic g.
      *   
      **************************************************************************
      * D a t a b a s e     T a b l e s                                        * 
      **************************************************************************
      **************************************************************************   
      * SQLCA                                                                  * 
      **************************************************************************
       EXEC SQL INCLUDE SQLCA.cpy END-EXEC.
      * 
      **************************************************************************
      * Customer Ranging Information Table                                     * 
      **************************************************************************
       COPY "SCWCURAD.cpy". 
      *
      **************************************************************************
      * Item Table                                                             * 
      **************************************************************************
       COPY "SCWITEMD.cpy". 
      *
      **************************************************************************
      * Customer Table                                                         * 
      **************************************************************************
       COPY "SCWCUSTD.cpy". 
      *
      **************************************************************************
      * O t h e r  w o r k i n g  s t o r a g e  c o p y b o o k s             * 
      **************************************************************************
      *
      **************************************************************************
      * Node copybook.                                                         * 
      **************************************************************************
       COPY "SCWNODE.cpy".
      *
       LINKAGE SECTION.
      *---------------- 
       COPY "SCWRANGL.cpy". 
      **************************************************************************
      * Standard error return code                                             * 
      **************************************************************************
       COPY "SCWSQLEL.cpy".
      *  
      *
      **************************************************************************
      **************************************************************************
      *                                                                      ***
      * P R O C E D U R E   D I V I S I O N.                                 ***
      *                                                                      ***
      **************************************************************************
      **************************************************************************      
      *
       PROCEDURE DIVISION USING RANG-PARMS, ERR-RETURN-CODE.
      *----------------------------------------------------
      * 
       0000-RANG-MAIN SECTION.
       0000-RANG-START.
           display "***************"
           display "SCWRANG STARTED" 
           display "***************"
                      
           MOVE N"0000-RANG-MAIN SECTION" TO WX-CURRENT-PARAGRAPH.
           if wf-debug = N"Y" 
              display "SCWRANG: " 
                      WX-CURRENT-PARAGRAPH
           end-if           
      *
           PERFORM 1000-INITIALIZE.
      *
      *** Find the commodity and sub commodity for the item
           PERFORM 2000-GET-COMMS.   
      *
      *** Find ranging group for the customer
           move N"Y" to wx-trader-is-cust
           PERFORM 3000-GET-CUST-RANGE-GROUP.       
      *
      *** Check SCWT-CUST-RANGE
           if wf-debug = N"Y" 
              display "SCWRANG: wx-trader-is-cust ="
                      wx-trader-is-cust
              display "SCWRANG: wf-allow = "        
                      wf-allow
           end-if           
                                    
           if wx-trader-is-cust = N"Y"
              PERFORM 4000-CHECK-CUST-RANGE
           else 
              if wf-debug = N"Y" 
              display "SCWRANG: wx-trader-is-cust ="
                      wx-trader-is-cust
              display "SCWRANG: wf-allow = "        
                      wf-allow
             end-if     
             move N"N" to wf-allow
           end-if 
      *
           IF  WF-DEBUG = N"Y"
               DISPLAY "WF-ALLOW     : " WF-ALLOW
           END-IF.    
      * 
      *****************                                  
                                        
               
         INVOKE "com.aquitecintl.ewms.common.facades.DateService"
            "getInstance"               RETURNING INT-VAL
           ON EXCEPTION
               MOVE "INVK0002"         TO ERR-RTN-CODE           
               MOVE "getInstance"      TO WX-ERROR-METHOD
              PERFORM 9600-ERROR-AND-EXIT
           END-INVOKE.  
            
      ******************   
           IF  WF-ALLOW = N"Y"
               MOVE N"N"                TO RANG-ALLOW
           ELSE
               MOVE N"Y"                TO RANG-ALLOW
           END-IF.
      *
       0000-RANG-EXIT.
           PERFORM 9999-RANG-ENDIT.
      *
      *------------------------------------------------------------------------*
       1000-INITIALIZE SECTION.
       1000-INITIALIZE-START.
           MOVE N"1000-INITIALIZE"      TO WX-CURRENT-PARAGRAPH.
           if wf-debug = N"Y" 
              display "SCWRANG: " 
                      WX-CURRENT-PARAGRAPH
           end-if           

      *
      *     INITIALIZE ERR-RETURN-CODE.     
      *     MOVE ZEROES                 TO ERR-RTN-CODE.     
      * 
      *** Create a connetion to DB2          
           Copy "SCWCNCTP.cpy".
           Copy "SCWCNTDB.cpy".
      *
      *
      *
      *** Initialise working storage variables.
           INITIALIZE WX-VARIABLES.
      *
       1000-EXIT.
           EXIT.
      *
      *------------------------------------------------------------------------*      
       2000-GET-COMMS SECTION.
       2000-GET-COMMS-START. 
           MOVE N"2000-GET-COMMS"       TO WX-CURRENT-PARAGRAPH.                 
           if wf-debug = N"Y" 
              display "SCWRANG: " 
                      WX-CURRENT-PARAGRAPH
           end-if           

      * 
           EXEC sql
               SELECT  commodity
                      ,sub_comm                     
                 INTO :w9-commodity
                     ,:w9-sub-comm
                 FROM  scwt_item
                WHERE  cpy_cd           = :RANG-CPY-CD
                  AND  item_facl        = :RANG-item-facl
                  AND  item_whse        = :RANG-item-whse
                  AND  item_owner       = :RANG-item-owner
                  AND  item_code        = :RANG-item-code
           END-EXEC.
           PERFORM CB-DATABASE-LOCK-CHECK.   
      *     
           MOVE N"1"                    TO WX-CURRENT-SQL-STMT.
           EVALUATE SQLCODE
             WHEN 0
               CONTINUE
             WHEN OTHER
               MOVE N"RANG1001"         TO ERR-RTN-CODE
               MOVE ZERO                TO w9-commodity
                                           w9-sub-comm
           END-EVALUATE.
      *
       2000-EXIT.
           EXIT.
      *
      *------------------------------------------------------------------------*      
       3000-GET-CUST-RANGE-GROUP SECTION.
       3000-GET-CUST-RANGE-GROUP-START.
           MOVE N"3000-GET-CUST-RANGE-GROUP" TO WX-CURRENT-PARAGRAPH.                 
           if wf-debug = N"Y" 
              display "SCWRANG: " 
                      WX-CURRENT-PARAGRAPH
           end-if           
           if wf-debug = N"Y" 
              display "SCWRANG: RANG-cpy-cd = "
                      RANG-cpy-cd 
              display "SCWRANG: RANG-customer-facl"
                      RANG-customer-facl           
              display "SCWRANG: RANG-customer-whse"
                      RANG-customer-whse
              display "SCWRANG: RANG-customer-code"
                      RANG-customer-code
           end-if

      *  
           EXEC sql
               SELECT  range_group
                 INTO :wx-group-range                  
                 FROM  scwt_customer
                WHERE  cpy_cd           = :RANG-cpy-cd
                  AND  facl             = :RANG-customer-facl
                  AND  whse             = :RANG-customer-whse
                  AND  customer_code    = :RANG-customer-code
           END-EXEC.
           PERFORM CB-DATABASE-LOCK-CHECK.   
      *     
           MOVE N"2"                    TO WX-CURRENT-SQL-STMT.
           EVALUATE SQLCODE
             WHEN 0
               CONTINUE
             WHEN OTHER
               if wf-debug = N"Y"
                  display "SCWRANG: Not on Customer Table"
                  display "SCWRANG: sqlstate = "
                          sqlstate
               end-if           
               MOVE N"N" to wx-trader-is-cust 
      *04      MOVE N"RANG1002"         TO ERR-RTN-CODE
      *04      PERFORM 9500-POISON-ERROR-AND-EXIT    
           END-EVALUATE.
           if wf-debug = N"Y"
              display "SCWRANG: wx-trader-is-cust"
                      wx-trader-is-cust
           end-if.

      *
       3000-EXIT.
           EXIT.
      *
      *------------------------------------------------------------------------*      
       4000-CHECK-CUST-RANGE SECTION.            
       4000-CHECK-CUST-RANGE-START.
           MOVE N"4000-CHECK-CUST-RANGE" TO WX-CURRENT-PARAGRAPH.                 
           if wf-debug = N"Y" 
              display "SCWRANG: " 
                      WX-CURRENT-PARAGRAPH
           end-if           

      *  
           MOVE N"N"                    TO WF-ALLOW.
      *    
           PERFORM 4100-CHECK-SQL1.
           IF  WF-ALLOW = N"Y"
               GO TO 4000-EXIT
           END-IF.
      *    
           PERFORM 4200-CHECK-SQL2.
           IF  WF-ALLOW = N"Y"
               GO TO 4000-EXIT
           END-IF.
      *    
           PERFORM 4300-CHECK-SQL3.
           IF  WF-ALLOW = N"Y"
               GO TO 4000-EXIT
           END-IF.
      *    
           PERFORM 4400-CHECK-SQL4.
           IF  WF-ALLOW = N"Y"
               GO TO 4000-EXIT
           END-IF.
      *    
           PERFORM 4500-CHECK-SQL5.
           IF  WF-ALLOW = N"Y"
               GO TO 4000-EXIT
           END-IF.
      *    
           PERFORM 4600-CHECK-SQL6.
      *    
       4000-EXIT.
           EXIT.
      *
      **----------------------------------------------------------------------**
      ** CUSTOMER_CODE          = RANG-CUSTOMER-CODE                          **
      ** GROUP_RANGE            = SPACES                                      **
      ** COMMODITY              = ZERO                                        **
      ** SUB_COMM               = ZERO                                        **
      ** ITEM_OWNER             = RANG-ITEM-OWNER                             **
      ** ITEM-CODE              = RANG-ITEM-CODE                              **
      **----------------------------------------------------------------------**
       4100-CHECK-SQL1 SECTION.
       4100-CHECK-SQL1-START.
           MOVE N"4100-CHECK-SQL1"      TO WX-CURRENT-PARAGRAPH.                 
           if wf-debug = N"Y" 
              display "SCWRANG: " 
                      WX-CURRENT-PARAGRAPH
           end-if           

      *    
           if wf-debug = N"Y" 
              display "SCWRANG: RANG-cpy-cd = "
                      RANG-cpy-cd 
              display "SCWRANG: RANG-customer-facl"
                      RANG-customer-facl           
              display "SCWRANG: RANG-customer-whse"
                      RANG-customer-whse
              display "SCWRANG: RANG-customer-code"
                      RANG-customer-code
           end-if
                      
           EXEC sql
               SELECT  customer_code
                      ,range_group
                      ,commodity              
                      ,sub_comm
                      ,item_owner
                      ,item_code
                 INTO :scwt-cust-range.customer-code
                     ,:scwt-cust-range.range-group
                     ,:scwt-cust-range.commodity              
                     ,:scwt-cust-range.sub-comm
                     ,:scwt-cust-range.item-owner
                     ,:scwt-cust-range.item-code
                 FROM  scwt_cust_range
                WHERE cpy_cd            = :RANG-cpy-cd
                  AND facl              = :RANG-customer-facl
                  AND whse              = :RANG-customer-whse
                  AND customer_code     = :RANG-customer-code
                  AND range_group       = ' '
                  AND commodity         = 0
                  AND sub_comm          = 0
                  AND item_owner        = :RANG-item-owner
                  AND item_code         = :RANG-item-code
           END-EXEC.
           PERFORM CB-DATABASE-LOCK-CHECK.   
      *
           MOVE N"3"                    TO WX-CURRENT-SQL-STMT.
           PERFORM 9400-EVAL-SQL.
      *    
       4100-EXIT.
           EXIT.
      *
      **----------------------------------------------------------------------**
      ** CUSTOMER_CODE          = RANG-CUSTOMER-CODE                          **
      ** GROUP_RANGE            = SPACES                                      **
      ** COMMODITY              = WX-COMMODITY                                **
      ** SUB_COMM               = WX-SUB-COMM                                 **
      ** ITEM_OWNER             = RANG-ITEM-OWNER                             **
      ** ITEM-CODE              = SPACES                                      **
      **----------------------------------------------------------------------**
       4200-CHECK-SQL2 SECTION.
       4200-CHECK-SQL2-START.
           MOVE N"4200-CHECK-SQL2"      TO WX-CURRENT-PARAGRAPH.                 
           if wf-debug = N"Y" 
              display "SCWRANG: " 
                      WX-CURRENT-PARAGRAPH
           end-if           

      *  
           EXEC sql
               SELECT  customer_code
                      ,range_group
                      ,commodity              
                      ,sub_comm
                      ,item_owner
                      ,item_code
                 INTO :scwt-cust-range.customer-code
                     ,:scwt-cust-range.range-group
                     ,:scwt-cust-range.commodity              
                     ,:scwt-cust-range.sub-comm
                     ,:scwt-cust-range.item-owner
                     ,:scwt-cust-range.item-code
                 FROM  scwt_cust_range
                WHERE cpy_cd            = :RANG-cpy-cd
                  AND facl              = :RANG-customer-facl
                  AND whse              = :RANG-customer-whse
                  AND customer_code     = :RANG-customer-code
                  AND range_group       = ' '
                  AND commodity         = :w9-commodity
                  AND sub_comm          = :w9-sub-comm
                  AND item_owner        = :RANG-item-owner
                  AND item_code         = ' ' 
           END-EXEC.
           PERFORM CB-DATABASE-LOCK-CHECK.   
      *
           MOVE N"4"                    TO WX-CURRENT-SQL-STMT.
           PERFORM 9400-EVAL-SQL.
      *    
       4200-EXIT.
           EXIT.
      *
      **----------------------------------------------------------------------**
      ** CUSTOMER_CODE          = RANG-CUSTOMER-CODE                          **
      ** GROUP_RANGE            = SPACES                                      **
      ** COMMODITY              = WX-COMMODITY                                **
      ** SUB_COMM               = ZERO                                        **
      ** ITEM_OWNER             = RANG-ITEM-OWNER                             **
      ** ITEM-CODE              = SPACES                                      **
      **----------------------------------------------------------------------**
       4300-CHECK-SQL3 SECTION.
       4300-CHECK-SQL3-START.
           MOVE N"4300-CHECK-SQL3"      TO WX-CURRENT-PARAGRAPH.                 
           if wf-debug = N"Y" 
              display "SCWRANG: " 
                      WX-CURRENT-PARAGRAPH
           end-if           

      *  
           EXEC sql
               SELECT  customer_code
                      ,range_group
                      ,commodity              
                      ,sub_comm
                      ,item_owner
                      ,item_code
                 INTO :scwt-cust-range.customer-code
                     ,:scwt-cust-range.range-group
                     ,:scwt-cust-range.commodity              
                     ,:scwt-cust-range.sub-comm
                     ,:scwt-cust-range.item-owner
                     ,:scwt-cust-range.item-code
                 FROM  scwt_cust_range
                WHERE cpy_cd            = :RANG-cpy-cd
                  AND facl              = :RANG-customer-facl
                  AND whse              = :RANG-customer-whse
                  AND customer_code     = :RANG-customer-code
                  AND range_group       = ' ' 
                  AND commodity         = :w9-commodity
                  AND sub_comm          = 0
                  AND item_owner        = :RANG-item-owner
                  AND item_code         = ' '
           END-EXEC.
           PERFORM CB-DATABASE-LOCK-CHECK.   
      *
           MOVE N"5"                    TO WX-CURRENT-SQL-STMT.
           PERFORM 9400-EVAL-SQL.
      *    
       4300-EXIT.
           EXIT.
      *
      **----------------------------------------------------------------------**
      ** CUSTOMER_CODE          = SPACES                                      **
      ** GROUP_RANGE            = WX-GROUP-RANGE                              **
      ** COMMODITY              = ZERO                                        **
      ** SUB_COMM               = ZERO                                        **
      ** ITEM_OWNER             = RANG-ITEM-OWNER                             **
      ** ITEM-CODE              = RANG-ITEM-CODE                              **
      **----------------------------------------------------------------------**
       4400-CHECK-SQL4 SECTION.
       4400-CHECK-SQL4-START.
           MOVE N"4400-CHECK-SQL4"      TO WX-CURRENT-PARAGRAPH.                 
           if wf-debug = N"Y" 
              display "SCWRANG: " 
                      WX-CURRENT-PARAGRAPH
           end-if           

      *  
           EXEC sql
               SELECT  customer_code
                      ,range_group
                      ,commodity              
                      ,sub_comm
                      ,item_owner
                      ,item_code
                 INTO :scwt-cust-range.customer-code
                     ,:scwt-cust-range.range-group
                     ,:scwt-cust-range.commodity              
                     ,:scwt-cust-range.sub-comm
                     ,:scwt-cust-range.item-owner
                     ,:scwt-cust-range.item-code
                 FROM  scwt_cust_range
                WHERE cpy_cd            = :RANG-cpy-cd
                  AND facl              = :RANG-customer-facl
                  AND whse              = :RANG-customer-whse
                  AND customer_code     = ' ' 
                  AND range_group       = :wx-group-range
                  AND commodity         = 0
                  AND sub_comm          = 0
                  AND item_owner        = :RANG-item-owner
                  AND item_code         = :RANG-item-code
           END-EXEC.
           PERFORM CB-DATABASE-LOCK-CHECK.   
      *
           MOVE N"7"                    TO WX-CURRENT-SQL-STMT.
           PERFORM 9400-EVAL-SQL.
      *    
       4400-EXIT.
           EXIT.
      *
      **----------------------------------------------------------------------**
      ** CUSTOMER_CODE          = SPACES                                      **
      ** GROUP_RANGE            = WX-GROUP-RANGE                              **
      ** COMMODITY              = WX-COMMODITY                                **
      ** SUB_COMM               = WX-SUB-COMM                                 **
      ** ITEM_OWNER             = RANG-ITEM-OWNER                             **
      ** ITEM-CODE              = SPACES                                      **
      **----------------------------------------------------------------------**
       4500-CHECK-SQL5 SECTION.
       4500-CHECK-SQL5-START.
           MOVE N"4500-CHECK-SQL5"      TO WX-CURRENT-PARAGRAPH.                 
           if wf-debug = N"Y" 
              display "SCWRANG: " 
                      WX-CURRENT-PARAGRAPH
           end-if           

      *  
           EXEC sql
               SELECT  customer_code
                      ,range_group
                      ,commodity              
                      ,sub_comm
                      ,item_owner
                      ,item_code
                 INTO :scwt-cust-range.customer-code
                     ,:scwt-cust-range.range-group
                     ,:scwt-cust-range.commodity              
                     ,:scwt-cust-range.sub-comm
                     ,:scwt-cust-range.item-owner
                     ,:scwt-cust-range.item-code
                 FROM  scwt_cust_range
                WHERE cpy_cd            = :RANG-cpy-cd
                  AND facl              = :RANG-customer-facl
                  AND whse              = :RANG-customer-whse
                  AND customer_code     = ' '
                  AND range_group       = :wx-group-range
                  AND commodity         = :w9-commodity
                  AND sub_comm          = :w9-sub-comm
                  AND item_owner        = :RANG-item-owner
                  AND item_code         = ' '
           END-EXEC.
           PERFORM CB-DATABASE-LOCK-CHECK.   
      *
           MOVE N"8"                    TO WX-CURRENT-SQL-STMT.
           PERFORM 9400-EVAL-SQL.
      *     
       4500-EXIT.
           EXIT.
      *
      **----------------------------------------------------------------------**
      ** CUSTOMER_CODE          = SPACES                                      **
      ** GROUP_RANGE            = WX-GROUP-RANGE                              **
      ** COMMODITY              = WX-COMMODITY                                **
      ** SUB_COMM               = ZERO                                        **
      ** ITEM_OWNER             = RANG-ITEM-OWNER                             **
      ** ITEM-CODE              = SPACES                                      **
      **----------------------------------------------------------------------**
       4600-CHECK-SQL6 SECTION.
       4600-CHECK-SQL6-START.
           MOVE N"4600-CHECK-SQL6"      TO WX-CURRENT-PARAGRAPH.                 
           if wf-debug = N"Y" 
              display "SCWRANG: " 
                      WX-CURRENT-PARAGRAPH
           end-if           

      *  
           EXEC sql
               SELECT  customer_code
                      ,range_group
                      ,commodity              
                      ,sub_comm
                      ,item_owner
                      ,item_code
                 INTO :scwt-cust-range.customer-code
                     ,:scwt-cust-range.range-group
                     ,:scwt-cust-range.commodity              
                     ,:scwt-cust-range.sub-comm
                     ,:scwt-cust-range.item-owner
                     ,:scwt-cust-range.item-code
                 FROM  scwt_cust_range
                WHERE cpy_cd            = :RANG-cpy-cd
                  AND facl              = :RANG-customer-facl
                  AND whse              = :RANG-customer-whse
                  AND customer_code     = ' '
                  AND range_group       = :wx-group-range
                  AND commodity         = :w9-commodity
                  AND sub_comm          = 0
                  AND item_owner        = :RANG-item-owner
                  AND item_code         = ' '
           END-EXEC.
           PERFORM CB-DATABASE-LOCK-CHECK.   
      *
           MOVE N"9"                    TO WX-CURRENT-SQL-STMT.
           PERFORM 9400-EVAL-SQL.
      *    
       4600-EXIT.
           EXIT.
      *
      *------------------------------------------------------------------------*
       9400-EVAL-SQL SECTION.
       9400-EVAL-SQL-START.
      *
           EVALUATE SQLCODE
             WHEN 0        
               MOVE N"Y"                TO WF-ALLOW   
             WHEN 100
                CONTINUE                       
             WHEN OTHER
               MOVE N"RANG1003"         TO ERR-RTN-CODE
               PERFORM 9500-POISON-ERROR-AND-EXIT    
           END-EVALUATE.                       
      * 
       9400-EXIT.
           EXIT.
      *       
      *------------------------------------------------------------------------*
       9500-POISON-ERROR-AND-EXIT SECTION.
       9500-POISON-ERROR-AND-EXIT-START.
      *
           MOVE N"Y"                    TO ERR-RTN-POISON.
           MOVE SQLCODE                TO ERR-SQLCODE
           MOVE SQLSTATE               TO ERR-SQLSTATE      
           PERFORM 9700-SETUP-ERROR-RTN-DATA
           GO TO 9999-RANG-ENDIT.
      *
       9500-EXIT.
           EXIT.
      *
      *------------------------------------------------------------------------*           
       9600-ERROR-AND-EXIT SECTION.
       9600-ERROR-AND-EXIT-START.
      * 
           MOVE SQLCODE                TO ERR-SQLCODE.
           MOVE SQLSTATE               TO ERR-SQLSTATE.     
           PERFORM 9700-SETUP-ERROR-RTN-DATA.
           GO TO 9999-RANG-ENDIT.
      *
       9600-EXIT.
           EXIT.
      *
      *------------------------------------------------------------------------*           
       9700-SETUP-ERROR-RTN-DATA SECTION.
       9700-SETUP-ERROR-RTN-DATA-START.
      *
      *** Set up error data depending on error code
           MOVE SPACES                 TO ERR-RTN-DATA.
           EVALUATE ERR-RTN-CODE
             WHEN N"INVK0002"
             WHEN N"INVK0003"
             WHEN N"INVK0004"
               STRING N"Error in method " DELIMITED BY SIZE
                   WX-ERROR-METHOD       DELIMITED BY SIZE
                 INTO ERR-RTN-DATA
               END-STRING
             WHEN OTHER            
               STRING N"ST: "                 
                  ,SQLSTATE                                 
                  ,N"-"                   
                  ,RANG-cpy-cd
                  ,N"-"                   
                  ,RANG-CUSTOMER-FACL   
                  ,RANG-CUSTOMER-WHSE    
                  ,N"-"                   
                  ,N"SQL Stmt-"           
                  ,WX-CURRENT-SQL-STMT  DELIMITED BY SIZE 
                 INTO ERR-RTN-DATA
               END-STRING
           END-EVALUATE.   
      *                
           MOVE ERR-RTN-CODE       TO NODE-RESULT-FIELD. 
           MOVE ERR-RTN-DATA       TO NODE-ERROR-MESSAGE-DATA.
           MOVE ERR-RTN-POISON     TO NODE-POISON-MESSAGE-FLAG.           
      * 
           IF WF-DEBUG = N"Y"
             DISPLAY "*************************************************"
             DISPLAY "*** DEBUG INFO    "
             DISPLAY "*** Paragraph Name = "   WX-CURRENT-PARAGRAPH
             DISPLAY "*** SQLCODE        = "   SQLCODE
             DISPLAY "*** SQLSTATE       = "   SQLSTATE
             DISPLAY "*** Current SQL Stmt = " WX-CURRENT-SQL-STMT
             DISPLAY "*** Err-Rtn-Code   = "   ERR-RTN-CODE
             DISPLAY "*** Err-Rtn-Data   = "   ERR-RTN-DATA
             DISPLAY "*************************************************"              
           END-IF.
      *    
       9700-EXIT.
           EXIT.
      *
      *------------------------------------------------------------------------*           
       9999-ERROR-HANDLE SECTION.
       9999-ERROR-HANDLE-START.
      *
            PERFORM 9999-RANG-ENDIT.
      *     
       9999-ERROR-HANDLE-EXIT.
            EXIT.        
      *------------------------------------------------------------------------*           
       9999-RANG-ENDIT SECTION.
       9999-RANG-GOBACK.
      *
           GOBACK.
       COPY "SCWDBSTP.cpy".
      *
      *------------------------------------------------------------------------*           
